import * as types from './types';

class Record implements types.Record {
    public attributes: types.RecordAttributes;
    public ID: string;
    public RECORDTYPEID: string;
    public NAME: string;
    constructor(id: string, type: string, recordTypeId?: string, name?: string) {
        this.ID = id;
        this.RECORDTYPEID = recordTypeId;
        this.NAME = name;
        this.attributes = {
            type
        };
    }
}

class AgfADMWork extends Record implements types.agf__ADM_Work__c {
    public AGF__FOUND_IN_BUILD__C: string;
    public AGF__PRIORITY__C: string;
    public AGF__PRODUCT_TAG__C: string;
    public AGF__STATUS__C: string;
    public AGF__SUBJECT__C: string;
    constructor(productTag: string, foundinBuild: string, options?: types.agf__ADM_Work__c) {
        super(options.ID, 'agf__ADM_Work__c', options.RECORDTYPEID);
        this.AGF__SUBJECT__C = options.agf__Subject__c;
        this.AGF__PRODUCT_TAG__C = productTag;
        this.AGF__FOUND_IN_BUILD__C = foundinBuild;
        this.AGF__PRIORITY__C = options.AGF__PRIORITY__C ?? 'Low';
        this.AGF__STATUS__C = options.agf__Status__c ?? 'New';
    }
}

class QualityControl extends Record implements types.Quality_Control__c {
    public EXTERNAL_JOB_ID__C: string;
    public WORK__C: string;
    public OUTPUT__C: string;
    public STATUS__C: string;
    public TYPE__C: string;
    constructor(externalId: string, type: string, optionalParameters?: types.Quality_Control__c) {
        super(optionalParameters.ID, 'Quality_Control__c', optionalParameters.RECORDTYPEID);
        this.EXTERNAL_JOB_ID__C = externalId;
        this.WORK__C = optionalParameters.Work__c;
        this.OUTPUT__C = optionalParameters.Output__c;
        this.STATUS__C = optionalParameters.Status__c ?? 'In-Progress';
        this.TYPE__C = type;
    }
}

export {
    AgfADMWork as agf__ADM_Work__c,
    QualityControl as Quality_Control__c,
    Record
};

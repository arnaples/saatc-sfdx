import { exit } from '@oclif/errors';
import { core, UX } from '@salesforce/command';
import { SfdxError } from '@salesforce/core';
import { AnyJson } from '@salesforce/ts-types';
import { exec } from 'child_process';
import * as path from 'path';
import * as util from 'util';
import * as xml_js from 'xml-js';
import source2manifest from './source2manifest';
import * as saatcTypes from './types';
export interface LoginPathSchema {
    target: string;
    uri: string;
    isDefault: boolean;
}

export enum ConfigSettings {
    agile = 'AgileAccelerator.Org',
    validate = 'Validation.Org',
    test = 'Test.Org'
}

export class TreeRecordBase {
    public attributes: {
        type: string;
        referenceId: string;
    };
    constructor(objectType: string, referenceName: string) {
        this.attributes = {
            type: objectType,
            referenceId: referenceName
        };
    }
}

export class QualityControlRecord extends TreeRecordBase {
    public EXTERNAL_JOB_ID__C: string;
    public OUTPUT__C: string;
    public STATUS__C: string;
    public TYPE__C: string;
    public WORK__C: string;
    constructor(instanceId: number, jobId: string, status: string, workId: string, output?: string) {
        super('Quality_Control__c', `qc${instanceId}`);
        this.EXTERNAL_JOB_ID__C = jobId;
        this.OUTPUT__C = output;
        this.STATUS__C = status;
        this.WORK__C = workId;
    }
}

export class Tree {
    public records: TreeRecordBase[];
    private util = new SAATCUtil();
    constructor(records: TreeRecordBase[]) {
        this.records = records;
        this.writeTree();
    }
    public writeTree(): void {
        const fileData: AnyJson = JSON.parse(JSON.stringify({records: this.records}));
        this.util.writeJSONFileSync( fileData, this.util.treePath);
    }
    public async importTree(agileAlias: string): Promise<AnyJson> {
        return await this.util.importTree(this.util.treePath, agileAlias);
    }
}
export class Spinner {
    public uXInstance: UX;
    public isAsync: boolean;
    constructor(uxinstance: UX, isscript: boolean) {
        this.uXInstance = uxinstance;
        this.isAsync = isscript;
    }
    public startSpinner(message: string): void {
        if (! this.isAsync) {
            this.uXInstance.startSpinner(message);
        }
    }
    public setSpinnerStatus(message: string): void {
        if (! this.isAsync) {
            this.uXInstance.setSpinnerStatus(message);
        }
    }
    public stopSpinner(message: string): void {
        if (! this.isAsync) {
            this.uXInstance.stopSpinner(message);
        }
    }
}

export function end(error: number | SfdxError): never {
    if (typeof error === 'number') {
        exit(error);
    } else {
        throw(error);
    }
}
export class SAATCUtil {
    // PRIVATE ATTRIBUTES
    public configFolderPath = '.saatc';
    public configFilePath = `${this.configFolderPath}/config.json`;
    public treePath = `${this.configFolderPath}/treeImport.json`;
    public testResultsFileDir = `${this.configFolderPath}/tests`;
    public testResultFilePath = `${this.testResultsFileDir}/%s.json`;
    public validationResultsFileDir = `${this.configFolderPath}/validation`;
    public validationResultFilePath = `${this.validationResultsFileDir}/%s.json`;
    private addedToGitIgnore = false;
    private defaultSalesforceAPIVersion = '51.0';
    private fileStructureRegexes = {
        twoParted: /^((([\w-]+).)?([\w-]+))(?:\.\w+-meta)?(?:\.\w+$)/,
        normal: /^([\w- /%]+(?=\.))(?:\.\w+-meta)?(?:\.\w+$)/
    };
    private packageDirectories: string[] = [];
    private manifestBase = 'manifest/workItems/';
    private specialFileTypes = {
        foldered: ['reports', 'dashboards', 'email', 'documents'],
        objected: ['compactLayouts', 'fields', 'fieldSets', 'listViews', 'recordTypes', 'sharingReasons', 'validationRules', 'owebLinks'],
        twoParted: ['approvalProcesses', 'customMetadata', 'quickActions']
    };
    // SYNC FUNCTIONS
    public configureDefaultPath(targetStructure: saatcTypes.Path_Dictionary, targetToDefaul: string): saatcTypes.Path_Dictionary {
        for (const value of Object.keys(targetStructure)) {
            targetStructure[value].isDefault = false;
        }
        targetStructure[targetToDefaul].isDefault = true;
        return targetStructure;
    }
    public convertJSON2XML(jsonObject: string, filePath?: string) {
        const response = {
            status: 0,
            result: ''
        };
        try {
            const xml = xml_js.json2xml(jsonObject, {compact: true, spaces: 4, indentCdata: true});
            core.fs.writeFileSync(filePath, xml, {encoding: 'utf-8'});
            response.result = filePath;
        } catch (error) {
            response.status = 1;
            response.result = error as string;
        }
        return response;
    }
    public convertXML2JSON(xmlFilePath: string): JSON {
        if (!this.doesFileExistSync(this.manifestBase)) {
            core.fs.mkdirSync(this.manifestBase);
        }
        if (!this.doesFileExistSync(xmlFilePath)) {
            const newData = '{}';
            core.fs.writeFileSync(xmlFilePath, newData, {encoding: 'utf-8'});
            return JSON.parse(newData);
        }
        const xml = core.fs.readFileSync(xmlFilePath, 'utf-8');
        const options = {
            ignoreComment: true,
            alwaysChildren: true,
            alwaysArray: true,
            compact: true
        };
        return JSON.parse(xml_js.xml2json(xml, options));
    }
    public doesFileExistSync(filePath: string): boolean {
        return core.fs.fileExistsSync(filePath);
    }
    public garbageCollect(jobId: string, type: string) {
        if (!jobId || !type) return;
        let filePath: string;
        if (type === 'validate') {
            filePath = this.validationResultFilePath;
        } else if (type === 'test') {
            filePath = this.testResultFilePath;
        } else {
            return;
        }
        core.fs.unlinkSync(filePath.replace('%s', jobId));
    }
    public getJSONPackage(filePath?: string, apiversion?: string): saatcTypes.Manifest {

        const xmlJSON: JSON = this.convertXML2JSON(filePath);
        const packageJSON: saatcTypes.PackageFromXML[] = xmlJSON?.['Package'];
        const manifestJSON: saatcTypes.Manifest = {
            _declaration: {
                _attributes: {
                    version: '1.0',
                    encoding: 'UTF-8',
                    standalone: 'yes'
                }
            },
            Package: [
                {
                    _attributes: {
                        xmlns: 'http://soap.sforce.com/2006/04/metadata'
                    },
                    types: [],
                    version: apiversion ?? packageJSON?.[0]?.version?.[0]?._text?.[0] ?? this.getConfigFile().defaultAPI
                }
            ]
        };
        // build the change log structure from the existing package.xml
        manifestJSON.Package[0].types = packageJSON?.[0]?.types?.map(element => {
            const members: string[] = element?.members ? element.members.map(iElement => iElement._text[0]) : [];
            return {
                name: element.name?.[0]?._text?.[0] ?? 'Unnamed',
                members
            };
        }) ?? [];
        return manifestJSON;
    }
    public getManifestFilePath(manifestName) {
        return `${this.manifestBase}${manifestName}.xml`;
    }
    public isTrueError(stderr: string) {
        return stderr && !stderr.toLowerCase().includes('warning');
    }
    public setSpinnerStatus(ux: UX, message: string, isScript: boolean) {
        if (! isScript) {
            ux.setSpinnerStatus(message);
        }
    }
    public startSpinner(ux: UX, message: string, isScript: boolean) {
        if (! isScript) {
            ux.startSpinner(message);
        }
    }
    public stopSpinner(ux: UX, message: string, isScript: boolean) {
        if (! isScript) {
            ux.stopSpinner(message);
        }
    }
    public writeJSONFileSync(jsonStructure: AnyJson|saatcTypes.SAATC_Config|saatcTypes.Test_Result, filePath: string): void {
        if (! core.fs.fileExistsSync(this.configFolderPath)) {
            core.fs.mkdirSync(this.configFolderPath);
            core.fs.mkdirSync(this.testResultsFileDir);
            core.fs.mkdirSync(this.validationResultsFileDir);
            core.fs.writeFileSync(`${this.configFolderPath}/out.txt`, null);
        }
        core.fs.writeJsonSync(filePath, jsonStructure as AnyJson);
    }

    // ASYNC FUNCTION
    public async addAndCommitFile(branchName: string, filePath: string, addFullManifest: boolean = false) {
        const asyncexec = util.promisify(exec);
        const response = {status: 0, result: ''};
        let asyncReturn = {stdout: '', stderr: ''};

        asyncReturn = await asyncexec(`git add ${filePath}`);
        if (asyncReturn.stderr && this.isTrueError(asyncReturn.stderr)) {
            response.status = 1;
            response.result = asyncReturn.stderr;
            return response;
        }
        if (addFullManifest) {
            asyncReturn = await asyncexec('git add ./manifest/package.xml');
            if (asyncReturn.stderr && this.isTrueError(asyncReturn.stderr)) {
                response.status = 1;
                response.result = asyncReturn.stderr;
                return response;
            }
        }
        asyncReturn = await asyncexec(`git commit -m "Manifest-${branchName}" --no-verify`);
        if (asyncReturn.stderr && this.isTrueError(asyncReturn.stderr)) {
            response.status = 1;
            response.result = asyncReturn.stderr;
            return response;
        }
        response.result = 'files successfully staged and committed';
        return response;
    }
    public addSavedPath(schema: LoginPathSchema, forceAdd?: boolean): saatcTypes.SAATC_Config {
        const fullFile = this.getConfigFile();
        const savedPaths: saatcTypes.Path_Dictionary = fullFile.savedPaths;
        if (savedPaths.target !== undefined && !forceAdd) {
            throw new Error('target not defined');
        }
        if (schema.uri.startsWith('/')) schema.uri = schema.uri.replace('/', '');
        savedPaths.target = {
            uri: schema.uri,
            isDefault: schema.isDefault
        };
        if (schema.isDefault) this.configureDefaultPath(savedPaths, schema.target);
        this.writeConfigFile(fullFile);
        return fullFile;
    }
    public async addCommitedItemsToJSONPackage(commitOptions: saatcTypes.Commit_Configs) {
        // finally if a commit has was provided, add changes from commit
        const committedFiles = await this.getModifiedFilesFromCommitHash(commitOptions.commitHash, commitOptions.fromCommitHash);
        if (committedFiles.status === 1) {
            return {
                status: 1,
                result: 'something failed'
            };
        }
        if (!this.handleFiles(JSON.parse(committedFiles.result), commitOptions.manifest)) {
            return {
                status: 2,
                result: 'No new files added to manifest'
            };
        }
        this.cleanTypes(commitOptions.manifest.Package[0].types);
        return ({
            status: 0,
            result: commitOptions.manifest
        });
    }
    public async addWorkingItemsToJSONPackage(manifestJSON: saatcTypes.Manifest) {
        // add new working files to the change
        const getModifiedFiles = await this.getForceAppModifiedFiles();
        if (getModifiedFiles.status === 1) {
            return {
                status: 1,
                result: 'something failed'
            };
        } else if (getModifiedFiles.status === 2) {
            return {
                status: 2,
                result: 'No new files added to manifest'
            };
        }
        if (!this.handleFiles(JSON.parse(getModifiedFiles.result) as AnyJson, manifestJSON)) {
            return {
                status: 2,
                result: 'No new files added to manifest'
            };
        }
        this.cleanTypes(manifestJSON.Package[0].types);
        return ({
            status: 0,
            result: manifestJSON
        });
    }
    public async getBranchName() {
        const asyncExec = util.promisify(exec);
        const {stdout, stderr} = await asyncExec('git branch --show-current');
        if (stderr) {
            return {
                status: 1,
                result: 'Unknown issue fetching branch name'
            };
        }
        return {
            status: 0,
            result: stdout
        };
    }
    public getConfigFile(): saatcTypes.SAATC_Config {
        const filePath = path.resolve(this.configFilePath);

        let fullFile: saatcTypes.SAATC_Config;
        if (this.doesFileExistSync(this.configFilePath)) {
           fullFile = ((core.fs.readJsonSync(filePath)) as unknown) as saatcTypes.SAATC_Config;

           // in case the user removes some configs, ensure they are set
           fullFile.ValidationResultsToKeep = fullFile.ValidationResultsToKeep ?? 3;
           fullFile.TestResultsToKeep = fullFile.TestResultsToKeep ?? 3;
           fullFile.savedPaths = fullFile.savedPaths ?? {};
           fullFile.aliases = fullFile.aliases ?? {};
           fullFile.cachedWorkIds = fullFile.cachedWorkIds ?? {};
           fullFile.jobResults = fullFile.jobResults ?? {};
           fullFile.defaultAPI = fullFile.defaultAPI ?? this.defaultSalesforceAPIVersion;
           fullFile.gitIgnored = fullFile.gitIgnored ?? false;

           this.addedToGitIgnore = fullFile.gitIgnored;
        } else {
            // File doesn't exist at path. Must create a new one before proceeding
            const basicStructure: saatcTypes.SAATC_Config = {
                savedPaths: {},
                gitIgnored: false,
                defaultAPI: this.defaultSalesforceAPIVersion,
                ValidationResultsToKeep: 3,
                TestResultsToKeep: 3,
                aliases: {},
                cachedWorkIds: {},
                jobResults: {}
            };
            this.writeConfigFile(basicStructure);
            fullFile = basicStructure;
        }
        return fullFile;
    }
    public async getExistingTree(workName: string, agileAlias: string): Promise<AnyJson> {
        const asyncexec = util.promisify(exec);
        const response = {status: 0, result: ''};
        const {stdout, stderr} = await asyncexec(`sfdx force:data:tree:export -q "SELECT Component_API_Name__c FROM Release_Component__c WHERE Bug_Story__r.Name = '${workName}'" -u ${agileAlias} --outputdir=${this.configFolderPath} --json`);
        if (stderr && this.isTrueError(stderr)) {
            response.status = 1;
            response.result = stderr;
            return response;
        }
        response.result = JSON.parse(stdout)['result'];
        return response;
    }
    public async getFileName() {
        const branchName = await this.getBranchName();
        if (branchName.status === 1) {
            return branchName;
        }
        const pattern = /^(?:(?:feature|hotfix|bug)\/)([Ww]-\d{6})(?:.*)/g;
        const out = pattern.exec(branchName.result);
        if (out) {
            return {
                status: 0,
                result: out[1]
            };
        } else {
            return {
                status: 1,
                result: 'branch name not formatted properly'
            };
        }
    }
    public async getForceAppModifiedFiles() {

        const asyncexec = util.promisify(exec);

        const {stdout, stderr} = await asyncexec('git status --porcelain=v2 -z');
        if (stderr) {
            return {
                status: 1,
                result: stderr
            };
        } else {
            const changedFiles = stdout.split(String.fromCharCode(0));
            if (changedFiles.length === 0) {
                return {
                    status: 2,
                    result: 'no new files added'
                };
            }
            const additionPaths: string[] = [];
            const deletionPaths: string[] = [];
            let type: string;
            let xy: string;
            let sub: string;
            let mH: string;
            let mI: string;
            let mW: string;
            let hH: string;
            let hI: string;
            let gitPath: string[];
            changedFiles.forEach(changedFile => {
                if (changedFile) {
                    switch (changedFile.charAt(0)) {
                        case '1':
                            [type, xy, sub, mH, mI, mW, hH, hI, ...gitPath] = changedFile.split(' ');
                            break;
                        case '?':
                        case '!':
                            [type, ...gitPath] = changedFile.split(' ');
                            break;
                    }
                    type = type; sub = sub; mH = mH; mI = mI; mW = mW; hH = hH; hI = hI;
                    // checking if item is in force-app dir
                    const condensedPath = this.getCondensedPath(gitPath);
                    if (this.isSFDXFile(condensedPath)) {
                        let _unused = (xy.includes('A') || xy.includes('M')) ? additionPaths.push(condensedPath) : xy.includes('D') ? deletionPaths.push(condensedPath) : undefined;
                        _unused = _unused;
                    }
                }
            });
            return {
                status: 0,
                result: JSON.stringify({
                    additions: additionPaths,
                    deletions: deletionPaths
                })
            };
        }
    }
    public async getModifiedFilesFromCommitHash(commitHash: string, fromCommitHash?: string) {
        const asyncexec = util.promisify(exec);
        const command = fromCommitHash ? `git diff ${fromCommitHash}..${commitHash} --name-status` : `git diff ${commitHash}~ ${commitHash} --name-status`;
        const {stdout, stderr} = await asyncexec(command);
        if (stderr) {
            return {
                status: 1,
                result: stderr
            };
        } else {
            const changedFiles = stdout.split('\n');
            const additionPaths: string[] = [];
            const deletionPaths: string[] = [];
            changedFiles.forEach(changedFile => {
                if (changedFile) {
                    const [type, ...gitPath] = changedFile.split('\t');
                    const condensedPath = this.getCondensedPath(gitPath);
                    if (this.isSFDXFile(condensedPath)) {
                        ['A', 'M'].includes(type) ? additionPaths.push(condensedPath) : deletionPaths.push(condensedPath);
                    }
                }
            });
            return {
                status: 0,
                result: JSON.stringify({
                    additions: additionPaths,
                    deletions: deletionPaths
                })
            };
        }
    }
    public async getSFDXCLIVersion(): Promise<AnyJson> {
        const asyncexec = util.promisify(exec);
        const response = {status: 0, result: ''};
        const {stdout, stderr} = await asyncexec('sfdx version');
        if (stdout) {
            const versionString = stdout as string;
            const versionReg = /(?:^sfdx-cli\/)(\d {1,2}\.\d {1,3}\.\d {1,3})/g;
            const version = (versionReg.exec(versionString)?.[1] ?? '') as string;
            response.result = version;
        } else if (stderr && this.isTrueError(stderr)) {
            response.result = stderr;
            response.status = 1;
        }
        return response;
    }
    public async getWorkId(workName: string, agileAlias: string): Promise<AnyJson> {
        const asyncexec = util.promisify(exec);
        const response = {status: 0, result: ''};
        // First check to see if we've cached this workId
        const configFile = this.getConfigFile();
        const cachedWorkIds = configFile.cachedWorkIds;
        if (cachedWorkIds.hasOwnProperty(workName)) {
            response.result = cachedWorkIds[workName];
        } else { // If not retrieve it then cache it
            const {stdout, stderr} = await asyncexec(`sfdx force:data:soql:query -q "SELECT Id FROM agf__ADM_Work__c WHERE Name = '${workName}'" -u ${agileAlias} --json`);
            if (stderr && this.isTrueError(stderr)) {
                response.status = 1,
                response.result = stderr;
            } else {
                const iResponse = JSON.parse(stdout);
                const workId = iResponse['result']['records'][0]['Id'];
                response.result = workId;

                // add this workId to configFile
                cachedWorkIds[workName] = workId;
                this.writeConfigFile(configFile);
            }
        }
        return response;
    }
    public async importTree(filepath: string, agileAlias: string): Promise<AnyJson> {
        const asyncexec = util.promisify(exec);
        const {stdout, stderr} = await asyncexec(`sfdx force:data:tree:import -f ${filepath} -u ${agileAlias}`);
        const response = {status: 0, result: ''};
        if (stderr && this.isTrueError(stderr)) {
            response.status = 1,
            response.result = stderr;
        } else {
            response.result = stdout;
        }
        return response;
    }
    public writeConfigFile(jsonStructure: saatcTypes.SAATC_Config): void {
        if (! this.addedToGitIgnore) {
            this.writeToGitIgnore(this.configFolderPath);
            this.addedToGitIgnore = true;
            jsonStructure.gitIgnored = true;
        }
        this.writeJSONFileSync(jsonStructure, this.configFilePath);
    }

    // PRIVATE FUNCTIONS
    private addToStructure(typeList: saatcTypes.PackageMember[], node: string, value: string, ...otherAttributes): boolean {
        if (node === 'objects' && otherAttributes[0].length > 1) {
            otherAttributes[0][0] = otherAttributes[0][0] === 'webLinks' ? 'owebLinks' : otherAttributes[0][0];
            return this.addToStructure(typeList, otherAttributes[0][0], otherAttributes[0][1], [value]);
        } else {
            const manifestType: string = source2manifest[node];
            typeList = typeList ?? []; // if for some reason this is undefined
            let thisType = typeList.find(thisTypeInstance => thisTypeInstance.name === manifestType);
            if (thisType === undefined) {
                thisType = {
                    name: manifestType,
                    members: []
                };
                typeList.push(thisType);
            }
            value = this.getManifestMemberName(node, value, otherAttributes);
            if (!thisType.members.includes(value)) {
                thisType.members.push(value);
                return true;
            } else return false;
        }
    }
    private getCondensedPath(filePath: string[]): string {
        const condensedPath = filePath.join(' ');
        return this.isSFDXFile(condensedPath) ? condensedPath : condensedPath.substring(condensedPath.indexOf('/') + 1);
    }
    private isSFDXFile(filePath: string): boolean {
        if (this.packageDirectories.length === 0) {
            const sfdxProjectConfig = core.fs.readJsonSync('./sfdx-project.json');
            this.packageDirectories = sfdxProjectConfig?.['packageDirectories'].map((element: AnyJson) => element['path']);
        }
        return this.packageDirectories.some((element: string) => filePath.startsWith(element));
    }
    private cleanFileName(value: string, reg: RegExp): string {
        const fileParts = value.split(reg);
        return fileParts.length === 1 ? fileParts[0] : fileParts[1];
    }
    private cleanTypes(typeArray: saatcTypes.PackageMember[]) {
        typeArray.forEach(element => {
            element.members.sort( (a: string, b: string) => {
                a = a.toLowerCase();
                b = b.toLowerCase();
                return a < b ? -1 : a === b ? 0 : 1;
            });
        });
        typeArray.sort((a, b) => {
            return a.name < b.name ? -1 : a.name === b.name ? 0 : 1;
        });
    }
    private formatFolderedFileNames(value: string, otherAttributes?: any[]): string {
        otherAttributes.splice(0, 0, value);
        return otherAttributes.join('/');
    }
    private formatObjectFileName(value: string, objectApi: string): string {
        return `${objectApi}.${value}`;
    }
    private getManifestMemberName(node: string, value: string, otherAttributes: any[]) {
        if (this.specialFileTypes.foldered.includes(node)) value = this.cleanFileName(this.formatFolderedFileNames(value, otherAttributes[0]), this.fileStructureRegexes.normal);
        else if (this.specialFileTypes.objected.includes(node)) value = this.formatObjectFileName(this.cleanFileName(value, this.fileStructureRegexes.normal), otherAttributes[0] as string);
        else if (this.specialFileTypes.twoParted.includes(node)) value = this.cleanFileName(value, this.fileStructureRegexes.twoParted);
        else value = this.cleanFileName(value, this.fileStructureRegexes.normal);
        return value;
    }
    private handleFiles(filePaths: AnyJson, betterJSON: saatcTypes.Manifest) {
        let manifestModified = false;
        for (const entry of filePaths['additions']) {
            manifestModified = this.updateStructure(entry, betterJSON.Package[0].types, true) || manifestModified;
        }
        for (const entry of filePaths['deletions']) {
            manifestModified = this.updateStructure(entry, betterJSON.Package[0].types, false) || manifestModified;
        }
        return manifestModified;
    }
    private removeFromStructure(typeList: object[], node: string, value: string) {
        const manifestType: string = source2manifest[node];
        typeList = typeList ?? []; // if for some reason this is undefined
        const thisTypeIndex = typeList.findIndex(thisIndexedType => thisIndexedType['name'] === manifestType);
        let thisType = typeList[thisTypeIndex];
        if (thisType === undefined) {
            thisType = {
                name: manifestType,
                members: []
            };
            typeList.push(thisType);
        }
        const typeMembers = thisType['members'];
        if (this.specialFileTypes.foldered.includes(node)) value = this.formatFolderedFileNames(value);
        value = this.cleanFileName(value, this.fileStructureRegexes.normal);
        if (typeMembers.includes(value)) typeMembers.splice(typeMembers.indexOf(value), 1);
        if (typeMembers.length === 0) {
            typeList.splice(thisTypeIndex, 1);
            return true;
        } else {
            return false;
        }
    }
    private updateStructure(entry: string, manifestTypes: saatcTypes.PackageMember[], addToStructure: boolean): boolean {
        const metaAPI = entry.split('main/default/')[1];
        const [meta, API, ...otherAttributes] = metaAPI.split('/');
        return addToStructure ? this.addToStructure(manifestTypes, meta, API, otherAttributes) : this.removeFromStructure(manifestTypes, meta, API);
    }
    private writeToGitIgnore(line: string): void {
        const theFile = (core.fs.readFileSync('./.gitignore')).toString().split('\n');
        if (!theFile.includes(line)) {
            theFile.push(line);
            core.fs.writeFileSync('./.gitignore', theFile.join('\n'));
        }
    }
}

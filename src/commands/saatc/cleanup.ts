import { core, flags, SfdxCommand } from '@salesforce/command';
import { Messages } from '@salesforce/core';
import { AnyJson } from '@salesforce/ts-types';
import * as SAATC_UTIL from '../../ssatc-util';

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
// const messages = Messages.loadMessages('saatc-sfdx', 'saatc');

export default class CleanUp extends SfdxCommand {
    public static description = 'remove cached config variables';
    public static examples = [
        'sfdx saatc:cleanup -V -T',
        'sfdx saatc:cleanup --all'
    ];
    protected static flagsConfig = {
        validate : flags.boolean({
            char : 'V',
            description : 'remove validation results'
        }),
        tests : flags.boolean({
            char: 'T',
            description : 'remove test results'
        }),
        workid : flags.boolean({
            char : 'W',
            description : 'remove cached work ids'
        }),
        alias : flags.boolean({
            char : 'A',
            description : 'clears alias configurations'
        }),
        all : flags.boolean({
            description : 'remove all configurations',
            exclusive : ['validate', 'tests', 'workid', 'alias']
        }),
        noprompt : flags.boolean({
            description : 'skip all deletion confirmations'
        })
    };

    protected static requiresUsername = false;
    protected static requiresProject = true;
    protected saatcUtil: SAATC_UTIL.SAATCUtil = new SAATC_UTIL.SAATCUtil();

    public async run(): Promise<AnyJson> {
        const skipPrompts = this.flags.noprompt;
        const config = this.saatcUtil.getConfigFile();
        const deleteAll = this.flags.all;
        let canContinue = true;
        if (deleteAll) {
            canContinue = skipPrompts ? true : await this.ux.confirm('are you sure? this action cannot be undone');
        }
        if (deleteAll || this.flags.validate) {
            canContinue = (deleteAll || skipPrompts) ? true : await this.ux.confirm('you are about to delete all validation results. this action cannot be undone. do you want to continue?');
            if (canContinue) {
                core.fs.remove(this.saatcUtil.validationResultsFileDir)
                .catch(error => console.error(error));
                Object.values(config.jobResults).forEach(value => {
                    value.validateId = [];
                });
            }
        }
        if (deleteAll || this.flags.tests) {
            canContinue = (deleteAll || skipPrompts) ? true : await this.ux.confirm('you are about to delete all test results. this action cannot be undone. do you want to continue?');
            if (canContinue) {
                core.fs.remove(this.saatcUtil.testResultsFileDir)
                .catch(error => console.error(error));
                Object.values(config.jobResults).forEach(value => {
                    value.testId = [];
                });
            }
        }
        if (deleteAll || this.flags.tests || this.flags.validate) {
            for (const work of Object.keys(config.jobResults)) {
                if (config.jobResults[work].validateId.length === 0 && config.jobResults[work].testId.length === 0) {
                    delete config.jobResults[work];
                }
            }
        }
        if (deleteAll || this.flags.workid) {
            canContinue = (deleteAll || skipPrompts) ? true : await this.ux.confirm('you are about to delete all cahced work ids. this action cannot be undone. do you want to continue?');
            if (canContinue) {
                config.cachedWorkIds = {};
            }
        }
        if (deleteAll || this.flags.alias) {
            canContinue = (deleteAll || skipPrompts) ? true : await this.ux.confirm('you are about to delete all cahced alias information. this action cannot be undone. do you want to continue?');
            if (canContinue) {
                for (const alias of Object.keys(config.aliases)) {
                    config.aliases[alias] = undefined;
                }
            }
        }
        this.saatcUtil.writeConfigFile(config);
        return 'config file(s) cleaned up';
    }
}

import { flags, SfdxCommand } from '@salesforce/command';
import { Messages, SfdxError } from '@salesforce/core';
import { AnyJson } from '@salesforce/ts-types';
import { agf__ADM_Work__c, Quality_Control__c } from '../../../classes';
import * as SAATC_UTIL from '../../../ssatc-util';

// Initialize Messages with the current plugin directory,
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('saatc-sfdx', 'saatc');

export default class Log extends SfdxCommand {
    public static description = messages.getMessage('bamboo.log.description');
    public static examples = [
        messages.getMessage('bamboo.log.example.1'),
        messages.getMessage('bamboo.log.example.2')
    ];

    protected static flagsConfig = {
        jobid: flags.string({
            description: messages.getMessage('bamboo.log.jobid.description'),
            required: true,
            char: 'i'
        }),
        script: flags.boolean({
            description: messages.getMessage('flags.script.description')
        }),
        environment: flags.string({ // Need a corresponding work record where the env.name == --environment
            description: messages.getMessage('bamboo.log.environment.description'),
            char: 'e'
        })
    };
    protected static requiresUsername = true;
    protected static requiresProject = true;
    protected saatcUtil: SAATC_UTIL.SAATCUtil = new SAATC_UTIL.SAATCUtil();

    public async run(): Promise<AnyJson> {
        const spinner = new SAATC_UTIL.Spinner(this.ux, this.flags.script);
        spinner.startSpinner('saatc:bamboo:log...');
        spinner.setSpinnerStatus('fetching required information');
        const connection = this.org.getConnection();
        const workConnection = connection.sobject('agf__ADM_Work__c');
        const qualityConnection = connection.sobject('Quality_Control__c');
        const workRecord = (await workConnection.findOne(`agf__Subject__c = 'Build Health ${this.flags.environment}'`, ['Id'])) as agf__ADM_Work__c;
        const qualityControlRecord = new Quality_Control__c(this.flags.jobid, 'Build', {Status__c: 'In-Progress', Work__c: workRecord.ID});

        spinner.setSpinnerStatus('recording build attempt');

        await qualityConnection.create(qualityControlRecord).catch (rejection => {
            console.error(rejection);
            throw new SfdxError(rejection);
        });
        return {
            status: 0,
            result: 'Build Attempt Recorded'
        };
    }
}

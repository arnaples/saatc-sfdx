import { flags, SfdxCommand } from '@salesforce/command';
import { Aliases, AuthInfo, Connection, Messages, SfdxError } from '@salesforce/core';
import { AnyJson } from '@salesforce/ts-types';
import * as SAATC_UTIL from '../../../ssatc-util';
import { agf__ADM_Work__c, Quality_Control__c } from '../../../types';

// Initialize Messages with the current plugin directory,
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('saatc-sfdx', 'saatc');

export default class Poll extends SfdxCommand {
    public static description = messages.getMessage('bamboo.poll.description');
    public static examples = [
        messages.getMessage('bamboo.poll.example.1')
    ];

    protected static flagsConfig = {
        environment: flags.string({ // Need a corresponding work record where the env.name == --environment
            description: messages.getMessage('bamboo.poll.environment.description'),
            char: 'e'
        }),
        jobenvironmentusername: flags.string({
            description: messages.getMessage('bamboo.poll.jobenvironmentusername.description'),
            char: 'j',
            required: true
        }),
        script: flags.boolean({
            description: messages.getMessage('flags.script.description')
        })
    };

    protected static requiresUsername = true;
    protected static requiresProject = true;
    protected saatcUtil: SAATC_UTIL.SAATCUtil = new SAATC_UTIL.SAATCUtil();

    public async run(): Promise<AnyJson> {
        const spinner = new SAATC_UTIL.Spinner(this.ux, this.flags.script);
        spinner.startSpinner('saatc:bamboo:log...');
        spinner.setSpinnerStatus('fetching required information');
        const connection = this.org.getConnection(); // this should be the alias for the training org
        let orgAlias: string;
        await Aliases.fetch(this.flags.jobenvironmentusername)
                .then(value => orgAlias = value)
                .catch (rejection => {
                    throw new SfdxError(rejection);
                });
        if (orgAlias === undefined) {
            throw new SfdxError('No valid authorizations for the supplied job environment alias');
        }
        const targetConnection = await Connection.create({
            authInfo: await AuthInfo.create({
                username: orgAlias
            })
        });
        const qualityControlConnection = connection.sobject('Quality_Control__c');
        const qualityControlRecord: Quality_Control__c = await qualityControlConnection.findOne(`Work__r.agf__Subject__c = 'Build Health ${this.flags.environment}' AND Status__c = 'In-Progress'`,
                                                                            ['Id', 'Output__c', 'Status__c', 'External_Job_Id__c', 'Work__c']
                                                                        ) as Quality_Control__c;
        if ( qualityControlRecord === null || qualityControlRecord === undefined ) {
            throw new SfdxError('No Quality Control was found with the supplied parameters');
        }
        const workConnection = connection.sobject('agf__ADM_Work__c');
        const workRecord: agf__ADM_Work__c = await workConnection.findOne(`Id = '${qualityControlRecord.Work__c}'`, ['Id', 'agf__Status__c']) as agf__ADM_Work__c;
        // update fails if the workId is passed b/c of M-D relationship
        delete qualityControlRecord.Work__c;
        const jobResults = await targetConnection.metadata.checkDeployStatus(qualityControlRecord.External_Job_Id__c, true);
        if (jobResults.success === false) {
            qualityControlRecord.Output__c = JSON.stringify(jobResults.details?.['componentFailures'], undefined, 2 );
            qualityControlRecord.Status__c = 'Fail';
            workRecord.agf__Status__c = 'Rejected';
        } else {
            qualityControlRecord.Status__c = 'Pass';
            workRecord.agf__Status__c = 'Ready for Deployment';
        }
        spinner.setSpinnerStatus('recording job details');
        await qualityControlConnection.update(qualityControlRecord).catch(rejection => {
            console.error(rejection);
            throw new SfdxError(rejection);
        });
        await workConnection.update(workRecord).catch(rejection => {
            console.error(rejection);
            throw new SfdxError(rejection);
        });
        return {
            status: 0,
            result: 'Build Attempt Recorded'
        };
    }
}

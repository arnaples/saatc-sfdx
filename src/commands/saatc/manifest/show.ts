import { core, SfdxCommand } from '@salesforce/command';
import { Messages, SfdxError } from '@salesforce/core';
import { AnyJson } from '@salesforce/ts-types';
import { exec } from 'child_process';
import * as SAATC_UTIL from '../../../ssatc-util';

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('saatc-sfdx', 'saatc');

export default class Show extends SfdxCommand {
    public static description = messages.getMessage('manifest.build.description');
    public static examples = [
        messages.getMessage('manifest.build.example.1'),
        messages.getMessage('manifest.build.example.2')
    ];

    protected static requiresUsername = false;
    protected static requiresProject = true;
    protected saatcUtil: SAATC_UTIL.SAATCUtil = new SAATC_UTIL.SAATCUtil();

    public async run(): Promise<AnyJson> {
        const spinner = new SAATC_UTIL.Spinner(this.ux, this.flags.script);
        spinner.startSpinner('saatc:manifest:show...');
        spinner.setSpinnerStatus('getting branch name');
        const fileName = await this.saatcUtil.getFileName();
        if (fileName?.status === 1) {
            throw new SfdxError(fileName.result);
        }
        const branchName: string = fileName?.result;
        const expectedManifestPath = `./manifest/workItems/${branchName}.xml`;
        if (!core.fs.fileExistsSync(expectedManifestPath)) {
            throw new SfdxError(
                'There is no manifest created yet for this branch',
                'manifestnotcreated',
                [
                    'sfdx saatc:manifest:built -c <commitHas>'
                ],
                1
            );
        }
        exec(`code ${expectedManifestPath}`);
        this.ux.log(expectedManifestPath);
        return {
            status: 0,
            result: expectedManifestPath
        };
    }
}

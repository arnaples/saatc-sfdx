import { core, flags, SfdxCommand } from '@salesforce/command';
import { Messages, Org, SfdxError } from '@salesforce/core';
import { AnyJson } from '@salesforce/ts-types';
import { exec } from 'child_process';
import * as util from 'util';
import * as SAATC_UTIL from '../../../ssatc-util';

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('saatc-sfdx', 'saatc');

export default class Validate extends SfdxCommand {
    public static description = messages.getMessage('manifest.validate.description');
    public static examples = [
        messages.getMessage('manifest.build.example.1'),
        messages.getMessage('manifest.build.example.2')
    ];

    protected static flagsConfig = {
        nowait: flags.boolean({
            description: 'execute the validate asynchronously',
            char: 'W'
        }),
        script: flags.boolean({
            description: 'do not set if not running command from a script'
        })
    };
    protected static requiresUsername = false;
    protected static requiresProject = true;

    protected saatcUtil: SAATC_UTIL.SAATCUtil = new SAATC_UTIL.SAATCUtil();

    public async run(): Promise<AnyJson> {
        const spinner = new SAATC_UTIL.Spinner(this.ux, this.flags.script);
        spinner.startSpinner('saatc:manifest:validate...');
        spinner.setSpinnerStatus('checking configs');
        let workName: string;

        const config = this.saatcUtil.getConfigFile();
        if (! config?.aliases[SAATC_UTIL.ConfigSettings.validate]) {
            throw new SfdxError(
                messages.getMessage('manifest.validate.error.aliasnotset'),
                'confignotset',
                [
                    messages.getMessage('manifest.validate.error.aliasnotset.try1')
                ],
                1
            );
        }

        const validationAlias = config?.aliases[SAATC_UTIL.ConfigSettings.validate];
        const orgAlias: Org = await Org.create({
            aliasOrUsername: validationAlias
        });
        await orgAlias.refreshAuth();
        spinner.setSpinnerStatus('getting manifest');
        const fileName = await this.saatcUtil.getFileName();
        if (fileName?.status === 1) {
            throw new SfdxError(fileName.result);
        } else {
            workName = fileName?.result;
        }
        const filePath = this.saatcUtil.getManifestFilePath(workName);
        if (!this.saatcUtil.doesFileExistSync(filePath)) {
            throw new SfdxError(
                'there is no manifest created for your current branch',
                'validate.error.manifestnotcreated',
                [
                    'sfdx saatc:build:package -c <somecommithash>',
                    'sfdx saatc:build:package -W'
                ],
                1
            );
        }
        // run a validation
        spinner.setSpinnerStatus('starting validation');
        // make the request, but set a no wait to get the job id
        const dontWait = (this.flags.nowait || this.flags.script);
        if (dontWait) {
            spinner.isAsync = true;
            this.validate(workName, filePath, validationAlias, spinner)
            .catch(error => {
                console.error(error);
            });

        } else {
            const validateResult = await this.validate(workName, filePath, validationAlias, spinner);
            if (validateResult.status === 2) {
                spinner.stopSpinner('Validation Failed');
                const resultObj = JSON.parse(validateResult.response);
                this.ux.table(resultObj, ['lineNumber', 'componentType', 'fullName', 'problem']);
            } else if (validateResult.status === 1) {
                throw new SfdxError(validateResult.response);
            }
        }
        return {
            status: 0,
            result: 'Validation Successful'
        };
    }

    /**
     * @statuscode 0 = success
     * @statuscode 1 = process failed
     * @statuscode 2 = validation returned errors
     * @statuscode 10 = record quality controls failed but validation succeeded
     * @statuscode 12 = record quality controls failed AND validation returned errors
     */
    private async validate(branchName: string, filePath: string, validationAlias: string, spinner: SAATC_UTIL.Spinner) {
        const asyncexec = util.promisify(exec);
        let asyncReturn = {stdout: '', stderr: ''};
        const result = {status: 0, response: ''};
        spinner.setSpinnerStatus('initiating validation');
        asyncReturn = await asyncexec(`sfdx force:source:deploy -x ${filePath} -w 0 -u ${validationAlias} -c --json`);
        if (asyncReturn.stderr && this.saatcUtil.isTrueError(asyncReturn.stderr)) {
            result.status = 1;
            result.response = asyncReturn.stderr;
            return result;
        }
        let deployId: string;
        const validationRequestPromise = JSON.parse(asyncReturn.stdout);
        if (validationRequestPromise['status'] === 0) {
            deployId = validationRequestPromise['result']['deploys'][0]['id'];
        }
        // writing the job id to the confiig file
        const saatc = new SAATC_UTIL.SAATCUtil();
        const config = saatc.getConfigFile();
        const jobResult = config.jobResults[branchName];
        if (!jobResult) {
            config.jobResults[branchName] = {
                validateId: [deployId],
                testId: []
            };
        } else {
            let newSize = config.jobResults[branchName].validateId.unshift(deployId);
            while (newSize > config.ValidationResultsToKeep ) {
                saatc.garbageCollect(config.jobResults[branchName].validateId.pop(), 'validate');
                newSize--;
            }
        }
        saatc.writeConfigFile(config);

        spinner.setSpinnerStatus('validating');
        // if request was successful, wait for the report to complete
        try {
            asyncReturn = await asyncexec(`sfdx force:source:deploy:report -i ${deployId} -u ${validationAlias} --json`);
        } catch (error) {
            asyncReturn.stdout = error['stdout'];
            asyncReturn.stderr = error['stderr'];
        }
        if (asyncReturn.stderr && this.saatcUtil.isTrueError(asyncReturn.stderr)) {
            result.status = 1;
            result.response = asyncReturn.stderr;
            return result;
        }
        const validationReposnsePromise = JSON.parse(asyncReturn.stdout) as AnyJson;
        const expectedValidationPath =  this.saatcUtil.validationResultFilePath.replace('%s', deployId);
        if (!this.saatcUtil.doesFileExistSync(this.saatcUtil.validationResultsFileDir)) {
            core.fs.mkdirSync(this.saatcUtil.validationResultsFileDir);
        }
        this.saatcUtil.writeJSONFileSync(validationReposnsePromise, expectedValidationPath);
        spinner.setSpinnerStatus('validation complete');
        const failures = validationReposnsePromise?.['result']?.['details']?.['componentFailures'];
        const successes = validationReposnsePromise?.['result']?.['details']?.['componentSuccesses'];
        if (failures) {
            spinner.setSpinnerStatus('validation failed');
            const results = Array.isArray(failures) ? failures : [failures];
            result.status = 2;
            result.response = JSON.stringify(results);
        } else if (!failures && successes) {
            // currently do nothing.
            spinner.setSpinnerStatus('validation successful');
            result.status = 0;
        }
        this.ux.log('Validation Results can be found here: ' + expectedValidationPath);
        return result;
    }
}

import { flags, SfdxCommand } from '@salesforce/command';
import { Messages, SfdxError } from '@salesforce/core';
import { AnyJson } from '@salesforce/ts-types';
import * as SAATC_UTIL from '../../../ssatc-util';

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('saatc-sfdx', 'saatc');

export default class Build extends SfdxCommand {
    public static description = messages.getMessage('manifest.build.description');
    public static examples = [
        messages.getMessage('manifest.build.example.1'),
        messages.getMessage('manifest.build.example.2')
    ];

    protected static flagsConfig = {
        api : flags.string({
            char : 'v',
            description : messages.getMessage('manifest.build.params.apiversion.description')
        }),
        commit: flags.string({
            char: 'c',
            description: messages.getMessage('manifest.build.params.commit.description')
        }),
        includeworkingfiles: flags.boolean({
            char: 'W',
            description: 'if set, will include uncommitted changes into the manifest from your current working directory'
        }),
        commitgeneratedmanifest: flags.boolean({
            char: 'B',
            description: 'commit the generated manifest file immediately upon creation',
            dependsOn: ['commit']
        }),
        fromcommit: flags.string({
            char: 'f',
            description: 'if declared, generate the manifest as a diff from the supplied hash'
        }),
        nopackage: flags.boolean({
            description: 'do not update the package manifest'
        }),
        filepath: flags.string({
            char: 'p',
            description: 'if declared, will write the manifest at the file path'
        }),
        script: flags.boolean({
            description: 'do not set if not running command from a script'
        })
    };

    protected static requiresUsername = false;
    protected static requiresProject = true;
    protected saatcUtil: SAATC_UTIL.SAATCUtil = new SAATC_UTIL.SAATCUtil();

    public async run(): Promise<AnyJson> {
        const spinner = new SAATC_UTIL.Spinner(this.ux, this.flags.script);
        spinner.startSpinner('saatc:manifest:build...');
        const apiversion: string = this.flags.api;
        const commitHash: string = this.flags.commit;
        const commitManifest: boolean = this.flags.commitgeneratedmanifest;
        const fromCommitHash: string = this.flags.fromcommit;
        const suppliedPath: string = this.flags.filepath;
        let branchName: string;
        let filesAdded = false;
        let fullPackageUpdated = false;
        if (!suppliedPath) {
            spinner.setSpinnerStatus('getting branch name');
            const fileName = await this.saatcUtil.getFileName();
            if (fileName?.status === 1) {
                throw new SfdxError(fileName.result);
            }
            branchName = fileName?.result;
        }
        spinner.setSpinnerStatus('building manifest');
        const filePath = suppliedPath ?? this.saatcUtil.getManifestFilePath(branchName);
        const jsonManifest = this.saatcUtil.getJSONPackage(filePath, apiversion);
        const packageManifest = this.saatcUtil.getJSONPackage('./manifest/package.xml');

        if (this.flags.includeworkingfiles) {
            const getWorkingPackageResults = await this.saatcUtil.addWorkingItemsToJSONPackage(jsonManifest);
            if (getWorkingPackageResults.status === 0) {
                filesAdded = true;
            } else if (getWorkingPackageResults.status === 1) {
                throw new SfdxError(
                    getWorkingPackageResults.result as string,
                    'package.error.getworkingpackage',
                    undefined,
                    1
                );
            }
        }
        if (commitHash) {
            const getCommittedPackageResults = await this.saatcUtil.addCommitedItemsToJSONPackage({manifest: jsonManifest, commitHash, fromCommitHash});
            const getUpdateToFullManifestResults = this.flags.nopackage
                ? {status: 2, result: 'skipping addition of package'}
                : await this.saatcUtil.addCommitedItemsToJSONPackage({manifest: packageManifest, commitHash, fromCommitHash});
            if (getCommittedPackageResults.status === 0) {
                filesAdded = true;
            } else if (getCommittedPackageResults.status === 1) {
                throw new SfdxError(
                    getCommittedPackageResults.result as string,
                    'package.error.getcommittedpackage',
                    undefined,
                    1
                );
            }
            if (getUpdateToFullManifestResults.status === 0) {
                fullPackageUpdated = true;
            } else if (getUpdateToFullManifestResults.status === 1) {
                throw new SfdxError(
                    getUpdateToFullManifestResults.result as string,
                    'package.error.getcommittedpackage',
                    undefined,
                    1
                );
            }
        }
        if (!filesAdded && !fullPackageUpdated) {
            this.ux.stopSpinner('no new files added to manifest');
            if (this.flags.script) {
                this.exit(2);
            }
            return {
                status: 2,
                result: 'no new files added to manifest'
            };
        }
        spinner.setSpinnerStatus('writing manifest');
        const writeResult = this.saatcUtil.convertJSON2XML(JSON.stringify(jsonManifest), filePath);
        const writeResult1 = this.flags.nopackage
        ? {status: 2, result: 'skipping addition of package'}
        : this.saatcUtil.convertJSON2XML(JSON.stringify(packageManifest), './manifest/package.xml');
        if (writeResult.status === 1) {
            throw new SfdxError(
                `Error while writing XML file; ${writeResult.result}`,
                'package.error.writemanifest',
                undefined,
                1
            );
        }
        if (writeResult1.status === 1) {
            throw new SfdxError(
                `Error while writing XML file; ${writeResult.result}`,
                'package.error.writemanifest',
                undefined,
                1
            );
        }
        if (commitManifest) {
            spinner.setSpinnerStatus('committing new manifest');
            const commitResult = await this.saatcUtil.addAndCommitFile(branchName, this.saatcUtil.getManifestFilePath(branchName), fullPackageUpdated);
            if (commitResult.status === 1) {
                throw new SfdxError(
                    `Error while adding/committing file; ${commitResult.result}`,
                    'package.error.commitnewmanifest',
                    undefined,
                    1
                );
            }
        }
        this.ux.stopSpinner('manifest complete');
        this.ux.log(`manifest created at ${writeResult.result}`);
        if (this.flags.script) {
            this.exit(0);
        }
        return jsonManifest as unknown as AnyJson;
    }
}

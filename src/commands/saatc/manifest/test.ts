import { flags, SfdxCommand } from '@salesforce/command';
import { fs, Messages, Org, SfdxError } from '@salesforce/core';
import { AnyJson } from '@salesforce/ts-types';
import { exec } from 'child_process';
import * as util from 'util';
import * as SAATC_UTIL from '../../../ssatc-util';
import { PackageMember, Test_Result } from '../../../types';

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('saatc-sfdx', 'saatc');

export default class Test extends SfdxCommand {
    public static description = 'This command is still under construction';
    public static examples = [
        messages.getMessage('build.package.example.1'),
        messages.getMessage('build.package.example.2')
    ];
    protected static flagsConfig = {
        commandonly: flags.boolean({
            description: 'returns the command to run; does not execute the command',
            char: 'C'
        }),
        script: flags.boolean({
            description: 'do not set if not running command from a script'
        })
    };

    protected static requiresUsername = false;
    protected static requiresProject = true;
    protected saatcUtil: SAATC_UTIL.SAATCUtil = new SAATC_UTIL.SAATCUtil();

    public async run(): Promise<AnyJson> {
        const spinner = new SAATC_UTIL.Spinner(this.ux, this.flags.script);
        spinner.startSpinner('saatc:manifest:test');
        const asyncExec = util.promisify(exec);
        spinner.setSpinnerStatus('checking configs');
        let workName: string;

        const config = this.saatcUtil.getConfigFile();
        if (! config?.aliases[SAATC_UTIL.ConfigSettings.test]) {
            throw new SfdxError(
                messages.getMessage('agile.validate.error.aliasnotset'),
                'confignotset',
                [
                    messages.getMessage('agile.validate.error.aliasnotset.try1')
                ],
                1
            );
        }

        const validationAlias = config?.aliases[SAATC_UTIL.ConfigSettings.test];
        const orgAlias: Org = await Org.create({
            aliasOrUsername: validationAlias
        });
        await orgAlias.refreshAuth();
        spinner.setSpinnerStatus('getting manifest');
        const fileName = await this.saatcUtil.getFileName();
        if (fileName?.status === 1) {
            throw new SfdxError(fileName.result);
        } else {
            workName = fileName?.result;
        }
        const filePath = this.saatcUtil.getManifestFilePath(workName);
        if (!this.saatcUtil.doesFileExistSync(filePath)) {
            throw new SfdxError(
                'there is no manifest created for your current branch',
                'validate.error.manifestnotcreated',
                [
                    'sfdx saatc:build:package -c <somecommithash>',
                    'sfdx saatc:build:package -W'
                ],
                1
            );
        }
        spinner.setSpinnerStatus('preparing tests');
        const jsonManifest = this.saatcUtil.getJSONPackage(workName);

        const modifiedApex: PackageMember = jsonManifest.Package[0].types.find(element => element.name === 'ApexClass');
        if (!modifiedApex) {
            return 'no testable apex found';
        }
        const testClassSet: Set<string> = new Set<string>();
        let asyncReturn = {stdout: '', stderr: ''};

        const getTestClasses = async (classes: string[]) => {
            const classLength = classes.length;
            const classSet = new Set<string>(classes);
            const followUpClassSet = new Set<string>();
            const commandString = `grep force-app/main/default -r -e ${classes.join(' -e ')} -l`;
            asyncReturn = await asyncExec(commandString);
            const foundClasses = asyncReturn.stdout.split('\n');
            foundClasses.forEach(foundClass => {
                if (foundClass.toLowerCase().includes('test')) testClassSet.add(foundClass);
                else followUpClassSet.add(foundClass);
            });
            if (classSet.size !== classLength) {
                getTestClasses(Array.from(classSet))
                .catch(error => console.error(error));
            }
        };
        try {
            await getTestClasses(modifiedApex.members);
        } catch (e) {
            const error = JSON.stringify(e);
            if (error.includes("'grep' is not recognized")) {
                throw new SfdxError(
                    'This command can currently only be run from a linux shell'
                );
            } else {
                throw new SfdxError(
                    error
                );
            }
        }

        const testClasses = Array.from(testClassSet).map(element => {
            return element.substring(element.lastIndexOf('/') + 1, element.indexOf('.cls'));
        });
        // need to trim filePath details from test classes
        const command = `sfdx force:apex:test:run -t "${testClasses.join(',')}" -u ${validationAlias} --json`;
        if (this.flags.commandonly) {
            spinner.stopSpinner('command created');
            this.ux.log(command);
            return command;
        }
        spinner.setSpinnerStatus('running tests');
        asyncReturn = await asyncExec(command);
        spinner.setSpinnerStatus('processing results');
        if (this.saatcUtil.isTrueError(asyncReturn.stderr)) {
            console.error(asyncReturn.stderr);
            throw new SfdxError(
                asyncReturn.stderr,
                'runapextests',
                [
                    messages.getMessage('agile.components.example.1')
                ],
                1
            );
        }
        const testResults = JSON.parse(asyncReturn.stdout) as Test_Result;
        const jobId = testResults.result.summary.testRunId;
        const jobResult = config.jobResults[workName];
        if (!jobResult) {
            config.jobResults[workName] = {
                validateId: [],
                testId: [jobId]
            };
        } else {
            let newSize = config.jobResults[workName].testId.unshift(jobId);
            while (newSize > config.TestResultsToKeep ) {
                this.saatcUtil.garbageCollect(config.jobResults[workName].testId.pop(), 'test');
                newSize--;
            }
        }
        this.saatcUtil.writeConfigFile(config);
        if (!this.saatcUtil.doesFileExistSync(this.saatcUtil.testResultsFileDir)) {
            fs.mkdirSync(this.saatcUtil.testResultsFileDir);
        }
        const resultFilePath = `${this.saatcUtil.testResultFilePath.replace('%s', jobId)}`;
        this.saatcUtil.writeJSONFileSync(testResults, resultFilePath);
        const outcome = testResults.result.summary.outcome.toLowerCase();
        const output = `Test ${jobId} ${outcome}. Full results can be found here: ${resultFilePath}`;
        const failedTests = testResults.result.tests.filter(element => element.Outcome === 'Fail');
        if (!this.flags.script) {
            this.ux.log(output);
            if (outcome === 'failed') {
                this.ux.table(failedTests, {
                    columns: [
                        {key: 'FullName', label: 'Test'},
                        {key: 'Message', label: 'Error Message', format: (value, row): string => {
                            return value.replace(/: /g, ':\n');
                        }},
                        {key: 'StackTrace' , label: 'Stack Trace'}
                    ]
                });
            }
        }
        return output;
    }
}

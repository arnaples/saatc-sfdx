import { flags, SfdxCommand } from '@salesforce/command';
import { Aliases, AuthInfo, Connection, Messages, SfdxError } from '@salesforce/core';
import { Tokens } from '@salesforce/core/lib/messages';
import { AnyJson } from '@salesforce/ts-types';
import * as SAATC_UTIL from '../../../../ssatc-util';
import { Quality_Control__c } from '../../../../types';

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('saatc-sfdx', 'saatc');

export default class Report extends SfdxCommand {
    public static description = messages.getMessage('agile.commit.description');
    public static examples = [
        messages.getMessage('agile.commit.example.1'),
        messages.getMessage('agile.commit.example.2')
    ];

    protected static flagsConfig = {
        script: flags.boolean({
            description: messages.getMessage('flgs.script.description')
        })
    };
    protected static requiresUsername = false;
    protected static requiresProject = true;
    protected saatcUtil: SAATC_UTIL.SAATCUtil = new SAATC_UTIL.SAATCUtil();

    public async run(): Promise<AnyJson> {
        const spinner = new SAATC_UTIL.Spinner(this.ux, this.flags.script);
        const response = {status: 0, result: ''};
        spinner.startSpinner('saatc:agile:validate:report...');
        spinner.setSpinnerStatus('checking configurations');
        const config = this.saatcUtil.getConfigFile();
        if (! config?.aliases[SAATC_UTIL.ConfigSettings.agile]) {
            throw new SfdxError(
                messages.getMessage('common.error.confignotset'),
                'confignotset',
                [
                    messages.getMessage('agile.components.error.confignotset.try.1')
                ]
            );
        }
        let branchName: string;
        spinner.setSpinnerStatus('getting work name');
        const branchObject = await this.saatcUtil.getFileName();
        if (branchObject?.status === 1) {
            throw new SfdxError(branchObject.result);
        } else {
            branchName = branchObject?.result;
        }
        const agileAlias = config.aliases[SAATC_UTIL.ConfigSettings.agile];
        const getWorkIdResult = await this.saatcUtil.getWorkId(branchName, agileAlias);
        if (getWorkIdResult['status'] === 1) {
            const errorTokens: Tokens = [getWorkIdResult['result']];
            throw new SfdxError(messages.getMessage('agile.components.error.unknown', errorTokens));
        }
        const workId: string = getWorkIdResult['result'];
        // End Preparation
        let connection: Connection;
        try {
            connection = await Connection.create({
                authInfo: await AuthInfo.create({
                    username: await Aliases.fetch(agileAlias)
                })
            });
        } catch (error) {
            SAATC_UTIL.end(this.flags.script
                ? 1
                : new SfdxError(
                    error,
                    'cannotestablishconnection',
                    [],
                    1
                )
            );
        }
        spinner.setSpinnerStatus('fetching quality control data');
        let qcRecord: Quality_Control__c;
        try {
            qcRecord = (await connection.sobject('Quality_Control__c').findOne(`Work__c = '${workId}' AND Type__c = 'Validation' ORDER BY CREATEDDATE DESC`, ['Id' , 'Status__c'], {limit: 1})) as Quality_Control__c;
        } catch (error) {
            SAATC_UTIL.end(this.flags.script
                ? 1
                : new SfdxError(
                    error,
                    'validationmissing',
                    [
                        'sfdx saatc:agile:validate:report'
                    ],
                    1
                )
            );
        }
        if (qcRecord.Status__c !== 'Pass') {
            SAATC_UTIL.end(this.flags.script
                ? 2
                : new SfdxError(
                    'The most recent validation has failed. Push not allowed',
                    'invalidpush',
                    [
                        'sfdx saatc:agile:validate',
                        'sfdx saatc:agile:validate:report'
                    ],
                    2
                )
            );
        } else {
            if (this.flags.script) {
                this.exit(0);
            } else {
                const message = 'most recent validation was successful';
                spinner.stopSpinner(message);
                response.result = message;
            }
        }
        return response;
    }
}

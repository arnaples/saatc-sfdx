import { flags, SfdxCommand } from '@salesforce/command';
import { Messages, SfdxError } from '@salesforce/core';
import { AnyJson } from '@salesforce/ts-types';
import { exec } from 'child_process';
import * as util from 'util';
import * as SAATC_UTIL from '../../../ssatc-util';

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('saatc-sfdx', 'saatc');

export default class Deploy extends SfdxCommand {
    public static description = messages.getMessage('manifest.build.description');
    public static examples = [
        messages.getMessage('manifest.build.example.1'),
        messages.getMessage('manifest.build.example.2')
    ];

    protected static flagsConfig = {
        quiet: flags.builtin()
    };

    protected static requiresUsername = true;
    protected static requiresProject = true;
    protected saatcUtil: SAATC_UTIL.SAATCUtil = new SAATC_UTIL.SAATCUtil();

    public async run(): Promise<AnyJson> {
        await this.org.refreshAuth();
        const spinner = new SAATC_UTIL.Spinner(this.ux, !this.flags.quiet);
        spinner.startSpinner('saatc:manifest:deploy');
        spinner.setSpinnerStatus('getting manifest...');
        const asyncexec = util.promisify(exec);
        let branchName: string;
        const fileName = await this.saatcUtil.getFileName();
        if (fileName?.status === 1) {
            throw new SfdxError(fileName.result);
        } else {
            branchName = fileName?.result;
        }
        const filePath = this.saatcUtil.getManifestFilePath(branchName);
        const fileExists = this.saatcUtil.doesFileExistSync(filePath);
        if (!fileExists) {
            throw new SfdxError(
                'there is not a manifest created yet for your currently set branch',
                'packagenotcreated',
                [
                    'sfdx saatc:manifest:build -c somecommithash',
                    'sfdx saatc:manifest:build'
                ],
                1
            );
        }
        await asyncexec(`sfdx force:source:deploy -x ${filePath} -u ${this.flags.targetusername}`);
        return {
            status: 0,
            result: 'finished'
        };
    }
}

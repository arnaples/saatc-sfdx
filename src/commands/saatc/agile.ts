import { SfdxCommand } from '@salesforce/command';
import { Messages } from '@salesforce/core';
import { AnyJson } from '@salesforce/ts-types';
import * as SAATC_UTIL from '../../ssatc-util';

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('saatc-sfdx', 'saatc');

export default class Agile extends SfdxCommand {
    public static description = messages.getMessage('agile.description');
    public static examples = [
        messages.getMessage('agile.example.1')
    ];

    protected static requiresUsername = false;
    protected static requiresProject = false;
    protected saatcUtil: SAATC_UTIL.SAATCUtil = new SAATC_UTIL.SAATCUtil();

    public async run(): Promise<AnyJson> {
        const configFile = this.saatcUtil.getConfigFile();
        const tableData = [];
        for (const [key, value] of Object.entries(SAATC_UTIL.ConfigSettings)) {
            tableData.push(
                {
                    setting: key,
                    alias: configFile.aliases[value]
                }
            );
        }
        this.ux.table(tableData, ['setting', 'alias']);
        return tableData;
    }
}

import { flags, SfdxCommand } from '@salesforce/command';
import { Messages, SfdxError } from '@salesforce/core';
import { AnyJson } from '@salesforce/ts-types';
import { exec } from 'child_process';
import * as util from 'util';
import * as SAATC_UTIL from '../../ssatc-util';
import * as saatcTypes from '../../types';

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('saatc-sfdx', 'saatc');

export default class Login extends SfdxCommand {
    public static description = messages.getMessage('login.description');
    public static examples = [
        messages.getMessage('login.example1'),
        messages.getMessage('login.example2')
    ];
    protected static flagsConfig = {
        alias : flags.string({
            char : 'u',
            description : messages.getMessage('login.params.alias.description')
        }),
        target : flags.string({
            char : 't',
            description : messages.getMessage('login.params.target.description')
        }),
        default : flags.boolean({
            char : 'D',
            description : messages.getMessage('login.params.default.description')
        })
    };

    protected static requiresUsername = false;
    protected static requiresProject = true;
    protected saatcUtil: SAATC_UTIL.SAATCUtil = new SAATC_UTIL.SAATCUtil();

    public async run(): Promise<AnyJson> {
        const asyncexec = util.promisify(exec);
        const alias = this.flags.alias;
        const target = this.flags.target;
        const isDefault = this.flags.default ?? false;
        let fullFile: saatcTypes.SAATC_Config = this.saatcUtil.getConfigFile();
        let uri = this.flags.uri;
        const savedPaths: saatcTypes.Path_Dictionary = fullFile.savedPaths;
        if (target) {
            if (savedPaths.target === undefined) {
                const addNew = await this.ux.prompt(messages.getMessage('login.prompt.addNewConfig'));
                while (uri === undefined) uri = await this.ux.prompt(messages.getMessage('login.prompt.addNewConfig.addNewURI'));
                if (addNew.toLowerCase() === 'y') {
                    fullFile = this.saatcUtil.addSavedPath({target, uri, isDefault});
                    savedPaths.target = fullFile.savedPaths[target];
                } else {
                    const exitMessage = messages.getMessage('login.abort.configNotAdded');
                    this.ux.log(exitMessage);
                    return exitMessage;
                }
            } else if (isDefault) {
                this.saatcUtil.configureDefaultPath(savedPaths, target);
                this.saatcUtil.writeConfigFile(fullFile);
            }
        } else {
            if (Object.entries(savedPaths).length === 0) {
                throw new SfdxError('No default target saved, add one first');
            }
            savedPaths.target = Object.values(savedPaths).find(element => element['isDefault']);
        }
        let command = `sfdx force:org:open -p ${savedPaths.target['uri']}`;
        if (alias) command = command + ` -u ${alias}`;
        const {stdout, stderr} = await asyncexec(command);
        this.ux.log(stderr);
        this.ux.log(stdout);
        return savedPaths.target['uri'];
    }
}

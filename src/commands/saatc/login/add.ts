import { flags, SfdxCommand } from '@salesforce/command';
import { Messages } from '@salesforce/core';
import { AnyJson } from '@salesforce/ts-types';
import * as SAATC_UTIL from '../../../ssatc-util';

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('saatc-sfdx', 'saatc');

export default class Add extends SfdxCommand {
    public static description = messages.getMessage('login.add.description');
    public static examples = [
        messages.getMessage('login.add.example.1')
    ];

    protected static flagsConfig = {
        target : flags.string({
            char : 't',
            description : messages.getMessage('login.add.params.target.description'),
            required : true
        }),
        uri : flags.string({
            char : 'u',
            description : messages.getMessage('login.add.params.uri.description'),
            required : true
        })
    };

    protected static requiresUsername = false;
    protected static requiresProject = true;
    protected saatcUtil: SAATC_UTIL.SAATCUtil = new SAATC_UTIL.SAATCUtil();

    public async run(): Promise<AnyJson> {
        const target: string = this.flags.target;
        const isDefault = false;
        const uri: string = this.flags.uri;
        try {
            this.saatcUtil.addSavedPath({target, uri, isDefault});
        } catch (e) {
            const result = await this.ux.prompt(messages.getMessage('login.add.prompt.1'));
            if (result.toLowerCase() === 'y') {
                try {
                    this.saatcUtil.addSavedPath({target, uri, isDefault}, true);
                } catch (f) {
                    return messages.getMessage('login.add.aborted');
                }
            }
        }
        const message = messages.getMessage('login.add.success', [target]);
        this.ux.log(message);
        return message;
    }
}

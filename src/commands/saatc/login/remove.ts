import { flags, SfdxCommand } from '@salesforce/command';
import { Messages, SfdxError } from '@salesforce/core';
import { AnyJson } from '@salesforce/ts-types';
import * as SAATC_UTIL from '../../../ssatc-util';
import * as saatcTypes from '../../../types';

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('saatc-sfdx', 'saatc');

export default class Remove extends SfdxCommand {
    public static description = messages.getMessage('login.remove.description');
    public static examples = [
        messages.getMessage('login.remove.example.1')
    ];

    protected static flagsConfig = {
        target : flags.string({
            char : 't',
            description : messages.getMessage('login.remove.params.target.description'),
            required : true
        })
    };
    protected static requiresUsername = false;
    protected static requiresProject = true;
    protected saatcUtil: SAATC_UTIL.SAATCUtil = new SAATC_UTIL.SAATCUtil();

    public async run(): Promise<AnyJson> {
        const target = this.flags.target;
        const fullFile = this.saatcUtil.getConfigFile();
        const savedPaths: saatcTypes.Path_Dictionary = fullFile.savedPaths;
        if (savedPaths[target] === undefined) throw new SfdxError(messages.getMessage('login.remove.error.targetDoesNotExist'));

        delete savedPaths[target];
        this.saatcUtil.writeConfigFile(fullFile);
        const message = messages.getMessage('login.remove.finished', [target]);
        this.ux.log(message);
        return message;
    }
}

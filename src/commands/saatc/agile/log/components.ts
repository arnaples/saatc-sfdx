import { flags, SfdxCommand } from '@salesforce/command';
import { Messages, SfdxError } from '@salesforce/core';
import { Tokens } from '@salesforce/core/lib/messages';
import { AnyJson } from '@salesforce/ts-types';
import { exec } from 'child_process';
import * as util from 'util';
import * as SAATC_UTIL from '../../../../ssatc-util';

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('saatc-sfdx', 'saatc');

export default class Components extends SfdxCommand {
    public static description = messages.getMessage('agile.log.components.description');
    public static examples = [
        messages.getMessage('agile.log.components.example.1'),
        messages.getMessage('agile.log.components.example.2')
    ];

    protected static flagsConfig = {
        manualconfigurations : flags.boolean({
            char: 'M',
            description: messages.getMessage('agile.log.components.manualconfigurations.description')
        }),
        script: flags.boolean({
            description: messages.getMessage('flags.script.description')
        })
    };
    protected static requiresUsername = false;
    protected static requiresProject = true;
    protected refId = 0;
    protected saatcUtil: SAATC_UTIL.SAATCUtil = new SAATC_UTIL.SAATCUtil();

    public async run(): Promise<AnyJson> {
        const spinner = new SAATC_UTIL.Spinner(this.ux, this.flags.script);
        spinner.startSpinner('saatc:agile:log:components...');
        const interactive: boolean = this.flags.manualconfigurations;
        const config = this.saatcUtil.getConfigFile();
        spinner.setSpinnerStatus(messages.getMessage('spinner.checkingconfigurations'));
        if (! config?.aliases[SAATC_UTIL.ConfigSettings.agile]) {
            throw new SfdxError(
                messages.getMessage('common.error.confignotset'),
                'confignotset',
                [
                    messages.getMessage('common.error.confignotset.try.1'),
                    messages.getMessage('agile.log.components.example.1')
                ],
                1
            );
        }
        spinner.setSpinnerStatus(messages.getMessage('spinner.gettingworkname'));
        let branchName: string;
        const fileName = await this.saatcUtil.getFileName();
        if (fileName?.status === 1) {
            throw new SfdxError(
                messages.getMessage('common.error.branchmanifestnotfound'),
                'retrievefilename',
                [
                    messages.getMessage('common.error.branchmanifestnotfound.try.1')
                ],
                1
            );
        } else {
            branchName = fileName?.result;
        }
        const agileAlias = config.aliases[SAATC_UTIL.ConfigSettings.agile];
        spinner.setSpinnerStatus(messages.getMessage('spinner.getworkid'));
        const getWorkIdResult = await this.saatcUtil.getWorkId(branchName, agileAlias);
        if (getWorkIdResult['status'] === 1) {
            const errorTokens: Tokens = [getWorkIdResult['result']];
            throw new SfdxError(
                messages.getMessage('common.error.workidnotretrieved', errorTokens),
                'retrieveworkid'
            );
        }
        const workId: string = getWorkIdResult['result'];
        spinner.setSpinnerStatus(messages.getMessage('spinner.checkingconfigurations'));
        const packageJSON = this.saatcUtil.getJSONPackage(this.saatcUtil.getManifestFilePath(branchName));
        spinner.setSpinnerStatus(messages.getMessage('spinner.fetchingcomponents'));
        const getExistingTreeResponse = await this.saatcUtil.getExistingTree(branchName, agileAlias);
        if (getExistingTreeResponse['status'] === 1) {
            throw new SfdxError(
                getExistingTreeResponse['result'],
                'retrieveexistingtree'
            );
        }
        spinner.setSpinnerStatus(messages.getMessage('spinner.addingreleasecomponents'));
        const existingTree: AnyJson = getExistingTreeResponse['result'];
        const types = packageJSON.Package?.[0]?.types ?? [];
        let verifiedCodeCoverage = false;
        const treeLeaves = [];
        if (! this.flags.script) {
            if (types.some(type => ['ApexPage', 'ApexClass', 'ApexTrigger'].includes(type.name))) {
                const input = await this.ux.prompt(messages.getMessage('agile.log.components.prompt.needscodecoverage'), {type: 'single', default: 'n'});
                if (input === 'y') verifiedCodeCoverage = true;
            }
        }
        types.forEach(type => {
            type.members.forEach(member => {
                if (!Array.from(existingTree['records']).some(element => element['Component_API_Name__c'] === member)) {
                    treeLeaves.push(new ReleaseComponent(this.refId++, workId, member, type['name'], verifiedCodeCoverage));
                }
            });
        });
        if (treeLeaves.length === 0) {
            const exitMessage = messages.getMessage('agile.log.components.statusquo');
            spinner.stopSpinner(exitMessage);
            if (this.flags.script) {
                this.exit(2);
            } else {
                return {
                    status: 2,
                    result: exitMessage
                };
            }
        }
        spinner.setSpinnerStatus(messages.getMessage('spinner.writingreleasecomponents'));
        const tree = new SAATC_UTIL.Tree(treeLeaves);
        if (interactive) {
            spinner.setSpinnerStatus(messages.getMessage('spinner.awaitinginput'));
            await this.doInteractiveSteps();
        }

        spinner.setSpinnerStatus(messages.getMessage('spinner.pushingreleasecomponents'));
        const treeImportResponse = await tree.importTree(agileAlias);
        if (treeImportResponse['status'] === 1) {
            throw new SfdxError(
                treeImportResponse['result'],
                'importtree'
            );
        }
        const successTokens: Tokens = [branchName];
        const message = messages.getMessage('agile.log.components.success', successTokens);
        spinner.stopSpinner(message);
        if (this.flags.script) {
            this.exit(0);
        } else {
            return {
                status: 0,
                result: message
            };
        }
    }
    private async doInteractiveSteps(): Promise<void> {
        const asyncexec = util.promisify(exec);
        this.ux.pauseSpinner( () => {
            this.ux.log(messages.getMessage('spinner.awaitingeditorclose'));
        });
        await asyncexec('code ./.saatc/treeImport.json -r -w');
    }
}
class ReleaseComponent extends SAATC_UTIL.TreeRecordBase {
    public COMPONENT_TYPE__C: string;
    public NAME: string;
    public VERIFIED_CODE_COVERAGE__C: boolean;
    public BUG_STORY__C: string;
    public COMPONENT_API_NAME__C: string;
    public NEW_COMPONENT__C: boolean;
    public DEPLOYMENT_NOTES__C: string;
    public MANUAL_CONFIGURATION_NOTES__C: string;
    public PRE_POST_MANUAL_CONFIG__C: string;
    public MANUAL_CONFIGURATION__C: boolean;
    public SHARED_COMPONENT__C: boolean;
    public DEVELOPER_S_DESCRIPTION__C: string;
    constructor(instanceId: number, workId: string, componentAPI: string, componentType: string, codeCoverageVerified: boolean) {
        super('Release_Component__c', `Release_ComponentRef${instanceId}`);
        this.COMPONENT_TYPE__C = componentType;
        this.NAME = componentAPI.slice(0, 80);
        this.VERIFIED_CODE_COVERAGE__C = codeCoverageVerified;
        this.BUG_STORY__C = workId;
        this.COMPONENT_API_NAME__C = componentAPI;
        this.NEW_COMPONENT__C = false;
        this.DEPLOYMENT_NOTES__C = '';
        this.MANUAL_CONFIGURATION_NOTES__C = '';
        this.PRE_POST_MANUAL_CONFIG__C = '';
        this.MANUAL_CONFIGURATION__C = false;
        this.SHARED_COMPONENT__C = false;
        this.DEVELOPER_S_DESCRIPTION__C = 'Created using the saatc sfdx-cli plugin';
    }
}

import { core, flags, SfdxCommand } from '@salesforce/command';
import { Messages, Org, SfdxError } from '@salesforce/core';
import { Tokens } from '@salesforce/core/lib/messages';
import { AnyJson } from '@salesforce/ts-types';
import { Quality_Control__c } from '../../../../classes';
import * as SAATC_UTIL from '../../../../ssatc-util';
import { Validation_Result } from '../../../../types';

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('saatc-sfdx', 'saatc');

export default class Components extends SfdxCommand {
    public static description = messages.getMessage('agile.log.validate.description');
    public static examples = [
        messages.getMessage('agile.log.validate.example.1')
    ];

    protected static flagsConfig = {
        script: flags.boolean({
            description: messages.getMessage('flags.script.description')
        })
    };
    protected static requiresUsername = false;
    protected static requiresProject = true;
    protected refId = 0;
    protected saatcUtil: SAATC_UTIL.SAATCUtil = new SAATC_UTIL.SAATCUtil();

    public async run(): Promise<AnyJson> {
        const spinner = new SAATC_UTIL.Spinner(this.ux, this.flags.script);
        spinner.startSpinner('saatc:agile:log:validate...');
        const config = this.saatcUtil.getConfigFile();

        spinner.setSpinnerStatus(messages.getMessage('spinner.checkingconfigurations'));
        if (! config?.aliases[SAATC_UTIL.ConfigSettings.agile]) {
            throw new SfdxError(
                messages.getMessage('common.error.confignotset'),
                'confignotset',
                [
                    messages.getMessage('common.error.confignotset.try.1')
                ],
                1
            );
        }

        spinner.setSpinnerStatus(messages.getMessage('spinner.refreshconnection'));
        const agileAlias = config.aliases[SAATC_UTIL.ConfigSettings.agile];
        const agileOrg = await Org.create({aliasOrUsername: agileAlias});
        await agileOrg.refreshAuth();
        const agileConenction = agileOrg.getConnection();
        const agileQC = agileConenction.sobject('Quality_Control__c');

        spinner.setSpinnerStatus(messages.getMessage('spinner.gettingworkname'));
        const fileName = await this.saatcUtil.getFileName();
        if (fileName?.status === 1) {
            throw new SfdxError(
                messages.getMessage('common.error.branchnotformattedcorrectly'),
                'retrievefilename',
                [
                    messages.getMessage('common.error.branchnotformattedcorrectly.try.1')
                ],
                1
            );
        }
        const workName: string = fileName?.result;

        spinner.setSpinnerStatus(messages.getMessage('spinner.getworkid'));
        const getWorkIdResult = await this.saatcUtil.getWorkId(workName, agileAlias);
        if (getWorkIdResult['status'] === 1) {
            const errorTokens: Tokens = [getWorkIdResult['result']];
            throw new SfdxError(
                messages.getMessage('common.error.workidnotretrieved', errorTokens),
                'retrieveworkid',
                [
                    messages.getMessage('common.error.workidnotretrieved.try.1')
                ],
                1
            );
        }
        const workId: string = getWorkIdResult['result'];

        spinner.setSpinnerStatus(messages.getMessage('spinner.fetchingvalidationattempt'));
        const validationId = config.jobResults[workName]?.validateId[0];

        if (validationId === undefined) {
            throw new SfdxError(
                messages.getMessage('agile.log.validate.error.nopreviousvalidation'),
                'validationnotattempted',
                [
                    messages.getMessage('agile.log.validate.error.nopreviousvalidation.try.1')
                ],
                1
            );
        }

        const validationContent = core.fs.readJsonSync(this.saatcUtil.validationResultFilePath.replace('%s', validationId)) as unknown as Validation_Result;

        const qualityControlRecord: Quality_Control__c = new Quality_Control__c(
            validationId,
            'Validation',
            {
                Work__c: workId,
                Output__c: JSON.stringify(validationContent.result.details.componentFailures, undefined, 2),
                Status__c: validationContent.result.success ? 'Pass' : 'Failed'
            }
        );
        agileQC.create(qualityControlRecord).catch (reason => {
            console.error(reason);
            throw new SfdxError(reason);
        });

        if (this.flags.script) {
            this.exit(0);
        } else {
            const message = messages.getMessage('agile.log.validate.success');
            this.ux.log(message);
            return {
                status: 0,
                result: message
            };
        }
    }
}

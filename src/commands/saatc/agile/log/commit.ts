import { flags, SfdxCommand } from '@salesforce/command';
import { Messages, SfdxError } from '@salesforce/core';
import { Tokens } from '@salesforce/core/lib/messages';
import { AnyJson } from '@salesforce/ts-types';
import * as SAATC_UTIL from '../../../../ssatc-util';

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('saatc-sfdx', 'saatc');

export default class Commit extends SfdxCommand {
    public static description = messages.getMessage('agile.log.commit.description');
    public static examples = [
        messages.getMessage('agile.log.commit.example.1'),
        messages.getMessage('agile.log.commit.example.2')
    ];

    protected static flagsConfig = {
        commit: flags.string({
            char: 'c',
            description: messages.getMessage('agile.log.commit.flags.commit.description'),
            required: true
        }),
        message: flags.string({
            char: 'm',
            description: messages.getMessage('agile.log.commit.flags.message.description')
        }),
        script: flags.boolean({
            description: messages.getMessage('flags.script.description')
        })
    };
    protected static requiresUsername = false;
    protected static requiresProject = true;
    protected saatcUtil: SAATC_UTIL.SAATCUtil = new SAATC_UTIL.SAATCUtil();

    public async run(): Promise<AnyJson> {
        const spinner = new SAATC_UTIL.Spinner(this.ux, this.flags.script);
        spinner.startSpinner('saatc:agile:log:commit...');
        const commitHash: string = this.flags.commit;
        const config = this.saatcUtil.getConfigFile();
        spinner.setSpinnerStatus(messages.getMessage('spinner.checkingconfigurations'));
        if (! config?.aliases[SAATC_UTIL.ConfigSettings.agile]) {
            throw new SfdxError(
                messages.getMessage('common.error.confignotset'),
                'confignotset',
                [
                    messages.getMessage('common.error.confignotset.try.1')
                ],
                1
            );
        }
        const getBranchNameResult = await this.saatcUtil.getBranchName();
        if (getBranchNameResult.status === 1) {
            throw new SfdxError(getBranchNameResult.result);
        }
        const branchName: string = getBranchNameResult.result;
        spinner.setSpinnerStatus(messages.getMessage('spinner.gettingworkname'));
        const getWorkNameResult = await this.saatcUtil.getFileName();
        if (getWorkNameResult?.status === 1) {
            throw new SfdxError(getWorkNameResult.result);
        }
        const workName: string = getWorkNameResult?.result;
        const agileAlias = config.aliases[SAATC_UTIL.ConfigSettings.agile];
        const getWorkIdResult = await this.saatcUtil.getWorkId(workName, agileAlias);
        if (getWorkIdResult['status'] === 1) {
            const errorTokens: Tokens = [getWorkIdResult['result']];
            throw new SfdxError(messages.getMessage('common.error.unknownretrieve', errorTokens) );
        }
        const workId: string = getWorkIdResult['result'];
        const treeLeaves = [new CommitRecord(workId, commitHash, branchName, this.flags.message)];
        spinner.setSpinnerStatus(messages.getMessage('spinner.writingcommit'));
        const tree = new SAATC_UTIL.Tree(treeLeaves);
        spinner.setSpinnerStatus(messages.getMessage('spinner.pushingcommit'));

        const treeImportResponse = await tree.importTree(agileAlias);
        if (treeImportResponse['status'] === 1) {
            throw new SfdxError(treeImportResponse['result']);
        }
        spinner.stopSpinner(messages.getMessage('spinner.commitrecorded'));
        const successTokens: Tokens = [workName];
        const message = messages.getMessage('agile.log.commit.success', successTokens);
        this.ux.log(message);
        if (this.flags.script) {
            this.exit(0);
        } else {
            return {
                status: 0,
                result: message
            };
        }
    }
}

class CommitRecord extends SAATC_UTIL.TreeRecordBase {
    public BITBUCKET_COMMIT_BRANCHNAME__C: string;
    public BITBUCKET_COMMIT_MESSAGE__C: string;
    public BITBUCKET_COMMIT_HASHID__C: string;
    public WORKRECORD__C: string;
    constructor(workId: string, commitHash: string, branchName: string, commitMessage: string) {
        super('Bitbucket_Commit__c', 'Release_ComponentRef0');
        this.WORKRECORD__C = workId;
        this.BITBUCKET_COMMIT_HASHID__C = commitHash;
        this.BITBUCKET_COMMIT_BRANCHNAME__C = branchName;
        this.BITBUCKET_COMMIT_MESSAGE__C = commitMessage;
    }
}

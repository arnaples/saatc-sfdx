import { SfdxCommand } from '@salesforce/command';
import { Messages } from '@salesforce/core';
import { AnyJson } from '@salesforce/ts-types';
import * as SAATC_UTIL from '../../../../ssatc-util';

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('saatc-sfdx', 'saatc');

export default class Test extends SfdxCommand {
    public static description = 'This command is still under construction';
    public static examples = [
        messages.getMessage('build.package.example.1'),
        messages.getMessage('build.package.example.2')
    ];

    protected static requiresUsername = false;
    protected static requiresProject = true;
    protected saatcUtil: SAATC_UTIL.SAATCUtil = new SAATC_UTIL.SAATCUtil();

    public async run(): Promise<AnyJson> {
        const spinner = new SAATC_UTIL.Spinner(this.ux, this.flags.script);
        spinner.startSpinner('saatc:manifest:test...');
        spinner.setSpinnerStatus('getting branch name');
        return {
            status: 0,
            result: 'This command is still under construction'
        };
    }
}

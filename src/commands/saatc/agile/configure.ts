import { flags, SfdxCommand } from '@salesforce/command';
import { Messages, SfdxError } from '@salesforce/core';
import { Tokens } from '@salesforce/core/lib/messages';
import { AnyJson } from '@salesforce/ts-types';
import { exec } from 'child_process';
import * as util from 'util';
import * as SAATC_UTIL from '../../../ssatc-util';

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('saatc-sfdx', 'saatc');

export default class Configure extends SfdxCommand {
    public static description = messages.getMessage('agile.configure.description');
    public static examples = [
        messages.getMessage('agile.configure.example.1')
    ];

    protected static flagsConfig = {
        alias : flags.string({
            char : 'u',
            description : messages.getMessage('agile.configure.params.alias.description'),
            required : true
        }),
        setting: flags.enum({
            description: messages.getMessage('agile.configure.params.setting.description'),
            char: 's',
            options: Object.keys(SAATC_UTIL.ConfigSettings),
            required: true
        })
    };

    protected static requiresUsername = false;
    protected static requiresProject = false;
    protected saatcUtil: SAATC_UTIL.SAATCUtil = new SAATC_UTIL.SAATCUtil();

    public async run(): Promise<AnyJson> {
        const targetAlias: string = this.flags.alias;
        const targetSetting: string = this.flags.setting;
        const asyncexec = util.promisify(exec);
        const tokens: Tokens = [targetSetting, targetAlias];
        const currentSFDXCLIVersion = await this.saatcUtil.getSFDXCLIVersion();
        if (currentSFDXCLIVersion['status'] === 1) {
            throw new SfdxError('There was a problem getting the current CLI version');
        }
        const authCommand = currentSFDXCLIVersion['result'] < '7.77.1' ? 'sfdx force:auth:list --json' : 'sfdx auth:list --json';
        const {stdout, stderr} = await asyncexec(authCommand);
        let isTargetAliasAuthorized = false;
        if (stderr && this.saatcUtil.isTrueError(stderr)) {
            const unknownTokens: Tokens = [stderr];
            throw new SfdxError(messages.getMessage('agile.configure.error.authlist', unknownTokens));
        }
        const authResult: JSON = JSON.parse(stdout);
        if (authResult?.['status'] === 0) {
            const results: JSON[] = authResult['result'];
            isTargetAliasAuthorized = results.some(element => element['alias'] === targetAlias);
        }
        if (!isTargetAliasAuthorized) {
            throw new SfdxError(
                messages.getMessage('agile.configure.error.aliasnotauthorized'),
                'Unknown target alias',
                [
                    messages.getMessage('agile.configure.error.aliasnotauthorized.try.1', tokens)
                ],
                1
            );
        }
        const configFile = this.saatcUtil.getConfigFile();
        configFile.aliases[SAATC_UTIL.ConfigSettings[targetSetting]] = targetAlias;
        this.saatcUtil.writeConfigFile(configFile);
        const message = messages.getMessage('agile.configure.success', tokens);
        this.ux.log(message);

        // before we wrap up, just check the other expected configs to see if they are set. Warn the user if not.
        Object.keys(SAATC_UTIL.ConfigSettings).forEach(_configKey => {
            const warningTokens = [_configKey, targetAlias, _configKey];
            if (! configFile.aliases[SAATC_UTIL.ConfigSettings[_configKey]] ) this.ux.warn(messages.getMessage('agile.configure.warn.aliasnotset', warningTokens));
        });
        return {
            status: 0,
            result: message
        };
    }
}

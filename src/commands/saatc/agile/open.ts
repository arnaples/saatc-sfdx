import { SfdxCommand } from '@salesforce/command';
import { Messages, SfdxError } from '@salesforce/core';
import { AnyJson } from '@salesforce/ts-types';
import { exec } from 'child_process';
import * as util from 'util';
import * as SAATC_UTIL from '../../../ssatc-util';

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('saatc-sfdx', 'saatc');

export default class Open extends SfdxCommand {
    public static description = messages.getMessage('agile.open.description');
    public static examples = [
        messages.getMessage('agile.open.example.1')
    ];

    protected static supportsUsername = false;
    protected static requiresProject = true;
    protected saatcUtil: SAATC_UTIL.SAATCUtil = new SAATC_UTIL.SAATCUtil();

    public async run(): Promise<AnyJson> {
        const spinner = new SAATC_UTIL.Spinner(this.ux, false);
        const config = this.saatcUtil.getConfigFile();
        const fileName = await this.saatcUtil.getFileName();
        let branchName: string;
        if (fileName?.status === 1) {
            throw new SfdxError(
                messages.getMessage('common.error.branchnotformattedcorrectly'),
                'retrievefilename',
                [
                    messages.getMessage('common.error.branchnotformattedcorrectly.try.1')
                ],
                1
            );
        } else {
            branchName = fileName?.result;
        }
        const agileAlias = config.aliases[SAATC_UTIL.ConfigSettings.agile];
        spinner.setSpinnerStatus(messages.getMessage('spinner.getworkid'));
        const getWorkIdResult = await this.saatcUtil.getWorkId(branchName, agileAlias);
        if (getWorkIdResult['status'] === 1) {
            const errorTokens = [getWorkIdResult['result']];
            throw new SfdxError(
                messages.getMessage('common.error.workidnotretrieved', errorTokens),
                'retrieveworkid',
                [
                    messages.getMessage('common.error.workidnotretrieved.try.1')
                ],
                1
            );
        }
        const workId: string = getWorkIdResult['result'];
        const asyncexec = util.promisify(exec);
        const {stdout} = await asyncexec(`sfdx force:org:open -p /lightning/r/agf__ADM_Work__c/${workId}/view -u ${agileAlias}`);
        return stdout;
    }
}

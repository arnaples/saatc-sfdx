import { core, flags, SfdxCommand } from '@salesforce/command';
import { Messages, SfdxError } from '@salesforce/core';
import { AnyJson } from '@salesforce/ts-types';
import * as SAATC_UTIL from '../../../ssatc-util';

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('saatc-sfdx', 'saatc');

export default class Open extends SfdxCommand {
    public static description = messages.getMessage('agile.report.description');
    public static examples = [
        messages.getMessage('agile.report.example.1'),
        messages.getMessage('agile.report.example.2')
    ];

    protected static flagsConfig = {
        validation : flags.boolean({
            description: messages.getMessage('agile.report.validation.description')
        }),
        test : flags.boolean({
            description: messages.getMessage('agile.report.test.description')
        })
    };
    protected static supportsUsername = false;
    protected static requiresProject = true;
    protected saatcUtil: SAATC_UTIL.SAATCUtil = new SAATC_UTIL.SAATCUtil();

    public async run(): Promise<AnyJson> {
        const spinner = new SAATC_UTIL.Spinner(this.ux, false);
        const config = this.saatcUtil.getConfigFile();
        const fileName = await this.saatcUtil.getFileName();
        let workName: string;
        if (fileName?.status === 1) {
            throw new SfdxError(
                fileName.result,
                'retrievefilename',
                [
                    messages.getMessage('agile.log.components.example.1')
                ],
                1
            );
        } else {
            workName = fileName?.result;
        }
        const agileAlias = config.aliases[SAATC_UTIL.ConfigSettings.agile];
        spinner.setSpinnerStatus(messages.getMessage('spinner.getworkid'));
        const getWorkIdResult = await this.saatcUtil.getWorkId(workName, agileAlias);
        if (getWorkIdResult['status'] === 1) {
            const errorTokens = [getWorkIdResult['result']];
            throw new SfdxError(
                messages.getMessage('agile.log.components.error.unknown', errorTokens),
                'retrieveworkid',
                [
                    messages.getMessage('agile.log.components.example.1')
                ],
                1
            );
        }
        const workId: string = getWorkIdResult['result'];
        const reportHighlights: Array<{label: string, value: string}> = [];
        reportHighlights.push({
            label: 'Work Id',
            value: workId
        });
        reportHighlights.push({
            label: 'Work Name',
            value: workName
        });
        const expectedManifestPath = this.saatcUtil.getManifestFilePath(workName);
        reportHighlights.push({
            label: 'Manifest File Path',
            value: core.fs.fileExistsSync(expectedManifestPath) ? expectedManifestPath : undefined
        });
        const expectedValidationPath = this.saatcUtil.validationResultFilePath.replace('%s', config.jobResults[workName]?.validateId[0]);
        reportHighlights.push({
            label: 'Validation File Path',
            value: core.fs.fileExistsSync(expectedValidationPath) ? expectedValidationPath : undefined
        });
        const expectedTestPath = this.saatcUtil.testResultFilePath.replace('%s', config.jobResults[workName]?.testId[0]);
        reportHighlights.push({
            label: 'Test File Path',
            value: core.fs.fileExistsSync(expectedTestPath) ? expectedTestPath : undefined
        });
        this.ux.table(reportHighlights, ['label', 'value']);
        return reportHighlights;
    }
}

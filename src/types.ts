interface QueryResult {
    totalSize: number;
    done: boolean;
    records: Record[];
}

interface Record {
    attributes?: RecordAttributes;
    ID?: string;
    RECORDTYPEID?: string;
    NAME?: string;
}

interface RecordAttributes {
    type: string;
    referenceId?: string;
}

interface AgfADMWork extends Record {
    AGF__FOUND_IN_BUILD__C?: string;
    AGF__PRIORITY__C?: string;
    agf__Product_Tag__c?: string;
    agf__Status__c?: string;
    agf__Subject__c?: string;
}

interface QualityControl extends Record {
    External_Job_Id__c?: string;
    Output__c?: string;
    Status__c?: string;
    Type__c?: string;
    Work__c?: string;
}

interface SAATCConfig {
    aliases: {
        [connection: string]: string;
    };
    cachedWorkIds: {
        [workNumber: string]: string;
    };
    defaultAPI: string;
    gitIgnored: boolean;
    ValidationResultsToKeep: number;
    TestResultsToKeep: number;
    jobResults: {
        [workNumber: string]: {
            validateId: string[];
            testId: string[];
        }
    };
    savedPaths: PathDictionary;
}

interface PathDictionary {
    [target: string]: {
        uri: string;
        isDefault: boolean;
    };
}

interface ValidationResult {
    status: number;
    result: {
        checkOnly: boolean,
        completedDate: Date,
        createdBy: string,
        createdByName: string,
        createdDate: Date,
        details: {
            componentSuccesses?: ValidationComponentBase[],
            componentFailures?: ValidationComponentFailure[],
            runTestResult: {
                numFailures: string,
                numTestsRun: string,
                totalTime: string
            }
        },
        done: boolean,
        id: string,
        ignoreWarnings: boolean,
        lastModifiedDate: Date,
        numberComponentErrors: number,
        numberComponentsDeployed: number,
        numberComponentsTotal: number,
        numberTestErrors: number,
        numberTestsCompleted: number,
        numberTestsTotal: number,
        rollbackOnError: boolean,
        runTestsEnabled: boolean,
        startDate: Date,
        status: string,
        success: boolean
    };
}

interface ValidationComponentBase {
    changed: string;
    componentType: string;
    created: string;
    createdDate: Date;
    deleted: string;
    fileName: string;
    fullName: string;
    success: string;
}
interface ValidationComponentFailure extends ValidationComponentBase {
    columnNumber: string;
    lineNumber: string;
    problem: string;
    problemType: string;
}

interface TestResult {
    status: number;
    result: {
        summary: TestResultSummary;
        tests: TestResultClass[];
    };
}

interface TestResultSummary {
    outcome: string;
    testsRan: number;
    passing: number;
    failing: number;
    skipped: number;
    passRate: string;
    failRate: string;
    testStartTime: string;
    testExecutionTime: string;
    testTotalTime: string;
    commandTime: string;
    hostname: string;
    orgId: string;
    username: string;
    testRunId: string;
    userId: string;
}
interface TestResultClass {
    Id: string;
    QueueItemId: string;
    StackTrace: string|null;
    Message: string|null;
    AsyncApexJobId: string;
    MethodName: string;
    Outcome: string;
    ApexClass: {
        Id: string;
        Name: string;
        NamespacePrefix: string|null
    };
    RunTime: number;
    FullName: string;
}

interface Manifest {
    _declaration: {
        _attributes: {
            version: string,
            encoding: string,
            standalone: string
        }
    };
    Package: Package[];
}

interface Package {
    _attributes: {
        xmlns: string
    };
    types?: PackageMember[];
    version: string;
}

interface PackageMember {
    name: string;
    members?: string[];
}

interface PackageFromXML {
    version?: [{
        _text?: string[]
    }];
    types?: [{
        members: [{
            _text?: string[]
        }],
        name: [{
            _text?: string[]
        }]
    }];
}

interface CommitConfigs {
    manifest: Manifest;
    commitHash: string;
    fromCommitHash?: string;
}

export {
    AgfADMWork as agf__ADM_Work__c,
    Manifest,
    Package,
    PackageFromXML,
    PackageMember,
    PathDictionary as Path_Dictionary,
    QualityControl as Quality_Control__c,
    QueryResult,
    Record,
    RecordAttributes,
    TestResult as Test_Result,
    TestResultClass as Test_Result_Class,
    TestResultSummary as Test_Result_Summary,
    SAATCConfig as SAATC_Config,
    CommitConfigs as Commit_Configs,
    ValidationResult as Validation_Result
};

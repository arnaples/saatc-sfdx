saatc-sfdx
==========

>Note: The commands within the `saatc:agile` topic are developed for a particular configuration of Salesforce's Agile Accelerator (agf__). These commands may not work in all instances.

All `sfdx saatc` commands support the -h attribute; which when provided will display a description, list of further topics, list of commands, list of options, and/or examples of how to use this command.

## Integrating with Agile Accelerator
### Steps to follow
1. `git commit`
2. `sfdx saatc:agile:commit`
3. `sfdx saatc:agile:components`

## Login notes
The `sfdx saatc:login` command is an exstension of the `sfdx force:org:open` command with the added support of bookmarking your on redirect paths.


Plugin for use with SFDX

[![Version](https://img.shields.io/npm/v/saatc-sfdx.svg)](https://npmjs.org/package/saatc-sfdx)
[![CircleCI](https://circleci.com/gh/arnaples/saatc-sfdx/tree/master.svg?style=shield)](https://circleci.com/gh/arnaples/saatc-sfdx/tree/master)
[![Appveyor CI](https://ci.appveyor.com/api/projects/status/github/arnaples/saatc-sfdx?branch=master&svg=true)](https://ci.appveyor.com/project/heroku/saatc-sfdx/branch/master)
[![Codecov](https://codecov.io/gh/arnaples/saatc-sfdx/branch/master/graph/badge.svg)](https://codecov.io/gh/arnaples/saatc-sfdx)
[![Greenkeeper](https://badges.greenkeeper.io/arnaples/saatc-sfdx.svg)](https://greenkeeper.io/)
[![Known Vulnerabilities](https://snyk.io/test/github/arnaples/saatc-sfdx/badge.svg)](https://snyk.io/test/github/arnaples/saatc-sfdx)
[![Downloads/week](https://img.shields.io/npm/dw/saatc-sfdx.svg)](https://npmjs.org/package/saatc-sfdx)
[![License](https://img.shields.io/npm/l/saatc-sfdx.svg)](https://github.com/arnaples/saatc-sfdx/blob/master/package.json)

<!-- usagestop -->

<!-- changelog -->
## Change Log
### 2.4.4
* fixed bug where linting caused a bug with regex
### 2.4.3
* fixed bug where linting caused a bug with regex
### 2.4.2
* fixed bug where linting caused a bug with regex
### 2.4.1
* fixed tsc compile issue
* conformed to jsForce record inserts
* started working on test classes
* updated/added dependencies
### 2.4.0
* resolved a basically all tslint errors
### 2.3.3
* hopefully the last bug squashed from allowing the naming convention. Was trying to open and build a commit at a malformed uri.
### 2.3.2
* continued to squash bugs related to files at the project root
### 2.3.1
* fixed a bug where files were being created at the project root
### 2.3
* added the ability to set the destination and naming convention of the built manifest.

  `sfdx saatc:manifest:build -p ./path/to/new/manifest.xml`

### 2.2
* added the ability to set declare from where to build the manifest _from_. If not declared, it will still build from the commit immediately prior to that passed in via the --commit parameter.

  `sfdx saatc:manifest:build -c HEAD -f <somePreviousCommit>`

## Command Reference (autobuilt via npm)
<!-- commands -->
* [`sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagile---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:configure -u <string> -s agile|validate|test [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagileconfigure--u-string--s-agilevalidatetest---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:commit -c <string> [-m <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogcommit--c-string--m-string---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogcomponents--m---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:tests [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogtests---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:validate [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogvalidate---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:open [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagileopen---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:report [--validation] [--test] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilereport---validation---test---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbamboolog--i-string---script--e-string--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:bamboo:poll -j <string> [-e <string>] [--script] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbamboopoll--j-string--e-string---script--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:cleanup [--all | -V | -T | -W | -A] [--noprompt] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatccleanup---all---v---t---w---a---noprompt---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatclogin--u-string--t-string--d---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginadd--t-string--u-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginremove--t-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:build [-v <string>] [-W] [-B -c <string>] [-f <string>] [--nopackage] [-p <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestbuild--v-string--w--b--c-string--f-string---nopackage--p-string---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:deploy [-u <string>] [--apiversion <string>] [--quiet] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestdeploy--u-string---apiversion-string---quiet---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:show [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestshow---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:test [-C] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifesttest--c---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:test:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifesttestreport---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:validate [-W] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestvalidate--w---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:validate:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestvalidatereport---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)

## `sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

retrieve the defined org alias for agile accelerator

```
retrieve the defined org alias for agile accelerator

USAGE
  $ sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile
```

## `sfdx saatc:agile:configure -u <string> -s agile|validate|test [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

set the org alias for the deployment related orgs

```
set the org alias for the deployment related orgs

USAGE
  $ sfdx saatc:agile:configure -u <string> -s agile|validate|test [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -s, --setting=(agile|validate|test)                                               (required) the setting to write to

  -u, --alias=alias                                                                 (required) the alias or username of
                                                                                    the org that for the target setting

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile:configure -s agile -u myAgileOrg
```

## `sfdx saatc:agile:log:commit -c <string> [-m <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will record the commit information to agile accelerator

```
will record the commit information to agile accelerator

USAGE
  $ sfdx saatc:agile:log:commit -c <string> [-m <string>] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -c, --commit=commit                                                               (required) record the specified
                                                                                    commit hash to agile accelerator

  -m, --message=message                                                             commit message to record

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:log:commit -c 44eaebf64
  sfdx saatc:agile:log:commit --commit=44eaebf647d74b8296f3a1f5593fe1c2d6c95ca6 --message="added button to screen"
```

## `sfdx saatc:agile:log:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will create release_component__c entries for every item in the working branch's manifest file

```
will create release_component__c entries for every item in the working branch's manifest file

USAGE
  $ sfdx saatc:agile:log:components [-M] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -M, --manualconfigurations                                                        start an interactive session to
                                                                                    write manual config steps

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:components
  sfdx saatc:agile:components -M
```

## `sfdx saatc:agile:log:tests [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

This command is still under construction

```
This command is still under construction

USAGE
  $ sfdx saatc:agile:log:tests [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  deprecated
  deprecated
```

## `sfdx saatc:agile:log:validate [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

record the latest validation results to agile accelerator

```
record the latest validation results to agile accelerator

USAGE
  $ sfdx saatc:agile:log:validate [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLE
  sfdx saatc:agile:log:validate
```

## `sfdx saatc:agile:open [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

opens the work item in agile accelerator

```
opens the work item in agile accelerator

USAGE
  $ sfdx saatc:agile:open [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile:open
```

## `sfdx saatc:agile:report [--validation] [--test] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

prints all information related to the current work item

```
prints all information related to the current work item

USAGE
  $ sfdx saatc:agile:report [--validation] [--test] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --test                                                                            print full test results to the
                                                                                    terminal

  --validation                                                                      print full validation results to the
                                                                                    terminal

EXAMPLES
  sfdx saatc:agile:report
  sfdx saatc:agile:report --validation --test
```

## `sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

warning: intended for use only with the bamboo agent. logs job information to agile accelerator

```
warning: intended for use only with the bamboo agent. logs job information to agile accelerator

USAGE
  $ sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] 
  [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -e, --environment=environment                                                     environment which this build was ran
                                                                                    against

  -i, --jobid=jobid                                                                 (required) the id of the validation
                                                                                    job to record

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:bamboo:log -i 007Ar00000aAzGyWAG
  sfdx saatc:bamboo:log --jobid=007Ar00000aAzGyWAG --environment=UAT
```

## `sfdx saatc:bamboo:poll -j <string> [-e <string>] [--script] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

warning: intended for use only with the bamboo agent. queries job status from the target org with details from agile accelerator

```
warning: intended for use only with the bamboo agent. queries job status from the target org with details from agile accelerator

USAGE
  $ sfdx saatc:bamboo:poll -j <string> [-e <string>] [--script] [-u <string>] [--apiversion <string>] [--json] 
  [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -e, --environment=environment                                                     environment which this build was ran
                                                                                    against

  -j, --jobenvironmentusername=jobenvironmentusername                               (required) the alias for the
                                                                                    environment in which the build was
                                                                                    executed

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLE
  sfdx saatc:bamboo:poll -j not.a.realusername@salesforce.com -e Validation
```

## `sfdx saatc:cleanup [--all | -V | -T | -W | -A] [--noprompt] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

remove cached config variables

```
remove cached config variables

USAGE
  $ sfdx saatc:cleanup [--all | -V | -T | -W | -A] [--noprompt] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -A, --alias                                                                       clears alias configurations
  -T, --tests                                                                       remove test results
  -V, --validate                                                                    remove validation results
  -W, --workid                                                                      remove cached work ids
  --all                                                                             remove all configurations
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --noprompt                                                                        skip all deletion confirmations

EXAMPLES
  sfdx saatc:cleanup -V -T
  sfdx saatc:cleanup --all
```

## `sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

logs the user into the currently set org

```
logs the user into the currently set org

USAGE
  $ sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -D, --default                                                                     add a new path to the preferred
                                                                                    paths

  -t, --target=target                                                               redirect the user upon login to the
                                                                                    saved target definition

  -u, --alias=alias                                                                 log the user into the aliased org
                                                                                    declared should that org be
                                                                                    authorized

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:login
  sfdx saatc:login -u myDevOrg -t settings
```

## `sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

add a new path to the saved paths

```
add a new path to the saved paths

USAGE
  $ sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will add the specified
                                                                                    target entry from the saved targets.

  -u, --uri=uri                                                                     (required) will set the uri for the
                                                                                    specified target to redirect to upon
                                                                                    login.

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:add -t setup -u lightning/setup/SetupOneHome/home
```

## `sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

removes a path from the saved login paths

```
removes a path from the saved login paths

USAGE
  $ sfdx saatc:login:remove -t <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will remove the specified
                                                                                    target entry from the saved targets

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:remove -t deprecatedpath
```

## `sfdx saatc:manifest:build [-v <string>] [-W] [-B -c <string>] [-f <string>] [--nopackage] [-p <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:manifest:build [-v <string>] [-W] [-B -c <string>] [-f <string>] [--nopackage] [-p <string>] [--script] 
  [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -B, --commitgeneratedmanifest                                                     commit the generated manifest file
                                                                                    immediately upon creation

  -W, --includeworkingfiles                                                         if set, will include uncommitted
                                                                                    changes into the manifest from your
                                                                                    current working directory

  -c, --commit=commit                                                               add the changes specified by the
                                                                                    commit hash to the manifest

  -f, --fromcommit=fromcommit                                                       if declared, generate the manifest
                                                                                    as a diff from the supplied hash

  -p, --filepath=filepath                                                           if declared, will write the manifest
                                                                                    at the file path

  -v, --api=api                                                                     api version for the built manifest
                                                                                    if different than what is already
                                                                                    declared

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --nopackage                                                                       do not update the package manifest

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:deploy [-u <string>] [--apiversion <string>] [--quiet] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:manifest:deploy [-u <string>] [--apiversion <string>] [--quiet] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --quiet                                                                           nothing emitted stdout

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:show [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:manifest:show [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:test [-C] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

This command is still under construction

```
This command is still under construction

USAGE
  $ sfdx saatc:manifest:test [-C] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -C, --commandonly                                                                 returns the command to run; does not
                                                                                    execute the command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  deprecated
  deprecated
```

## `sfdx saatc:manifest:test:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

deprecated

```
deprecated

USAGE
  $ sfdx saatc:manifest:test:report [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          deprecated

EXAMPLES
  desprecated
  desprecated
```

## `sfdx saatc:manifest:validate [-W] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will run a validation against the configured validation org with the working branch manifest file

```
will run a validation against the configured validation org with the working branch manifest file

USAGE
  $ sfdx saatc:manifest:validate [-W] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -W, --nowait                                                                      execute the validate asynchronously
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:validate:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will record the commit information to agile accelerator

```
will record the commit information to agile accelerator

USAGE
  $ sfdx saatc:manifest:validate:report [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:log:commit -c 44eaebf64
  sfdx saatc:agile:log:commit --commit=44eaebf647d74b8296f3a1f5593fe1c2d6c95ca6 --message="added button to screen"
```
<!-- commandsstop -->
* [`sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagile---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:configure -u <string> -s agile|validate|test [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagileconfigure--u-string--s-agilevalidatetest---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:commit -c <string> [-m <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogcommit--c-string--m-string---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogcomponents--m---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:tests [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogtests---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:validate [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogvalidate---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:open [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagileopen---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:report [--validation] [--test] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilereport---validation---test---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbamboolog--i-string---script--e-string--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:bamboo:poll -j <string> [-e <string>] [--script] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbamboopoll--j-string--e-string---script--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:cleanup [--all | -V | -T | -W | -A] [--noprompt] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatccleanup---all---v---t---w---a---noprompt---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatclogin--u-string--t-string--d---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginadd--t-string--u-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginremove--t-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:build [-v <string>] [-W] [-B -c <string>] [-f <string>] [--nopackage] [-p <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestbuild--v-string--w--b--c-string--f-string---nopackage--p-string---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:deploy [-u <string>] [--apiversion <string>] [--quiet] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestdeploy--u-string---apiversion-string---quiet---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:show [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestshow---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:test [-C] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifesttest--c---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:test:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifesttestreport---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:validate [-W] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestvalidate--w---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:validate:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestvalidatereport---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)

## `sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

retrieve the defined org alias for agile accelerator

```
retrieve the defined org alias for agile accelerator

USAGE
  $ sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile
```

## `sfdx saatc:agile:configure -u <string> -s agile|validate|test [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

set the org alias for the deployment related orgs

```
set the org alias for the deployment related orgs

USAGE
  $ sfdx saatc:agile:configure -u <string> -s agile|validate|test [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -s, --setting=(agile|validate|test)                                               (required) the setting to write to

  -u, --alias=alias                                                                 (required) the alias or username of
                                                                                    the org that for the target setting

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile:configure -s agile -u myAgileOrg
```

## `sfdx saatc:agile:log:commit -c <string> [-m <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will record the commit information to agile accelerator

```
will record the commit information to agile accelerator

USAGE
  $ sfdx saatc:agile:log:commit -c <string> [-m <string>] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -c, --commit=commit                                                               (required) record the specified
                                                                                    commit hash to agile accelerator

  -m, --message=message                                                             commit message to record

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:log:commit -c 44eaebf64
  sfdx saatc:agile:log:commit --commit=44eaebf647d74b8296f3a1f5593fe1c2d6c95ca6 --message="added button to screen"
```

## `sfdx saatc:agile:log:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will create release_component__c entries for every item in the working branch's manifest file

```
will create release_component__c entries for every item in the working branch's manifest file

USAGE
  $ sfdx saatc:agile:log:components [-M] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -M, --manualconfigurations                                                        start an interactive session to
                                                                                    write manual config steps

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:components
  sfdx saatc:agile:components -M
```

## `sfdx saatc:agile:log:tests [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

This command is still under construction

```
This command is still under construction

USAGE
  $ sfdx saatc:agile:log:tests [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  deprecated
  deprecated
```

## `sfdx saatc:agile:log:validate [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

record the latest validation results to agile accelerator

```
record the latest validation results to agile accelerator

USAGE
  $ sfdx saatc:agile:log:validate [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLE
  sfdx saatc:agile:log:validate
```

## `sfdx saatc:agile:open [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

opens the work item in agile accelerator

```
opens the work item in agile accelerator

USAGE
  $ sfdx saatc:agile:open [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile:open
```

## `sfdx saatc:agile:report [--validation] [--test] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

prints all information related to the current work item

```
prints all information related to the current work item

USAGE
  $ sfdx saatc:agile:report [--validation] [--test] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --test                                                                            print full test results to the
                                                                                    terminal

  --validation                                                                      print full validation results to the
                                                                                    terminal

EXAMPLES
  sfdx saatc:agile:report
  sfdx saatc:agile:report --validation --test
```

## `sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

warning: intended for use only with the bamboo agent. logs job information to agile accelerator

```
warning: intended for use only with the bamboo agent. logs job information to agile accelerator

USAGE
  $ sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] 
  [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -e, --environment=environment                                                     environment which this build was ran
                                                                                    against

  -i, --jobid=jobid                                                                 (required) the id of the validation
                                                                                    job to record

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:bamboo:log -i 007Ar00000aAzGyWAG
  sfdx saatc:bamboo:log --jobid=007Ar00000aAzGyWAG --environment=UAT
```

## `sfdx saatc:bamboo:poll -j <string> [-e <string>] [--script] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

warning: intended for use only with the bamboo agent. queries job status from the target org with details from agile accelerator

```
warning: intended for use only with the bamboo agent. queries job status from the target org with details from agile accelerator

USAGE
  $ sfdx saatc:bamboo:poll -j <string> [-e <string>] [--script] [-u <string>] [--apiversion <string>] [--json] 
  [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -e, --environment=environment                                                     environment which this build was ran
                                                                                    against

  -j, --jobenvironmentusername=jobenvironmentusername                               (required) the alias for the
                                                                                    environment in which the build was
                                                                                    executed

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLE
  sfdx saatc:bamboo:poll -j not.a.realusername@salesforce.com -e Validation
```

## `sfdx saatc:cleanup [--all | -V | -T | -W | -A] [--noprompt] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

remove cached config variables

```
remove cached config variables

USAGE
  $ sfdx saatc:cleanup [--all | -V | -T | -W | -A] [--noprompt] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -A, --alias                                                                       clears alias configurations
  -T, --tests                                                                       remove test results
  -V, --validate                                                                    remove validation results
  -W, --workid                                                                      remove cached work ids
  --all                                                                             remove all configurations
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --noprompt                                                                        skip all deletion confirmations

EXAMPLES
  sfdx saatc:cleanup -V -T
  sfdx saatc:cleanup --all
```

## `sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

logs the user into the currently set org

```
logs the user into the currently set org

USAGE
  $ sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -D, --default                                                                     add a new path to the preferred
                                                                                    paths

  -t, --target=target                                                               redirect the user upon login to the
                                                                                    saved target definition

  -u, --alias=alias                                                                 log the user into the aliased org
                                                                                    declared should that org be
                                                                                    authorized

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:login
  sfdx saatc:login -u myDevOrg -t settings
```

## `sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

add a new path to the saved paths

```
add a new path to the saved paths

USAGE
  $ sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will add the specified
                                                                                    target entry from the saved targets.

  -u, --uri=uri                                                                     (required) will set the uri for the
                                                                                    specified target to redirect to upon
                                                                                    login.

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:add -t setup -u lightning/setup/SetupOneHome/home
```

## `sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

removes a path from the saved login paths

```
removes a path from the saved login paths

USAGE
  $ sfdx saatc:login:remove -t <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will remove the specified
                                                                                    target entry from the saved targets

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:remove -t deprecatedpath
```

## `sfdx saatc:manifest:build [-v <string>] [-W] [-B -c <string>] [-f <string>] [--nopackage] [-p <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:manifest:build [-v <string>] [-W] [-B -c <string>] [-f <string>] [--nopackage] [-p <string>] [--script] 
  [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -B, --commitgeneratedmanifest                                                     commit the generated manifest file
                                                                                    immediately upon creation

  -W, --includeworkingfiles                                                         if set, will include uncommitted
                                                                                    changes into the manifest from your
                                                                                    current working directory

  -c, --commit=commit                                                               add the changes specified by the
                                                                                    commit hash to the manifest

  -f, --fromcommit=fromcommit                                                       if declared, generate the manifest
                                                                                    as a diff from the supplied hash

  -p, --filepath=filepath                                                           if declared, will write the manifest
                                                                                    at the file path

  -v, --api=api                                                                     api version for the built manifest
                                                                                    if different than what is already
                                                                                    declared

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --nopackage                                                                       do not update the package manifest

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:deploy [-u <string>] [--apiversion <string>] [--quiet] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:manifest:deploy [-u <string>] [--apiversion <string>] [--quiet] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --quiet                                                                           nothing emitted stdout

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:show [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:manifest:show [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:test [-C] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

This command is still under construction

```
This command is still under construction

USAGE
  $ sfdx saatc:manifest:test [-C] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -C, --commandonly                                                                 returns the command to run; does not
                                                                                    execute the command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  deprecated
  deprecated
```

## `sfdx saatc:manifest:test:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

deprecated

```
deprecated

USAGE
  $ sfdx saatc:manifest:test:report [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          deprecated

EXAMPLES
  desprecated
  desprecated
```

## `sfdx saatc:manifest:validate [-W] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will run a validation against the configured validation org with the working branch manifest file

```
will run a validation against the configured validation org with the working branch manifest file

USAGE
  $ sfdx saatc:manifest:validate [-W] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -W, --nowait                                                                      execute the validate asynchronously
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:validate:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will record the commit information to agile accelerator

```
will record the commit information to agile accelerator

USAGE
  $ sfdx saatc:manifest:validate:report [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:log:commit -c 44eaebf64
  sfdx saatc:agile:log:commit --commit=44eaebf647d74b8296f3a1f5593fe1c2d6c95ca6 --message="added button to screen"
```
<!-- commandsstop -->
* [`sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagile---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:configure -u <string> -s agile|validate|test [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagileconfigure--u-string--s-agilevalidatetest---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:commit -c <string> [-m <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogcommit--c-string--m-string---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogcomponents--m---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:tests [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogtests---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:validate [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogvalidate---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:open [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagileopen---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:report [--validation] [--test] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilereport---validation---test---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbamboolog--i-string---script--e-string--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:bamboo:poll -j <string> [-e <string>] [--script] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbamboopoll--j-string--e-string---script--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:cleanup [--all | -V | -T | -W | -A] [--noprompt] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatccleanup---all---v---t---w---a---noprompt---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatclogin--u-string--t-string--d---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginadd--t-string--u-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginremove--t-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:build [-v <string>] [-W] [-B -c <string>] [-f <string>] [--nopackage] [-p <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestbuild--v-string--w--b--c-string--f-string---nopackage--p-string---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:deploy [-u <string>] [--apiversion <string>] [--quiet] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestdeploy--u-string---apiversion-string---quiet---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:show [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestshow---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:test [-C] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifesttest--c---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:test:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifesttestreport---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:validate [-W] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestvalidate--w---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:validate:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestvalidatereport---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)

## `sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

retrieve the defined org alias for agile accelerator

```
retrieve the defined org alias for agile accelerator

USAGE
  $ sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile
```

## `sfdx saatc:agile:configure -u <string> -s agile|validate|test [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

set the org alias for the deployment related orgs

```
set the org alias for the deployment related orgs

USAGE
  $ sfdx saatc:agile:configure -u <string> -s agile|validate|test [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -s, --setting=(agile|validate|test)                                               (required) the setting to write to

  -u, --alias=alias                                                                 (required) the alias or username of
                                                                                    the org that for the target setting

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile:configure -s agile -u myAgileOrg
```

## `sfdx saatc:agile:log:commit -c <string> [-m <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will record the commit information to agile accelerator

```
will record the commit information to agile accelerator

USAGE
  $ sfdx saatc:agile:log:commit -c <string> [-m <string>] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -c, --commit=commit                                                               (required) record the specified
                                                                                    commit hash to agile accelerator

  -m, --message=message                                                             commit message to record

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:log:commit -c 44eaebf64
  sfdx saatc:agile:log:commit --commit=44eaebf647d74b8296f3a1f5593fe1c2d6c95ca6 --message="added button to screen"
```

## `sfdx saatc:agile:log:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will create release_component__c entries for every item in the working branch's manifest file

```
will create release_component__c entries for every item in the working branch's manifest file

USAGE
  $ sfdx saatc:agile:log:components [-M] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -M, --manualconfigurations                                                        start an interactive session to
                                                                                    write manual config steps

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:components
  sfdx saatc:agile:components -M
```

## `sfdx saatc:agile:log:tests [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

This command is still under construction

```
This command is still under construction

USAGE
  $ sfdx saatc:agile:log:tests [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  deprecated
  deprecated
```

## `sfdx saatc:agile:log:validate [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

record the latest validation results to agile accelerator

```
record the latest validation results to agile accelerator

USAGE
  $ sfdx saatc:agile:log:validate [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLE
  sfdx saatc:agile:log:validate
```

## `sfdx saatc:agile:open [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

opens the work item in agile accelerator

```
opens the work item in agile accelerator

USAGE
  $ sfdx saatc:agile:open [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile:open
```

## `sfdx saatc:agile:report [--validation] [--test] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

prints all information related to the current work item

```
prints all information related to the current work item

USAGE
  $ sfdx saatc:agile:report [--validation] [--test] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --test                                                                            print full test results to the
                                                                                    terminal

  --validation                                                                      print full validation results to the
                                                                                    terminal

EXAMPLES
  sfdx saatc:agile:report
  sfdx saatc:agile:report --validation --test
```

## `sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

warning: intended for use only with the bamboo agent. logs job information to agile accelerator

```
warning: intended for use only with the bamboo agent. logs job information to agile accelerator

USAGE
  $ sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] 
  [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -e, --environment=environment                                                     environment which this build was ran
                                                                                    against

  -i, --jobid=jobid                                                                 (required) the id of the validation
                                                                                    job to record

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:bamboo:log -i 007Ar00000aAzGyWAG
  sfdx saatc:bamboo:log --jobid=007Ar00000aAzGyWAG --environment=UAT
```

## `sfdx saatc:bamboo:poll -j <string> [-e <string>] [--script] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

warning: intended for use only with the bamboo agent. queries job status from the target org with details from agile accelerator

```
warning: intended for use only with the bamboo agent. queries job status from the target org with details from agile accelerator

USAGE
  $ sfdx saatc:bamboo:poll -j <string> [-e <string>] [--script] [-u <string>] [--apiversion <string>] [--json] 
  [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -e, --environment=environment                                                     environment which this build was ran
                                                                                    against

  -j, --jobenvironmentusername=jobenvironmentusername                               (required) the alias for the
                                                                                    environment in which the build was
                                                                                    executed

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLE
  sfdx saatc:bamboo:poll -j not.a.realusername@salesforce.com -e Validation
```

## `sfdx saatc:cleanup [--all | -V | -T | -W | -A] [--noprompt] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

remove cached config variables

```
remove cached config variables

USAGE
  $ sfdx saatc:cleanup [--all | -V | -T | -W | -A] [--noprompt] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -A, --alias                                                                       clears alias configurations
  -T, --tests                                                                       remove test results
  -V, --validate                                                                    remove validation results
  -W, --workid                                                                      remove cached work ids
  --all                                                                             remove all configurations
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --noprompt                                                                        skip all deletion confirmations

EXAMPLES
  sfdx saatc:cleanup -V -T
  sfdx saatc:cleanup --all
```

## `sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

logs the user into the currently set org

```
logs the user into the currently set org

USAGE
  $ sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -D, --default                                                                     add a new path to the preferred
                                                                                    paths

  -t, --target=target                                                               redirect the user upon login to the
                                                                                    saved target definition

  -u, --alias=alias                                                                 log the user into the aliased org
                                                                                    declared should that org be
                                                                                    authorized

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:login
  sfdx saatc:login -u myDevOrg -t settings
```

## `sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

add a new path to the saved paths

```
add a new path to the saved paths

USAGE
  $ sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will add the specified
                                                                                    target entry from the saved targets.

  -u, --uri=uri                                                                     (required) will set the uri for the
                                                                                    specified target to redirect to upon
                                                                                    login.

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:add -t setup -u lightning/setup/SetupOneHome/home
```

## `sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

removes a path from the saved login paths

```
removes a path from the saved login paths

USAGE
  $ sfdx saatc:login:remove -t <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will remove the specified
                                                                                    target entry from the saved targets

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:remove -t deprecatedpath
```

## `sfdx saatc:manifest:build [-v <string>] [-W] [-B -c <string>] [-f <string>] [--nopackage] [-p <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:manifest:build [-v <string>] [-W] [-B -c <string>] [-f <string>] [--nopackage] [-p <string>] [--script] 
  [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -B, --commitgeneratedmanifest                                                     commit the generated manifest file
                                                                                    immediately upon creation

  -W, --includeworkingfiles                                                         if set, will include uncommitted
                                                                                    changes into the manifest from your
                                                                                    current working directory

  -c, --commit=commit                                                               add the changes specified by the
                                                                                    commit hash to the manifest

  -f, --fromcommit=fromcommit                                                       if declared, generate the manifest
                                                                                    as a diff from the supplied hash

  -p, --filepath=filepath                                                           if declared, will write the manifest
                                                                                    at the file path

  -v, --api=api                                                                     api version for the built manifest
                                                                                    if different than what is already
                                                                                    declared

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --nopackage                                                                       do not update the package manifest

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:deploy [-u <string>] [--apiversion <string>] [--quiet] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:manifest:deploy [-u <string>] [--apiversion <string>] [--quiet] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --quiet                                                                           nothing emitted stdout

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:show [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:manifest:show [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:test [-C] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

This command is still under construction

```
This command is still under construction

USAGE
  $ sfdx saatc:manifest:test [-C] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -C, --commandonly                                                                 returns the command to run; does not
                                                                                    execute the command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  deprecated
  deprecated
```

## `sfdx saatc:manifest:test:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

deprecated

```
deprecated

USAGE
  $ sfdx saatc:manifest:test:report [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          deprecated

EXAMPLES
  desprecated
  desprecated
```

## `sfdx saatc:manifest:validate [-W] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will run a validation against the configured validation org with the working branch manifest file

```
will run a validation against the configured validation org with the working branch manifest file

USAGE
  $ sfdx saatc:manifest:validate [-W] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -W, --nowait                                                                      execute the validate asynchronously
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:validate:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will record the commit information to agile accelerator

```
will record the commit information to agile accelerator

USAGE
  $ sfdx saatc:manifest:validate:report [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:log:commit -c 44eaebf64
  sfdx saatc:agile:log:commit --commit=44eaebf647d74b8296f3a1f5593fe1c2d6c95ca6 --message="added button to screen"
```
<!-- commandsstop -->
* [`sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagile---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:configure -u <string> -s agile|validate|test [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagileconfigure--u-string--s-agilevalidatetest---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:commit -c <string> [-m <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogcommit--c-string--m-string---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogcomponents--m---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:tests [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogtests---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:validate [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogvalidate---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:open [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagileopen---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:report [--validation] [--test] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilereport---validation---test---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbamboolog--i-string---script--e-string--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:bamboo:poll -j <string> [-e <string>] [--script] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbamboopoll--j-string--e-string---script--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:cleanup [--all | -V | -T | -W | -A] [--noprompt] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatccleanup---all---v---t---w---a---noprompt---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatclogin--u-string--t-string--d---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginadd--t-string--u-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginremove--t-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:build [-v <string>] [-W] [-B -c <string>] [-f <string>] [--nopackage] [-p <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestbuild--v-string--w--b--c-string--f-string---nopackage--p-string---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:deploy [-u <string>] [--apiversion <string>] [--quiet] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestdeploy--u-string---apiversion-string---quiet---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:show [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestshow---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:test [-C] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifesttest--c---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:test:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifesttestreport---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:validate [-W] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestvalidate--w---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:validate:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestvalidatereport---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)

## `sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

retrieve the defined org alias for agile accelerator

```
retrieve the defined org alias for agile accelerator

USAGE
  $ sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile
```

## `sfdx saatc:agile:configure -u <string> -s agile|validate|test [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

set the org alias for the deployment related orgs

```
set the org alias for the deployment related orgs

USAGE
  $ sfdx saatc:agile:configure -u <string> -s agile|validate|test [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -s, --setting=(agile|validate|test)                                               (required) the setting to write to

  -u, --alias=alias                                                                 (required) the alias or username of
                                                                                    the org that for the target setting

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile:configure -s agile -u myAgileOrg
```

## `sfdx saatc:agile:log:commit -c <string> [-m <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will record the commit information to agile accelerator

```
will record the commit information to agile accelerator

USAGE
  $ sfdx saatc:agile:log:commit -c <string> [-m <string>] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -c, --commit=commit                                                               (required) record the specified
                                                                                    commit hash to agile accelerator

  -m, --message=message                                                             commit message to record

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:log:commit -c 44eaebf64
  sfdx saatc:agile:log:commit --commit=44eaebf647d74b8296f3a1f5593fe1c2d6c95ca6 --message="added button to screen"
```

## `sfdx saatc:agile:log:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will create release_component__c entries for every item in the working branch's manifest file

```
will create release_component__c entries for every item in the working branch's manifest file

USAGE
  $ sfdx saatc:agile:log:components [-M] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -M, --manualconfigurations                                                        start an interactive session to
                                                                                    write manual config steps

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:components
  sfdx saatc:agile:components -M
```

## `sfdx saatc:agile:log:tests [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

This command is still under construction

```
This command is still under construction

USAGE
  $ sfdx saatc:agile:log:tests [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  deprecated
  deprecated
```

## `sfdx saatc:agile:log:validate [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

record the latest validation results to agile accelerator

```
record the latest validation results to agile accelerator

USAGE
  $ sfdx saatc:agile:log:validate [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLE
  sfdx saatc:agile:log:validate
```

## `sfdx saatc:agile:open [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

opens the work item in agile accelerator

```
opens the work item in agile accelerator

USAGE
  $ sfdx saatc:agile:open [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile:open
```

## `sfdx saatc:agile:report [--validation] [--test] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

prints all information related to the current work item

```
prints all information related to the current work item

USAGE
  $ sfdx saatc:agile:report [--validation] [--test] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --test                                                                            print full test results to the
                                                                                    terminal

  --validation                                                                      print full validation results to the
                                                                                    terminal

EXAMPLES
  sfdx saatc:agile:report
  sfdx saatc:agile:report --validation --test
```

## `sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

warning: intended for use only with the bamboo agent. logs job information to agile accelerator

```
warning: intended for use only with the bamboo agent. logs job information to agile accelerator

USAGE
  $ sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] 
  [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -e, --environment=environment                                                     environment which this build was ran
                                                                                    against

  -i, --jobid=jobid                                                                 (required) the id of the validation
                                                                                    job to record

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:bamboo:log -i 007Ar00000aAzGyWAG
  sfdx saatc:bamboo:log --jobid=007Ar00000aAzGyWAG --environment=UAT
```

## `sfdx saatc:bamboo:poll -j <string> [-e <string>] [--script] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

warning: intended for use only with the bamboo agent. queries job status from the target org with details from agile accelerator

```
warning: intended for use only with the bamboo agent. queries job status from the target org with details from agile accelerator

USAGE
  $ sfdx saatc:bamboo:poll -j <string> [-e <string>] [--script] [-u <string>] [--apiversion <string>] [--json] 
  [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -e, --environment=environment                                                     environment which this build was ran
                                                                                    against

  -j, --jobenvironmentusername=jobenvironmentusername                               (required) the alias for the
                                                                                    environment in which the build was
                                                                                    executed

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLE
  sfdx saatc:bamboo:poll -j not.a.realusername@salesforce.com -e Validation
```

## `sfdx saatc:cleanup [--all | -V | -T | -W | -A] [--noprompt] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

remove cached config variables

```
remove cached config variables

USAGE
  $ sfdx saatc:cleanup [--all | -V | -T | -W | -A] [--noprompt] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -A, --alias                                                                       clears alias configurations
  -T, --tests                                                                       remove test results
  -V, --validate                                                                    remove validation results
  -W, --workid                                                                      remove cached work ids
  --all                                                                             remove all configurations
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --noprompt                                                                        skip all deletion confirmations

EXAMPLES
  sfdx saatc:cleanup -V -T
  sfdx saatc:cleanup --all
```

## `sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

logs the user into the currently set org

```
logs the user into the currently set org

USAGE
  $ sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -D, --default                                                                     add a new path to the preferred
                                                                                    paths

  -t, --target=target                                                               redirect the user upon login to the
                                                                                    saved target definition

  -u, --alias=alias                                                                 log the user into the aliased org
                                                                                    declared should that org be
                                                                                    authorized

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:login
  sfdx saatc:login -u myDevOrg -t settings
```

## `sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

add a new path to the saved paths

```
add a new path to the saved paths

USAGE
  $ sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will add the specified
                                                                                    target entry from the saved targets.

  -u, --uri=uri                                                                     (required) will set the uri for the
                                                                                    specified target to redirect to upon
                                                                                    login.

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:add -t setup -u lightning/setup/SetupOneHome/home
```

## `sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

removes a path from the saved login paths

```
removes a path from the saved login paths

USAGE
  $ sfdx saatc:login:remove -t <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will remove the specified
                                                                                    target entry from the saved targets

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:remove -t deprecatedpath
```

## `sfdx saatc:manifest:build [-v <string>] [-W] [-B -c <string>] [-f <string>] [--nopackage] [-p <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:manifest:build [-v <string>] [-W] [-B -c <string>] [-f <string>] [--nopackage] [-p <string>] [--script] 
  [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -B, --commitgeneratedmanifest                                                     commit the generated manifest file
                                                                                    immediately upon creation

  -W, --includeworkingfiles                                                         if set, will include uncommitted
                                                                                    changes into the manifest from your
                                                                                    current working directory

  -c, --commit=commit                                                               add the changes specified by the
                                                                                    commit hash to the manifest

  -f, --fromcommit=fromcommit                                                       if declared, generate the manifest
                                                                                    as a diff from the supplied hash

  -p, --filepath=filepath                                                           if declared, will write the manifest
                                                                                    at the file path

  -v, --api=api                                                                     api version for the built manifest
                                                                                    if different than what is already
                                                                                    declared

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --nopackage                                                                       do not update the package manifest

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:deploy [-u <string>] [--apiversion <string>] [--quiet] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:manifest:deploy [-u <string>] [--apiversion <string>] [--quiet] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --quiet                                                                           nothing emitted stdout

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:show [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:manifest:show [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:test [-C] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

This command is still under construction

```
This command is still under construction

USAGE
  $ sfdx saatc:manifest:test [-C] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -C, --commandonly                                                                 returns the command to run; does not
                                                                                    execute the command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  deprecated
  deprecated
```

## `sfdx saatc:manifest:test:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

deprecated

```
deprecated

USAGE
  $ sfdx saatc:manifest:test:report [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          deprecated

EXAMPLES
  desprecated
  desprecated
```

## `sfdx saatc:manifest:validate [-W] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will run a validation against the configured validation org with the working branch manifest file

```
will run a validation against the configured validation org with the working branch manifest file

USAGE
  $ sfdx saatc:manifest:validate [-W] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -W, --nowait                                                                      execute the validate asynchronously
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:validate:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will record the commit information to agile accelerator

```
will record the commit information to agile accelerator

USAGE
  $ sfdx saatc:manifest:validate:report [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:log:commit -c 44eaebf64
  sfdx saatc:agile:log:commit --commit=44eaebf647d74b8296f3a1f5593fe1c2d6c95ca6 --message="added button to screen"
```
<!-- commandsstop -->
* [`sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagile---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:configure -u <string> -s agile|validate|test [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagileconfigure--u-string--s-agilevalidatetest---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:commit -c <string> [-m <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogcommit--c-string--m-string---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogcomponents--m---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:tests [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogtests---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:validate [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogvalidate---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:open [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagileopen---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:report [--validation] [--test] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilereport---validation---test---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbamboolog--i-string---script--e-string--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:bamboo:poll -j <string> [-e <string>] [--script] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbamboopoll--j-string--e-string---script--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:cleanup [--all | -V | -T | -W | -A] [--noprompt] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatccleanup---all---v---t---w---a---noprompt---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatclogin--u-string--t-string--d---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginadd--t-string--u-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginremove--t-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:build [-v <string>] [-W] [-B -c <string>] [-f <string>] [--nopackage] [-p <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestbuild--v-string--w--b--c-string--f-string---nopackage--p-string---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:deploy [-u <string>] [--apiversion <string>] [--quiet] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestdeploy--u-string---apiversion-string---quiet---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:show [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestshow---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:test [-C] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifesttest--c---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:test:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifesttestreport---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:validate [-W] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestvalidate--w---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:validate:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestvalidatereport---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)

## `sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

retrieve the defined org alias for agile accelerator

```
retrieve the defined org alias for agile accelerator

USAGE
  $ sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile
```

## `sfdx saatc:agile:configure -u <string> -s agile|validate|test [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

set the org alias for the deployment related orgs

```
set the org alias for the deployment related orgs

USAGE
  $ sfdx saatc:agile:configure -u <string> -s agile|validate|test [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -s, --setting=(agile|validate|test)                                               (required) the setting to write to

  -u, --alias=alias                                                                 (required) the alias or username of
                                                                                    the org that for the target setting

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile:configure -s agile -u myAgileOrg
```

## `sfdx saatc:agile:log:commit -c <string> [-m <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will record the commit information to agile accelerator

```
will record the commit information to agile accelerator

USAGE
  $ sfdx saatc:agile:log:commit -c <string> [-m <string>] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -c, --commit=commit                                                               (required) record the specified
                                                                                    commit hash to agile accelerator

  -m, --message=message                                                             commit message to record

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:log:commit -c 44eaebf64
  sfdx saatc:agile:log:commit --commit=44eaebf647d74b8296f3a1f5593fe1c2d6c95ca6 --message="added button to screen"
```

## `sfdx saatc:agile:log:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will create release_component__c entries for every item in the working branch's manifest file

```
will create release_component__c entries for every item in the working branch's manifest file

USAGE
  $ sfdx saatc:agile:log:components [-M] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -M, --manualconfigurations                                                        start an interactive session to
                                                                                    write manual config steps

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:components
  sfdx saatc:agile:components -M
```

## `sfdx saatc:agile:log:tests [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

This command is still under construction

```
This command is still under construction

USAGE
  $ sfdx saatc:agile:log:tests [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  deprecated
  deprecated
```

## `sfdx saatc:agile:log:validate [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

record the latest validation results to agile accelerator

```
record the latest validation results to agile accelerator

USAGE
  $ sfdx saatc:agile:log:validate [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLE
  sfdx saatc:agile:log:validate
```

## `sfdx saatc:agile:open [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

opens the work item in agile accelerator

```
opens the work item in agile accelerator

USAGE
  $ sfdx saatc:agile:open [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile:open
```

## `sfdx saatc:agile:report [--validation] [--test] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

prints all information related to the current work item

```
prints all information related to the current work item

USAGE
  $ sfdx saatc:agile:report [--validation] [--test] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --test                                                                            print full test results to the
                                                                                    terminal

  --validation                                                                      print full validation results to the
                                                                                    terminal

EXAMPLES
  sfdx saatc:agile:report
  sfdx saatc:agile:report --validation --test
```

## `sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

warning: intended for use only with the bamboo agent. logs job information to agile accelerator

```
warning: intended for use only with the bamboo agent. logs job information to agile accelerator

USAGE
  $ sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] 
  [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -e, --environment=environment                                                     environment which this build was ran
                                                                                    against

  -i, --jobid=jobid                                                                 (required) the id of the validation
                                                                                    job to record

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:bamboo:log -i 007Ar00000aAzGyWAG
  sfdx saatc:bamboo:log --jobid=007Ar00000aAzGyWAG --environment=UAT
```

## `sfdx saatc:bamboo:poll -j <string> [-e <string>] [--script] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

warning: intended for use only with the bamboo agent. queries job status from the target org with details from agile accelerator

```
warning: intended for use only with the bamboo agent. queries job status from the target org with details from agile accelerator

USAGE
  $ sfdx saatc:bamboo:poll -j <string> [-e <string>] [--script] [-u <string>] [--apiversion <string>] [--json] 
  [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -e, --environment=environment                                                     environment which this build was ran
                                                                                    against

  -j, --jobenvironmentusername=jobenvironmentusername                               (required) the alias for the
                                                                                    environment in which the build was
                                                                                    executed

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLE
  sfdx saatc:bamboo:poll -j not.a.realusername@salesforce.com -e Validation
```

## `sfdx saatc:cleanup [--all | -V | -T | -W | -A] [--noprompt] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

remove cached config variables

```
remove cached config variables

USAGE
  $ sfdx saatc:cleanup [--all | -V | -T | -W | -A] [--noprompt] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -A, --alias                                                                       clears alias configurations
  -T, --tests                                                                       remove test results
  -V, --validate                                                                    remove validation results
  -W, --workid                                                                      remove cached work ids
  --all                                                                             remove all configurations
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --noprompt                                                                        skip all deletion confirmations

EXAMPLES
  sfdx saatc:cleanup -V -T
  sfdx saatc:cleanup --all
```

## `sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

logs the user into the currently set org

```
logs the user into the currently set org

USAGE
  $ sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -D, --default                                                                     add a new path to the preferred
                                                                                    paths

  -t, --target=target                                                               redirect the user upon login to the
                                                                                    saved target definition

  -u, --alias=alias                                                                 log the user into the aliased org
                                                                                    declared should that org be
                                                                                    authorized

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:login
  sfdx saatc:login -u myDevOrg -t settings
```

## `sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

add a new path to the saved paths

```
add a new path to the saved paths

USAGE
  $ sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will add the specified
                                                                                    target entry from the saved targets.

  -u, --uri=uri                                                                     (required) will set the uri for the
                                                                                    specified target to redirect to upon
                                                                                    login.

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:add -t setup -u lightning/setup/SetupOneHome/home
```

## `sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

removes a path from the saved login paths

```
removes a path from the saved login paths

USAGE
  $ sfdx saatc:login:remove -t <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will remove the specified
                                                                                    target entry from the saved targets

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:remove -t deprecatedpath
```

## `sfdx saatc:manifest:build [-v <string>] [-W] [-B -c <string>] [-f <string>] [--nopackage] [-p <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:manifest:build [-v <string>] [-W] [-B -c <string>] [-f <string>] [--nopackage] [-p <string>] [--script] 
  [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -B, --commitgeneratedmanifest                                                     commit the generated manifest file
                                                                                    immediately upon creation

  -W, --includeworkingfiles                                                         if set, will include uncommitted
                                                                                    changes into the manifest from your
                                                                                    current working directory

  -c, --commit=commit                                                               add the changes specified by the
                                                                                    commit hash to the manifest

  -f, --fromcommit=fromcommit                                                       if declared, generate the manifest
                                                                                    as a diff from the supplied hash

  -p, --filepath=filepath                                                           if declared, will write the manifest
                                                                                    at the file path

  -v, --api=api                                                                     api version for the built manifest
                                                                                    if different than what is already
                                                                                    declared

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --nopackage                                                                       do not update the package manifest

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:deploy [-u <string>] [--apiversion <string>] [--quiet] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:manifest:deploy [-u <string>] [--apiversion <string>] [--quiet] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --quiet                                                                           nothing emitted stdout

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:show [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:manifest:show [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:test [-C] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

This command is still under construction

```
This command is still under construction

USAGE
  $ sfdx saatc:manifest:test [-C] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -C, --commandonly                                                                 returns the command to run; does not
                                                                                    execute the command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  deprecated
  deprecated
```

## `sfdx saatc:manifest:test:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

deprecated

```
deprecated

USAGE
  $ sfdx saatc:manifest:test:report [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          deprecated

EXAMPLES
  desprecated
  desprecated
```

## `sfdx saatc:manifest:validate [-W] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will run a validation against the configured validation org with the working branch manifest file

```
will run a validation against the configured validation org with the working branch manifest file

USAGE
  $ sfdx saatc:manifest:validate [-W] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -W, --nowait                                                                      execute the validate asynchronously
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:validate:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will record the commit information to agile accelerator

```
will record the commit information to agile accelerator

USAGE
  $ sfdx saatc:manifest:validate:report [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:log:commit -c 44eaebf64
  sfdx saatc:agile:log:commit --commit=44eaebf647d74b8296f3a1f5593fe1c2d6c95ca6 --message="added button to screen"
```
<!-- commandsstop -->
* [`sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagile---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:configure -u <string> -s agile|validate|test [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagileconfigure--u-string--s-agilevalidatetest---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:commit -c <string> [-m <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogcommit--c-string--m-string---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogcomponents--m---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:tests [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogtests---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:validate [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogvalidate---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:open [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagileopen---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:report [--validation] [--test] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilereport---validation---test---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbamboolog--i-string---script--e-string--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:bamboo:poll -j <string> [-e <string>] [--script] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbamboopoll--j-string--e-string---script--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:cleanup [--all | -V | -T | -W | -A] [--noprompt] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatccleanup---all---v---t---w---a---noprompt---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatclogin--u-string--t-string--d---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginadd--t-string--u-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginremove--t-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:build [-v <string>] [-W] [-B -c <string>] [-f <string>] [--nopackage] [-p <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestbuild--v-string--w--b--c-string--f-string---nopackage--p-string---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:deploy [-u <string>] [--apiversion <string>] [--quiet] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestdeploy--u-string---apiversion-string---quiet---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:show [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestshow---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:test [-C] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifesttest--c---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:test:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifesttestreport---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:validate [-W] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestvalidate--w---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:validate:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestvalidatereport---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)

## `sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

retrieve the defined org alias for agile accelerator

```
retrieve the defined org alias for agile accelerator

USAGE
  $ sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile
```

## `sfdx saatc:agile:configure -u <string> -s agile|validate|test [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

set the org alias for the deployment related orgs

```
set the org alias for the deployment related orgs

USAGE
  $ sfdx saatc:agile:configure -u <string> -s agile|validate|test [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -s, --setting=(agile|validate|test)                                               (required) the setting to write to

  -u, --alias=alias                                                                 (required) the alias or username of
                                                                                    the org that for the target setting

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile:configure -s agile -u myAgileOrg
```

## `sfdx saatc:agile:log:commit -c <string> [-m <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will record the commit information to agile accelerator

```
will record the commit information to agile accelerator

USAGE
  $ sfdx saatc:agile:log:commit -c <string> [-m <string>] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -c, --commit=commit                                                               (required) record the specified
                                                                                    commit hash to agile accelerator

  -m, --message=message                                                             commit message to record

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:log:commit -c 44eaebf64
  sfdx saatc:agile:log:commit --commit=44eaebf647d74b8296f3a1f5593fe1c2d6c95ca6 --message="added button to screen"
```

## `sfdx saatc:agile:log:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will create release_component__c entries for every item in the working branch's manifest file

```
will create release_component__c entries for every item in the working branch's manifest file

USAGE
  $ sfdx saatc:agile:log:components [-M] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -M, --manualconfigurations                                                        start an interactive session to
                                                                                    write manual config steps

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:components
  sfdx saatc:agile:components -M
```

## `sfdx saatc:agile:log:tests [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

This command is still under construction

```
This command is still under construction

USAGE
  $ sfdx saatc:agile:log:tests [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  deprecated
  deprecated
```

## `sfdx saatc:agile:log:validate [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

record the latest validation results to agile accelerator

```
record the latest validation results to agile accelerator

USAGE
  $ sfdx saatc:agile:log:validate [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLE
  sfdx saatc:agile:log:validate
```

## `sfdx saatc:agile:open [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

opens the work item in agile accelerator

```
opens the work item in agile accelerator

USAGE
  $ sfdx saatc:agile:open [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile:open
```

## `sfdx saatc:agile:report [--validation] [--test] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

prints all information related to the current work item

```
prints all information related to the current work item

USAGE
  $ sfdx saatc:agile:report [--validation] [--test] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --test                                                                            print full test results to the
                                                                                    terminal

  --validation                                                                      print full validation results to the
                                                                                    terminal

EXAMPLES
  sfdx saatc:agile:report
  sfdx saatc:agile:report --validation --test
```

## `sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

warning: intended for use only with the bamboo agent. logs job information to agile accelerator

```
warning: intended for use only with the bamboo agent. logs job information to agile accelerator

USAGE
  $ sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] 
  [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -e, --environment=environment                                                     environment which this build was ran
                                                                                    against

  -i, --jobid=jobid                                                                 (required) the id of the validation
                                                                                    job to record

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:bamboo:log -i 007Ar00000aAzGyWAG
  sfdx saatc:bamboo:log --jobid=007Ar00000aAzGyWAG --environment=UAT
```

## `sfdx saatc:bamboo:poll -j <string> [-e <string>] [--script] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

warning: intended for use only with the bamboo agent. queries job status from the target org with details from agile accelerator

```
warning: intended for use only with the bamboo agent. queries job status from the target org with details from agile accelerator

USAGE
  $ sfdx saatc:bamboo:poll -j <string> [-e <string>] [--script] [-u <string>] [--apiversion <string>] [--json] 
  [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -e, --environment=environment                                                     environment which this build was ran
                                                                                    against

  -j, --jobenvironmentusername=jobenvironmentusername                               (required) the alias for the
                                                                                    environment in which the build was
                                                                                    executed

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLE
  sfdx saatc:bamboo:poll -j not.a.realusername@salesforce.com -e Validation
```

## `sfdx saatc:cleanup [--all | -V | -T | -W | -A] [--noprompt] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

remove cached config variables

```
remove cached config variables

USAGE
  $ sfdx saatc:cleanup [--all | -V | -T | -W | -A] [--noprompt] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -A, --alias                                                                       clears alias configurations
  -T, --tests                                                                       remove test results
  -V, --validate                                                                    remove validation results
  -W, --workid                                                                      remove cached work ids
  --all                                                                             remove all configurations
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --noprompt                                                                        skip all deletion confirmations

EXAMPLES
  sfdx saatc:cleanup -V -T
  sfdx saatc:cleanup --all
```

## `sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

logs the user into the currently set org

```
logs the user into the currently set org

USAGE
  $ sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -D, --default                                                                     add a new path to the preferred
                                                                                    paths

  -t, --target=target                                                               redirect the user upon login to the
                                                                                    saved target definition

  -u, --alias=alias                                                                 log the user into the aliased org
                                                                                    declared should that org be
                                                                                    authorized

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:login
  sfdx saatc:login -u myDevOrg -t settings
```

## `sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

add a new path to the saved paths

```
add a new path to the saved paths

USAGE
  $ sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will add the specified
                                                                                    target entry from the saved targets.

  -u, --uri=uri                                                                     (required) will set the uri for the
                                                                                    specified target to redirect to upon
                                                                                    login.

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:add -t setup -u lightning/setup/SetupOneHome/home
```

## `sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

removes a path from the saved login paths

```
removes a path from the saved login paths

USAGE
  $ sfdx saatc:login:remove -t <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will remove the specified
                                                                                    target entry from the saved targets

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:remove -t deprecatedpath
```

## `sfdx saatc:manifest:build [-v <string>] [-W] [-B -c <string>] [-f <string>] [--nopackage] [-p <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:manifest:build [-v <string>] [-W] [-B -c <string>] [-f <string>] [--nopackage] [-p <string>] [--script] 
  [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -B, --commitgeneratedmanifest                                                     commit the generated manifest file
                                                                                    immediately upon creation

  -W, --includeworkingfiles                                                         if set, will include uncommitted
                                                                                    changes into the manifest from your
                                                                                    current working directory

  -c, --commit=commit                                                               add the changes specified by the
                                                                                    commit hash to the manifest

  -f, --fromcommit=fromcommit                                                       if declared, generate the manifest
                                                                                    as a diff from the supplied hash

  -p, --filepath=filepath                                                           if declared, will write the manifest
                                                                                    at the file path

  -v, --api=api                                                                     api version for the built manifest
                                                                                    if different than what is already
                                                                                    declared

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --nopackage                                                                       do not update the package manifest

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:deploy [-u <string>] [--apiversion <string>] [--quiet] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:manifest:deploy [-u <string>] [--apiversion <string>] [--quiet] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --quiet                                                                           nothing emitted stdout

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:show [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:manifest:show [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:test [-C] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

This command is still under construction

```
This command is still under construction

USAGE
  $ sfdx saatc:manifest:test [-C] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -C, --commandonly                                                                 returns the command to run; does not
                                                                                    execute the command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  deprecated
  deprecated
```

## `sfdx saatc:manifest:test:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

deprecated

```
deprecated

USAGE
  $ sfdx saatc:manifest:test:report [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          deprecated

EXAMPLES
  desprecated
  desprecated
```

## `sfdx saatc:manifest:validate [-W] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will run a validation against the configured validation org with the working branch manifest file

```
will run a validation against the configured validation org with the working branch manifest file

USAGE
  $ sfdx saatc:manifest:validate [-W] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -W, --nowait                                                                      execute the validate asynchronously
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:validate:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will record the commit information to agile accelerator

```
will record the commit information to agile accelerator

USAGE
  $ sfdx saatc:manifest:validate:report [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:log:commit -c 44eaebf64
  sfdx saatc:agile:log:commit --commit=44eaebf647d74b8296f3a1f5593fe1c2d6c95ca6 --message="added button to screen"
```
<!-- commandsstop -->
* [`sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagile---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:configure -u <string> -s agile|validate|test [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagileconfigure--u-string--s-agilevalidatetest---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:commit -c <string> [-m <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogcommit--c-string--m-string---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogcomponents--m---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:tests [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogtests---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:validate [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogvalidate---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:open [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagileopen---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:report [--validation] [--test] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilereport---validation---test---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbamboolog--i-string---script--e-string--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:bamboo:poll -j <string> [-e <string>] [--script] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbamboopoll--j-string--e-string---script--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:cleanup [--all | -V | -T | -W | -A] [--noprompt] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatccleanup---all---v---t---w---a---noprompt---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatclogin--u-string--t-string--d---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginadd--t-string--u-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginremove--t-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:build [-v <string>] [-W] [-B -c <string>] [-f <string>] [--nopackage] [-p <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestbuild--v-string--w--b--c-string--f-string---nopackage--p-string---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:deploy [-u <string>] [--apiversion <string>] [--quiet] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestdeploy--u-string---apiversion-string---quiet---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:show [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestshow---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:test [-C] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifesttest--c---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:test:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifesttestreport---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:validate [-W] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestvalidate--w---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:validate:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestvalidatereport---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)

## `sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

retrieve the defined org alias for agile accelerator

```
retrieve the defined org alias for agile accelerator

USAGE
  $ sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile
```

## `sfdx saatc:agile:configure -u <string> -s agile|validate|test [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

set the org alias for the deployment related orgs

```
set the org alias for the deployment related orgs

USAGE
  $ sfdx saatc:agile:configure -u <string> -s agile|validate|test [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -s, --setting=(agile|validate|test)                                               (required) the setting to write to

  -u, --alias=alias                                                                 (required) the alias or username of
                                                                                    the org that for the target setting

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile:configure -s agile -u myAgileOrg
```

## `sfdx saatc:agile:log:commit -c <string> [-m <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will record the commit information to agile accelerator

```
will record the commit information to agile accelerator

USAGE
  $ sfdx saatc:agile:log:commit -c <string> [-m <string>] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -c, --commit=commit                                                               (required) record the specified
                                                                                    commit hash to agile accelerator

  -m, --message=message                                                             commit message to record

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:log:commit -c 44eaebf64
  sfdx saatc:agile:log:commit --commit=44eaebf647d74b8296f3a1f5593fe1c2d6c95ca6 --message="added button to screen"
```

## `sfdx saatc:agile:log:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will create release_component__c entries for every item in the working branch's manifest file

```
will create release_component__c entries for every item in the working branch's manifest file

USAGE
  $ sfdx saatc:agile:log:components [-M] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -M, --manualconfigurations                                                        start an interactive session to
                                                                                    write manual config steps

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:components
  sfdx saatc:agile:components -M
```

## `sfdx saatc:agile:log:tests [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

This command is still under construction

```
This command is still under construction

USAGE
  $ sfdx saatc:agile:log:tests [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  deprecated
  deprecated
```

## `sfdx saatc:agile:log:validate [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

record the latest validation results to agile accelerator

```
record the latest validation results to agile accelerator

USAGE
  $ sfdx saatc:agile:log:validate [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLE
  sfdx saatc:agile:log:validate
```

## `sfdx saatc:agile:open [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

opens the work item in agile accelerator

```
opens the work item in agile accelerator

USAGE
  $ sfdx saatc:agile:open [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile:open
```

## `sfdx saatc:agile:report [--validation] [--test] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

prints all information related to the current work item

```
prints all information related to the current work item

USAGE
  $ sfdx saatc:agile:report [--validation] [--test] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --test                                                                            print full test results to the
                                                                                    terminal

  --validation                                                                      print full validation results to the
                                                                                    terminal

EXAMPLES
  sfdx saatc:agile:report
  sfdx saatc:agile:report --validation --test
```

## `sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

warning: intended for use only with the bamboo agent. logs job information to agile accelerator

```
warning: intended for use only with the bamboo agent. logs job information to agile accelerator

USAGE
  $ sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] 
  [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -e, --environment=environment                                                     environment which this build was ran
                                                                                    against

  -i, --jobid=jobid                                                                 (required) the id of the validation
                                                                                    job to record

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:bamboo:log -i 007Ar00000aAzGyWAG
  sfdx saatc:bamboo:log --jobid=007Ar00000aAzGyWAG --environment=UAT
```

## `sfdx saatc:bamboo:poll -j <string> [-e <string>] [--script] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

warning: intended for use only with the bamboo agent. queries job status from the target org with details from agile accelerator

```
warning: intended for use only with the bamboo agent. queries job status from the target org with details from agile accelerator

USAGE
  $ sfdx saatc:bamboo:poll -j <string> [-e <string>] [--script] [-u <string>] [--apiversion <string>] [--json] 
  [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -e, --environment=environment                                                     environment which this build was ran
                                                                                    against

  -j, --jobenvironmentusername=jobenvironmentusername                               (required) the alias for the
                                                                                    environment in which the build was
                                                                                    executed

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLE
  sfdx saatc:bamboo:poll -j not.a.realusername@salesforce.com -e Validation
```

## `sfdx saatc:cleanup [--all | -V | -T | -W | -A] [--noprompt] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

remove cached config variables

```
remove cached config variables

USAGE
  $ sfdx saatc:cleanup [--all | -V | -T | -W | -A] [--noprompt] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -A, --alias                                                                       clears alias configurations
  -T, --tests                                                                       remove test results
  -V, --validate                                                                    remove validation results
  -W, --workid                                                                      remove cached work ids
  --all                                                                             remove all configurations
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --noprompt                                                                        skip all deletion confirmations

EXAMPLES
  sfdx saatc:cleanup -V -T
  sfdx saatc:cleanup --all
```

## `sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

logs the user into the currently set org

```
logs the user into the currently set org

USAGE
  $ sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -D, --default                                                                     add a new path to the preferred
                                                                                    paths

  -t, --target=target                                                               redirect the user upon login to the
                                                                                    saved target definition

  -u, --alias=alias                                                                 log the user into the aliased org
                                                                                    declared should that org be
                                                                                    authorized

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:login
  sfdx saatc:login -u myDevOrg -t settings
```

## `sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

add a new path to the saved paths

```
add a new path to the saved paths

USAGE
  $ sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will add the specified
                                                                                    target entry from the saved targets.

  -u, --uri=uri                                                                     (required) will set the uri for the
                                                                                    specified target to redirect to upon
                                                                                    login.

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:add -t setup -u lightning/setup/SetupOneHome/home
```

## `sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

removes a path from the saved login paths

```
removes a path from the saved login paths

USAGE
  $ sfdx saatc:login:remove -t <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will remove the specified
                                                                                    target entry from the saved targets

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:remove -t deprecatedpath
```

## `sfdx saatc:manifest:build [-v <string>] [-W] [-B -c <string>] [-f <string>] [--nopackage] [-p <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:manifest:build [-v <string>] [-W] [-B -c <string>] [-f <string>] [--nopackage] [-p <string>] [--script] 
  [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -B, --commitgeneratedmanifest                                                     commit the generated manifest file
                                                                                    immediately upon creation

  -W, --includeworkingfiles                                                         if set, will include uncommitted
                                                                                    changes into the manifest from your
                                                                                    current working directory

  -c, --commit=commit                                                               add the changes specified by the
                                                                                    commit hash to the manifest

  -f, --fromcommit=fromcommit                                                       if declared, generate the manifest
                                                                                    as a diff from the supplied hash

  -p, --filepath=filepath                                                           if declared, will write the manifest
                                                                                    at the file path

  -v, --api=api                                                                     api version for the built manifest
                                                                                    if different than what is already
                                                                                    declared

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --nopackage                                                                       do not update the package manifest

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:deploy [-u <string>] [--apiversion <string>] [--quiet] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:manifest:deploy [-u <string>] [--apiversion <string>] [--quiet] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --quiet                                                                           nothing emitted stdout

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:show [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:manifest:show [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:test [-C] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

This command is still under construction

```
This command is still under construction

USAGE
  $ sfdx saatc:manifest:test [-C] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -C, --commandonly                                                                 returns the command to run; does not
                                                                                    execute the command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  deprecated
  deprecated
```

## `sfdx saatc:manifest:test:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

deprecated

```
deprecated

USAGE
  $ sfdx saatc:manifest:test:report [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          deprecated

EXAMPLES
  desprecated
  desprecated
```

## `sfdx saatc:manifest:validate [-W] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will run a validation against the configured validation org with the working branch manifest file

```
will run a validation against the configured validation org with the working branch manifest file

USAGE
  $ sfdx saatc:manifest:validate [-W] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -W, --nowait                                                                      execute the validate asynchronously
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:validate:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will record the commit information to agile accelerator

```
will record the commit information to agile accelerator

USAGE
  $ sfdx saatc:manifest:validate:report [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:log:commit -c 44eaebf64
  sfdx saatc:agile:log:commit --commit=44eaebf647d74b8296f3a1f5593fe1c2d6c95ca6 --message="added button to screen"
```
<!-- commandsstop -->
* [`sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagile---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:configure -u <string> -s agile|validate|test [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagileconfigure--u-string--s-agilevalidatetest---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:commit -c <string> [-m <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogcommit--c-string--m-string---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogcomponents--m---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:tests [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogtests---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:validate [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogvalidate---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:open [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagileopen---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:report [--validation] [--test] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilereport---validation---test---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbamboolog--i-string---script--e-string--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:bamboo:poll -j <string> [-e <string>] [--script] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbamboopoll--j-string--e-string---script--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:cleanup [--all | -V | -T | -W | -A] [--noprompt] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatccleanup---all---v---t---w---a---noprompt---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatclogin--u-string--t-string--d---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginadd--t-string--u-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginremove--t-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:build [-v <string>] [-W] [-B -c <string>] [-f <string>] [--nopackage] [-p <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestbuild--v-string--w--b--c-string--f-string---nopackage--p-string---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:deploy [-u <string>] [--apiversion <string>] [--quiet] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestdeploy--u-string---apiversion-string---quiet---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:show [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestshow---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:test [-C] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifesttest--c---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:test:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifesttestreport---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:validate [-W] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestvalidate--w---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:validate:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestvalidatereport---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)

## `sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

retrieve the defined org alias for agile accelerator

```
retrieve the defined org alias for agile accelerator

USAGE
  $ sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile
```

## `sfdx saatc:agile:configure -u <string> -s agile|validate|test [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

set the org alias for the deployment related orgs

```
set the org alias for the deployment related orgs

USAGE
  $ sfdx saatc:agile:configure -u <string> -s agile|validate|test [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -s, --setting=(agile|validate|test)                                               (required) the setting to write to

  -u, --alias=alias                                                                 (required) the alias or username of
                                                                                    the org that for the target setting

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile:configure -s agile -u myAgileOrg
```

## `sfdx saatc:agile:log:commit -c <string> [-m <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will record the commit information to agile accelerator

```
will record the commit information to agile accelerator

USAGE
  $ sfdx saatc:agile:log:commit -c <string> [-m <string>] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -c, --commit=commit                                                               (required) record the specified
                                                                                    commit hash to agile accelerator

  -m, --message=message                                                             commit message to record

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:log:commit -c 44eaebf64
  sfdx saatc:agile:log:commit --commit=44eaebf647d74b8296f3a1f5593fe1c2d6c95ca6 --message="added button to screen"
```

## `sfdx saatc:agile:log:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will create release_component__c entries for every item in the working branch's manifest file

```
will create release_component__c entries for every item in the working branch's manifest file

USAGE
  $ sfdx saatc:agile:log:components [-M] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -M, --manualconfigurations                                                        start an interactive session to
                                                                                    write manual config steps

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:components
  sfdx saatc:agile:components -M
```

## `sfdx saatc:agile:log:tests [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

This command is still under construction

```
This command is still under construction

USAGE
  $ sfdx saatc:agile:log:tests [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  deprecated
  deprecated
```

## `sfdx saatc:agile:log:validate [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

record the latest validation results to agile accelerator

```
record the latest validation results to agile accelerator

USAGE
  $ sfdx saatc:agile:log:validate [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLE
  sfdx saatc:agile:log:validate
```

## `sfdx saatc:agile:open [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

opens the work item in agile accelerator

```
opens the work item in agile accelerator

USAGE
  $ sfdx saatc:agile:open [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile:open
```

## `sfdx saatc:agile:report [--validation] [--test] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

prints all information related to the current work item

```
prints all information related to the current work item

USAGE
  $ sfdx saatc:agile:report [--validation] [--test] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --test                                                                            print full test results to the
                                                                                    terminal

  --validation                                                                      print full validation results to the
                                                                                    terminal

EXAMPLES
  sfdx saatc:agile:report
  sfdx saatc:agile:report --validation --test
```

## `sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

warning: intended for use only with the bamboo agent. logs job information to agile accelerator

```
warning: intended for use only with the bamboo agent. logs job information to agile accelerator

USAGE
  $ sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] 
  [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -e, --environment=environment                                                     environment which this build was ran
                                                                                    against

  -i, --jobid=jobid                                                                 (required) the id of the validation
                                                                                    job to record

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:bamboo:log -i 007Ar00000aAzGyWAG
  sfdx saatc:bamboo:log --jobid=007Ar00000aAzGyWAG --environment=UAT
```

## `sfdx saatc:bamboo:poll -j <string> [-e <string>] [--script] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

warning: intended for use only with the bamboo agent. queries job status from the target org with details from agile accelerator

```
warning: intended for use only with the bamboo agent. queries job status from the target org with details from agile accelerator

USAGE
  $ sfdx saatc:bamboo:poll -j <string> [-e <string>] [--script] [-u <string>] [--apiversion <string>] [--json] 
  [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -e, --environment=environment                                                     environment which this build was ran
                                                                                    against

  -j, --jobenvironmentusername=jobenvironmentusername                               (required) the alias for the
                                                                                    environment in which the build was
                                                                                    executed

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLE
  sfdx saatc:bamboo:poll -j not.a.realusername@salesforce.com -e Validation
```

## `sfdx saatc:cleanup [--all | -V | -T | -W | -A] [--noprompt] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

remove cached config variables

```
remove cached config variables

USAGE
  $ sfdx saatc:cleanup [--all | -V | -T | -W | -A] [--noprompt] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -A, --alias                                                                       clears alias configurations
  -T, --tests                                                                       remove test results
  -V, --validate                                                                    remove validation results
  -W, --workid                                                                      remove cached work ids
  --all                                                                             remove all configurations
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --noprompt                                                                        skip all deletion confirmations

EXAMPLES
  sfdx saatc:cleanup -V -T
  sfdx saatc:cleanup --all
```

## `sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

logs the user into the currently set org

```
logs the user into the currently set org

USAGE
  $ sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -D, --default                                                                     add a new path to the preferred
                                                                                    paths

  -t, --target=target                                                               redirect the user upon login to the
                                                                                    saved target definition

  -u, --alias=alias                                                                 log the user into the aliased org
                                                                                    declared should that org be
                                                                                    authorized

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:login
  sfdx saatc:login -u myDevOrg -t settings
```

## `sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

add a new path to the saved paths

```
add a new path to the saved paths

USAGE
  $ sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will add the specified
                                                                                    target entry from the saved targets.

  -u, --uri=uri                                                                     (required) will set the uri for the
                                                                                    specified target to redirect to upon
                                                                                    login.

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:add -t setup -u lightning/setup/SetupOneHome/home
```

## `sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

removes a path from the saved login paths

```
removes a path from the saved login paths

USAGE
  $ sfdx saatc:login:remove -t <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will remove the specified
                                                                                    target entry from the saved targets

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:remove -t deprecatedpath
```

## `sfdx saatc:manifest:build [-v <string>] [-W] [-B -c <string>] [-f <string>] [--nopackage] [-p <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:manifest:build [-v <string>] [-W] [-B -c <string>] [-f <string>] [--nopackage] [-p <string>] [--script] 
  [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -B, --commitgeneratedmanifest                                                     commit the generated manifest file
                                                                                    immediately upon creation

  -W, --includeworkingfiles                                                         if set, will include uncommitted
                                                                                    changes into the manifest from your
                                                                                    current working directory

  -c, --commit=commit                                                               add the changes specified by the
                                                                                    commit hash to the manifest

  -f, --fromcommit=fromcommit                                                       if declared, generate the manifest
                                                                                    as a diff from the supplied hash

  -p, --filepath=filepath                                                           if declared, will write the manifest
                                                                                    at the file path

  -v, --api=api                                                                     api version for the built manifest
                                                                                    if different than what is already
                                                                                    declared

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --nopackage                                                                       do not update the package manifest

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:deploy [-u <string>] [--apiversion <string>] [--quiet] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:manifest:deploy [-u <string>] [--apiversion <string>] [--quiet] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --quiet                                                                           nothing emitted stdout

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:show [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:manifest:show [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:test [-C] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

This command is still under construction

```
This command is still under construction

USAGE
  $ sfdx saatc:manifest:test [-C] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -C, --commandonly                                                                 returns the command to run; does not
                                                                                    execute the command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  deprecated
  deprecated
```

## `sfdx saatc:manifest:test:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

deprecated

```
deprecated

USAGE
  $ sfdx saatc:manifest:test:report [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          deprecated

EXAMPLES
  desprecated
  desprecated
```

## `sfdx saatc:manifest:validate [-W] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will run a validation against the configured validation org with the working branch manifest file

```
will run a validation against the configured validation org with the working branch manifest file

USAGE
  $ sfdx saatc:manifest:validate [-W] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -W, --nowait                                                                      execute the validate asynchronously
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:validate:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will record the commit information to agile accelerator

```
will record the commit information to agile accelerator

USAGE
  $ sfdx saatc:manifest:validate:report [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:log:commit -c 44eaebf64
  sfdx saatc:agile:log:commit --commit=44eaebf647d74b8296f3a1f5593fe1c2d6c95ca6 --message="added button to screen"
```
<!-- commandsstop -->
* [`sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagile---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:configure -u <string> -s agile|validate|test [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagileconfigure--u-string--s-agilevalidatetest---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:commit -c <string> [-m <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogcommit--c-string--m-string---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogcomponents--m---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:tests [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogtests---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:validate [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogvalidate---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:open [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagileopen---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:report [--validation] [--test] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilereport---validation---test---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbamboolog--i-string---script--e-string--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:bamboo:poll -j <string> [-e <string>] [--script] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbamboopoll--j-string--e-string---script--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:cleanup [--all | -V | -T | -W | -A] [--noprompt] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatccleanup---all---v---t---w---a---noprompt---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatclogin--u-string--t-string--d---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginadd--t-string--u-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginremove--t-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:build [-v <string>] [-W] [-B -c <string>] [-f <string>] [--nopackage] [-p <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestbuild--v-string--w--b--c-string--f-string---nopackage--p-string---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:deploy [-u <string>] [--apiversion <string>] [--quiet] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestdeploy--u-string---apiversion-string---quiet---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:show [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestshow---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:test [-C] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifesttest--c---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:test:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifesttestreport---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:validate [-W] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestvalidate--w---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:validate:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestvalidatereport---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)

## `sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

retrieve the defined org alias for agile accelerator

```
retrieve the defined org alias for agile accelerator

USAGE
  $ sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile
```

## `sfdx saatc:agile:configure -u <string> -s agile|validate|test [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

set the org alias for the deployment related orgs

```
set the org alias for the deployment related orgs

USAGE
  $ sfdx saatc:agile:configure -u <string> -s agile|validate|test [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -s, --setting=(agile|validate|test)                                               (required) the setting to write to

  -u, --alias=alias                                                                 (required) the alias or username of
                                                                                    the org that for the target setting

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile:configure -s agile -u myAgileOrg
```

## `sfdx saatc:agile:log:commit -c <string> [-m <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will record the commit information to agile accelerator

```
will record the commit information to agile accelerator

USAGE
  $ sfdx saatc:agile:log:commit -c <string> [-m <string>] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -c, --commit=commit                                                               (required) record the specified
                                                                                    commit hash to agile accelerator

  -m, --message=message                                                             commit message to record

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:log:commit -c 44eaebf64
  sfdx saatc:agile:log:commit --commit=44eaebf647d74b8296f3a1f5593fe1c2d6c95ca6 --message="added button to screen"
```

## `sfdx saatc:agile:log:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will create release_component__c entries for every item in the working branch's manifest file

```
will create release_component__c entries for every item in the working branch's manifest file

USAGE
  $ sfdx saatc:agile:log:components [-M] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -M, --manualconfigurations                                                        start an interactive session to
                                                                                    write manual config steps

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:components
  sfdx saatc:agile:components -M
```

## `sfdx saatc:agile:log:tests [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

This command is still under construction

```
This command is still under construction

USAGE
  $ sfdx saatc:agile:log:tests [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  deprecated
  deprecated
```

## `sfdx saatc:agile:log:validate [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

record the latest validation results to agile accelerator

```
record the latest validation results to agile accelerator

USAGE
  $ sfdx saatc:agile:log:validate [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLE
  sfdx saatc:agile:log:validate
```

## `sfdx saatc:agile:open [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

opens the work item in agile accelerator

```
opens the work item in agile accelerator

USAGE
  $ sfdx saatc:agile:open [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile:open
```

## `sfdx saatc:agile:report [--validation] [--test] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

prints all information related to the current work item

```
prints all information related to the current work item

USAGE
  $ sfdx saatc:agile:report [--validation] [--test] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --test                                                                            print full test results to the
                                                                                    terminal

  --validation                                                                      print full validation results to the
                                                                                    terminal

EXAMPLES
  sfdx saatc:agile:report
  sfdx saatc:agile:report --validation --test
```

## `sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

warning: intended for use only with the bamboo agent. logs job information to agile accelerator

```
warning: intended for use only with the bamboo agent. logs job information to agile accelerator

USAGE
  $ sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] 
  [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -e, --environment=environment                                                     environment which this build was ran
                                                                                    against

  -i, --jobid=jobid                                                                 (required) the id of the validation
                                                                                    job to record

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:bamboo:log -i 007Ar00000aAzGyWAG
  sfdx saatc:bamboo:log --jobid=007Ar00000aAzGyWAG --environment=UAT
```

## `sfdx saatc:bamboo:poll -j <string> [-e <string>] [--script] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

warning: intended for use only with the bamboo agent. queries job status from the target org with details from agile accelerator

```
warning: intended for use only with the bamboo agent. queries job status from the target org with details from agile accelerator

USAGE
  $ sfdx saatc:bamboo:poll -j <string> [-e <string>] [--script] [-u <string>] [--apiversion <string>] [--json] 
  [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -e, --environment=environment                                                     environment which this build was ran
                                                                                    against

  -j, --jobenvironmentusername=jobenvironmentusername                               (required) the alias for the
                                                                                    environment in which the build was
                                                                                    executed

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLE
  sfdx saatc:bamboo:poll -j not.a.realusername@salesforce.com -e Validation
```

## `sfdx saatc:cleanup [--all | -V | -T | -W | -A] [--noprompt] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

remove cached config variables

```
remove cached config variables

USAGE
  $ sfdx saatc:cleanup [--all | -V | -T | -W | -A] [--noprompt] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -A, --alias                                                                       clears alias configurations
  -T, --tests                                                                       remove test results
  -V, --validate                                                                    remove validation results
  -W, --workid                                                                      remove cached work ids
  --all                                                                             remove all configurations
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --noprompt                                                                        skip all deletion confirmations

EXAMPLES
  sfdx saatc:cleanup -V -T
  sfdx saatc:cleanup --all
```

## `sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

logs the user into the currently set org

```
logs the user into the currently set org

USAGE
  $ sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -D, --default                                                                     add a new path to the preferred
                                                                                    paths

  -t, --target=target                                                               redirect the user upon login to the
                                                                                    saved target definition

  -u, --alias=alias                                                                 log the user into the aliased org
                                                                                    declared should that org be
                                                                                    authorized

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:login
  sfdx saatc:login -u myDevOrg -t settings
```

## `sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

add a new path to the saved paths

```
add a new path to the saved paths

USAGE
  $ sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will add the specified
                                                                                    target entry from the saved targets.

  -u, --uri=uri                                                                     (required) will set the uri for the
                                                                                    specified target to redirect to upon
                                                                                    login.

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:add -t setup -u lightning/setup/SetupOneHome/home
```

## `sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

removes a path from the saved login paths

```
removes a path from the saved login paths

USAGE
  $ sfdx saatc:login:remove -t <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will remove the specified
                                                                                    target entry from the saved targets

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:remove -t deprecatedpath
```

## `sfdx saatc:manifest:build [-v <string>] [-W] [-B -c <string>] [-f <string>] [--nopackage] [-p <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:manifest:build [-v <string>] [-W] [-B -c <string>] [-f <string>] [--nopackage] [-p <string>] [--script] 
  [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -B, --commitgeneratedmanifest                                                     commit the generated manifest file
                                                                                    immediately upon creation

  -W, --includeworkingfiles                                                         if set, will include uncommitted
                                                                                    changes into the manifest from your
                                                                                    current working directory

  -c, --commit=commit                                                               add the changes specified by the
                                                                                    commit hash to the manifest

  -f, --fromcommit=fromcommit                                                       if declared, generate the manifest
                                                                                    as a diff from the supplied hash

  -p, --filepath=filepath                                                           if declared, will write the manifest
                                                                                    at the file path

  -v, --api=api                                                                     api version for the built manifest
                                                                                    if different than what is already
                                                                                    declared

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --nopackage                                                                       do not update the package manifest

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:deploy [-u <string>] [--apiversion <string>] [--quiet] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:manifest:deploy [-u <string>] [--apiversion <string>] [--quiet] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --quiet                                                                           nothing emitted stdout

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:show [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:manifest:show [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:test [-C] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

This command is still under construction

```
This command is still under construction

USAGE
  $ sfdx saatc:manifest:test [-C] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -C, --commandonly                                                                 returns the command to run; does not
                                                                                    execute the command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  deprecated
  deprecated
```

## `sfdx saatc:manifest:test:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

deprecated

```
deprecated

USAGE
  $ sfdx saatc:manifest:test:report [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          deprecated

EXAMPLES
  desprecated
  desprecated
```

## `sfdx saatc:manifest:validate [-W] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will run a validation against the configured validation org with the working branch manifest file

```
will run a validation against the configured validation org with the working branch manifest file

USAGE
  $ sfdx saatc:manifest:validate [-W] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -W, --nowait                                                                      execute the validate asynchronously
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:validate:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will record the commit information to agile accelerator

```
will record the commit information to agile accelerator

USAGE
  $ sfdx saatc:manifest:validate:report [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:log:commit -c 44eaebf64
  sfdx saatc:agile:log:commit --commit=44eaebf647d74b8296f3a1f5593fe1c2d6c95ca6 --message="added button to screen"
```
<!-- commandsstop -->
* [`sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagile---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:configure -u <string> -s agile|validate [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagileconfigure--u-string--s-agilevalidate---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:commit -c <string> [-m <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogcommit--c-string--m-string---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogcomponents--m---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:tests [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogtests---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:log:validate [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilelogvalidate---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:open [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagileopen---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:report [--validation] [--test] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilereport---validation---test---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbamboolog--i-string---script--e-string--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:bamboo:poll -j <string> [-e <string>] [--script] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbamboopoll--j-string--e-string---script--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:cleanup [--all | -V | -T | -W | -A] [--noprompt] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatccleanup---all---v---t---w---a---noprompt---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatclogin--u-string--t-string--d---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginadd--t-string--u-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginremove--t-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:build [-v <string>] [-W] [-B -c <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestbuild--v-string--w--b--c-string---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:deploy [-u <string>] [--apiversion <string>] [--quiet] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestdeploy--u-string---apiversion-string---quiet---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:show [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestshow---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:test [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifesttest---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:test:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifesttestreport---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:validate [-W] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestvalidate--w---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:manifest:validate:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcmanifestvalidatereport---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)

## `sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

retrieve the defined org alias for agile accelerator

```
retrieve the defined org alias for agile accelerator

USAGE
  $ sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile
```

## `sfdx saatc:agile:configure -u <string> -s agile|validate [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

set the org alias for the deployment related orgs

```
set the org alias for the deployment related orgs

USAGE
  $ sfdx saatc:agile:configure -u <string> -s agile|validate [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -s, --setting=(agile|validate)                                                    (required) the setting to write to

  -u, --alias=alias                                                                 (required) the alias or username of
                                                                                    the org that for the target setting

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile:configure -s agile -u myAgileOrg
```

## `sfdx saatc:agile:log:commit -c <string> [-m <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will record the commit information to agile accelerator

```
will record the commit information to agile accelerator

USAGE
  $ sfdx saatc:agile:log:commit -c <string> [-m <string>] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -c, --commit=commit                                                               (required) record the specified
                                                                                    commit hash to agile accelerator

  -m, --message=message                                                             commit message to record

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:log:commit -c 44eaebf64
  sfdx saatc:agile:log:commit --commit=44eaebf647d74b8296f3a1f5593fe1c2d6c95ca6 --message="added button to screen"
```

## `sfdx saatc:agile:log:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will create release_component__c entries for every item in the working branch's manifest file

```
will create release_component__c entries for every item in the working branch's manifest file

USAGE
  $ sfdx saatc:agile:log:components [-M] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -M, --manualconfigurations                                                        start an interactive session to
                                                                                    write manual config steps

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:components
  sfdx saatc:agile:components -M
```

## `sfdx saatc:agile:log:tests [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

This command is still under construction

```
This command is still under construction

USAGE
  $ sfdx saatc:agile:log:tests [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  deprecated
  deprecated
```

## `sfdx saatc:agile:log:validate [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

record the latest validation results to agile accelerator

```
record the latest validation results to agile accelerator

USAGE
  $ sfdx saatc:agile:log:validate [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLE
  sfdx saatc:agile:log:validate
```

## `sfdx saatc:agile:open [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

opens the work item in agile accelerator

```
opens the work item in agile accelerator

USAGE
  $ sfdx saatc:agile:open [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile:open
```

## `sfdx saatc:agile:report [--validation] [--test] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

prints all information related to the current work item

```
prints all information related to the current work item

USAGE
  $ sfdx saatc:agile:report [--validation] [--test] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --test                                                                            print full test results to the
                                                                                    terminal

  --validation                                                                      print full validation results to the
                                                                                    terminal

EXAMPLES
  sfdx saatc:agile:report
  sfdx saatc:agile:report --validation --test
```

## `sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

warning: intended for use only with the bamboo agent. logs job information to agile accelerator

```
warning: intended for use only with the bamboo agent. logs job information to agile accelerator

USAGE
  $ sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] 
  [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -e, --environment=environment                                                     environment which this build was ran
                                                                                    against

  -i, --jobid=jobid                                                                 (required) the id of the validation
                                                                                    job to record

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:bamboo:log -i 007Ar00000aAzGyWAG
  sfdx saatc:bamboo:log --jobid=007Ar00000aAzGyWAG --environment=UAT
```

## `sfdx saatc:bamboo:poll -j <string> [-e <string>] [--script] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

warning: intended for use only with the bamboo agent. queries job status from the target org with details from agile accelerator

```
warning: intended for use only with the bamboo agent. queries job status from the target org with details from agile accelerator

USAGE
  $ sfdx saatc:bamboo:poll -j <string> [-e <string>] [--script] [-u <string>] [--apiversion <string>] [--json] 
  [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -e, --environment=environment                                                     environment which this build was ran
                                                                                    against

  -j, --jobenvironmentusername=jobenvironmentusername                               (required) the alias for the
                                                                                    environment in which the build was
                                                                                    executed

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLE
  sfdx saatc:bamboo:poll -j not.a.realusername@salesforce.com -e Validation
```

## `sfdx saatc:cleanup [--all | -V | -T | -W | -A] [--noprompt] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

remove cached config variables

```
remove cached config variables

USAGE
  $ sfdx saatc:cleanup [--all | -V | -T | -W | -A] [--noprompt] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -A, --alias                                                                       clears alias configurations
  -T, --tests                                                                       remove test results
  -V, --validate                                                                    remove validation results
  -W, --workid                                                                      remove cached work ids
  --all                                                                             remove all configurations
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --noprompt                                                                        skip all deletion confirmations

EXAMPLES
  sfdx saatc:cleanup -V -T
  sfdx saatc:cleanup --all
```

## `sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

logs the user into the currently set org

```
logs the user into the currently set org

USAGE
  $ sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -D, --default                                                                     add a new path to the preferred
                                                                                    paths

  -t, --target=target                                                               redirect the user upon login to the
                                                                                    saved target definition

  -u, --alias=alias                                                                 log the user into the aliased org
                                                                                    declared should that org be
                                                                                    authorized

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:login
  sfdx saatc:login -u myDevOrg -t settings
```

## `sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

add a new path to the saved paths

```
add a new path to the saved paths

USAGE
  $ sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will add the specified
                                                                                    target entry from the saved targets.

  -u, --uri=uri                                                                     (required) will set the uri for the
                                                                                    specified target to redirect to upon
                                                                                    login.

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:add -t setup -u lightning/setup/SetupOneHome/home
```

## `sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

removes a path from the saved login paths

```
removes a path from the saved login paths

USAGE
  $ sfdx saatc:login:remove -t <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will remove the specified
                                                                                    target entry from the saved targets

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:remove -t deprecatedpath
```

## `sfdx saatc:manifest:build [-v <string>] [-W] [-B -c <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:manifest:build [-v <string>] [-W] [-B -c <string>] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -B, --commitgeneratedmanifest                                                     commit the generated manifest file
                                                                                    immediately upon creation

  -W, --includeworkingfiles                                                         if set, will include uncommitted
                                                                                    changes into the manifest from your
                                                                                    current working directory

  -c, --commit=commit                                                               add the changes specified by the
                                                                                    commit hash to the manifest

  -v, --api=api                                                                     api version for the built manifest
                                                                                    if different than what is already
                                                                                    declared

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:deploy [-u <string>] [--apiversion <string>] [--quiet] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:manifest:deploy [-u <string>] [--apiversion <string>] [--quiet] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --quiet                                                                           nothing emitted stdout

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:show [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:manifest:show [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:test [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

This command is still under construction

```
This command is still under construction

USAGE
  $ sfdx saatc:manifest:test [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  deprecated
  deprecated
```

## `sfdx saatc:manifest:test:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

deprecated

```
deprecated

USAGE
  $ sfdx saatc:manifest:test:report [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          deprecated

EXAMPLES
  desprecated
  desprecated
```

## `sfdx saatc:manifest:validate [-W] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will run a validation against the configured validation org with the working branch manifest file

```
will run a validation against the configured validation org with the working branch manifest file

USAGE
  $ sfdx saatc:manifest:validate [-W] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -W, --nowait                                                                      execute the validate asynchronously
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:manifest:validate:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will record the commit information to agile accelerator

```
will record the commit information to agile accelerator

USAGE
  $ sfdx saatc:manifest:validate:report [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:log:commit -c 44eaebf64
  sfdx saatc:agile:log:commit --commit=44eaebf647d74b8296f3a1f5593fe1c2d6c95ca6 --message="added button to screen"
```
<!-- commandsstop -->
* [`sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagile---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:commit -c <string> [--script] [-m <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilecommit--c-string---script--m-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilecomponents--m---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:configure -u <string> -s agile|validate [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagileconfigure--u-string--s-agilevalidate---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:validate [-W] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilevalidate--w---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:validate:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilevalidatereport---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbamboolog--i-string---script--e-string--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:bamboo:poll -j <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbamboopoll--j-string---script--e-string--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:bamboo:test [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbambootest--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:build:deploy [-N] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbuilddeploy--n--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:build:package [-v <string>] [-W] [-B -c <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbuildpackage--v-string--w--b--c-string---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:deploy [-p <string> -F] [-c <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcdeploy--p-string--f--c-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatclogin--u-string--t-string--d---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginadd--t-string--u-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginremove--t-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)

## `sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

retrieve the defined org alias for agile accelerator

```
retrieve the defined org alias for agile accelerator

USAGE
  $ sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile
```

## `sfdx saatc:agile:commit -c <string> [--script] [-m <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will record the commit information to agile accelerator

```
will record the commit information to agile accelerator

USAGE
  $ sfdx saatc:agile:commit -c <string> [--script] [-m <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -c, --commit=commit                                                               (required) record the specified
                                                                                    commit hash to agile accelerator

  -m, --message=message                                                             commit message to record

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:commit -c 44eaebf64
  sfdx saatc:agile:commit --commit=44eaebf647d74b8296f3a1f5593fe1c2d6c95ca6
```

## `sfdx saatc:agile:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will create release_component__c entries for every item in the working branch's manifest file

```
will create release_component__c entries for every item in the working branch's manifest file

USAGE
  $ sfdx saatc:agile:components [-M] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -M, --manualconfigurations                                                        start an interactive session to
                                                                                    write manual config steps

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:components
  sfdx saatc:agile:components -M
```

## `sfdx saatc:agile:configure -u <string> -s agile|validate [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

set the org alias for the deployment related orgs

```
set the org alias for the deployment related orgs

USAGE
  $ sfdx saatc:agile:configure -u <string> -s agile|validate [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -s, --setting=(agile|validate)                                                    (required) the setting to write to

  -u, --alias=alias                                                                 (required) the alias or username of
                                                                                    the org that for the target setting

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile:configure -s agile -u myAgileOrg
```

## `sfdx saatc:agile:validate [-W] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will run a validation against the configured validation org with the working branch manifest file

```
will run a validation against the configured validation org with the working branch manifest file

USAGE
  $ sfdx saatc:agile:validate [-W] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -W, --nowait                                                                      execute the validate asynchronously
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:agile:validate:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will record the commit information to agile accelerator

```
will record the commit information to agile accelerator

USAGE
  $ sfdx saatc:agile:validate:report [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:commit -c 44eaebf64
  sfdx saatc:agile:commit --commit=44eaebf647d74b8296f3a1f5593fe1c2d6c95ca6
```

## `sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will run a validation against the configured validation org with the working branch manifest file

```
will run a validation against the configured validation org with the working branch manifest file

USAGE
  $ sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] 
  [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -e, --environment=environment                                                     environment which this build is
                                                                                    against

  -i, --jobid=jobid                                                                 (required) the id of the validation
                                                                                    job to record

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:bamboo:poll -j <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will run a validation against the configured validation org with the working branch manifest file

```
will run a validation against the configured validation org with the working branch manifest file

USAGE
  $ sfdx saatc:bamboo:poll -j <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] 
  [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -e, --environment=environment                                                     environment which this build is
                                                                                    against

  -j, --jobenvironmentusername=jobenvironmentusername                               (required) the alias for the
                                                                                    environment in which the build was
                                                                                    executed

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:bamboo:test [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will run a validation against the configured validation org with the working branch manifest file

```
will run a validation against the configured validation org with the working branch manifest file

USAGE
  $ sfdx saatc:bamboo:test [-u <string>] [--apiversion <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:build:deploy [-N] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:build:deploy [-N] [-u <string>] [--apiversion <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -N, --novalidate                                                                  will skip the validation phase and
                                                                                    go to a straigh deploy

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:build:package [-v <string>] [-W] [-B -c <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:build:package [-v <string>] [-W] [-B -c <string>] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -B, --commitgeneratedmanifest                                                     commit the generated manifest file
                                                                                    immediately upon creation

  -W, --includeworkingfiles                                                         if set, will include uncommitted
                                                                                    changes into the manifest from your
                                                                                    current working directory

  -c, --commit=commit                                                               add the changes specified by the
                                                                                    commit hash to the manifest

  -v, --api=api                                                                     api version for the built manifest
                                                                                    if different than what is already
                                                                                    declared

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:deploy [-p <string> -F] [-c <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

deploy files to a salesforce org

```
deploy files to a salesforce org

USAGE
  $ sfdx saatc:deploy [-p <string> -F] [-c <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -F, --fulldeploy                                                                  deploy all files indicated in the
                                                                                    manifest

  -c, --commit=commit                                                               add the changes specified by the
                                                                                    commit hash to the manifest

  -p, --packagename=packagename                                                     the name of the manifest file to
                                                                                    deploy. must be specified when
                                                                                    running in full deployment mode

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:deploy
  sfdx saatc:deploy -F -p "package.xml"
  sfdx saatc:deploy -c e9a6a03
```

## `sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

logs the user into the currently set org

```
logs the user into the currently set org

USAGE
  $ sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -D, --default                                                                     add a new path to the preferred
                                                                                    paths

  -t, --target=target                                                               redirect the user upon login to the
                                                                                    saved target definition

  -u, --alias=alias                                                                 log the user into the aliased org
                                                                                    declared should that org be
                                                                                    authorized

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:login
  sfdx saatc:login -u myDevOrg -t settings
```

## `sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

add a new path to the saved paths

```
add a new path to the saved paths

USAGE
  $ sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will add the specified
                                                                                    target entry from the saved targets.

  -u, --uri=uri                                                                     (required) will set the uri for the
                                                                                    specified target to redirect to upon
                                                                                    login.

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:add -t setup -u lightning/setup/SetupOneHome/home
```

## `sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

removes a path from the saved login paths

```
removes a path from the saved login paths

USAGE
  $ sfdx saatc:login:remove -t <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will remove the specified
                                                                                    target entry from the saved targets

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:remove -t deprecatedpath
```
<!-- commandsstop -->
* [`sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagile---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:commit -c <string> [--script] [-m <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilecommit--c-string---script--m-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilecomponents--m---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:configure -u <string> -s agile|validate [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagileconfigure--u-string--s-agilevalidate---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:validate [-W] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilevalidate--w---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:validate:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilevalidatereport---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbamboolog--i-string---script--e-string--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:bamboo:poll -j <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbamboopoll--j-string---script--e-string--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:build:deploy [-N] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbuilddeploy--n--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:build:package [-v <string>] [-W] [-B -c <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbuildpackage--v-string--w--b--c-string---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:deploy [-p <string> -F] [-c <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcdeploy--p-string--f--c-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatclogin--u-string--t-string--d---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginadd--t-string--u-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginremove--t-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)

## `sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

retrieve the defined org alias for agile accelerator

```
retrieve the defined org alias for agile accelerator

USAGE
  $ sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile
```

## `sfdx saatc:agile:commit -c <string> [--script] [-m <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will record the commit information to agile accelerator

```
will record the commit information to agile accelerator

USAGE
  $ sfdx saatc:agile:commit -c <string> [--script] [-m <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -c, --commit=commit                                                               (required) record the specified
                                                                                    commit hash to agile accelerator

  -m, --message=message                                                             commit message to record

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:commit -c 44eaebf64
  sfdx saatc:agile:commit --commit=44eaebf647d74b8296f3a1f5593fe1c2d6c95ca6
```

## `sfdx saatc:agile:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will create release_component__c entries for every item in the working branch's manifest file

```
will create release_component__c entries for every item in the working branch's manifest file

USAGE
  $ sfdx saatc:agile:components [-M] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -M, --manualconfigurations                                                        start an interactive session to
                                                                                    write manual config steps

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:components
  sfdx saatc:agile:components -M
```

## `sfdx saatc:agile:configure -u <string> -s agile|validate [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

set the org alias for the deployment related orgs

```
set the org alias for the deployment related orgs

USAGE
  $ sfdx saatc:agile:configure -u <string> -s agile|validate [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -s, --setting=(agile|validate)                                                    (required) the setting to write to

  -u, --alias=alias                                                                 (required) the alias or username of
                                                                                    the org that for the target setting

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile:configure -s agile -u myAgileOrg
```

## `sfdx saatc:agile:validate [-W] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will run a validation against the configured validation org with the working branch manifest file

```
will run a validation against the configured validation org with the working branch manifest file

USAGE
  $ sfdx saatc:agile:validate [-W] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -W, --nowait                                                                      execute the validate asynchronously
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:agile:validate:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will record the commit information to agile accelerator

```
will record the commit information to agile accelerator

USAGE
  $ sfdx saatc:agile:validate:report [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:commit -c 44eaebf64
  sfdx saatc:agile:commit --commit=44eaebf647d74b8296f3a1f5593fe1c2d6c95ca6
```

## `sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will run a validation against the configured validation org with the working branch manifest file

```
will run a validation against the configured validation org with the working branch manifest file

USAGE
  $ sfdx saatc:bamboo:log -i <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] 
  [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -e, --environment=environment                                                     environment which this build is
                                                                                    against

  -i, --jobid=jobid                                                                 (required) the id of the validation
                                                                                    job to record

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:bamboo:poll -j <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will run a validation against the configured validation org with the working branch manifest file

```
will run a validation against the configured validation org with the working branch manifest file

USAGE
  $ sfdx saatc:bamboo:poll -j <string> [--script] [-e <string>] [-u <string>] [--apiversion <string>] [--json] 
  [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -e, --environment=environment                                                     environment which this build is
                                                                                    against

  -j, --jobenvironmentusername=jobenvironmentusername                               (required) the alias for the
                                                                                    environment in which the build was
                                                                                    executed

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:build:deploy [-N] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:build:deploy [-N] [-u <string>] [--apiversion <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -N, --novalidate                                                                  will skip the validation phase and
                                                                                    go to a straigh deploy

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:build:package [-v <string>] [-W] [-B -c <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:build:package [-v <string>] [-W] [-B -c <string>] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -B, --commitgeneratedmanifest                                                     commit the generated manifest file
                                                                                    immediately upon creation

  -W, --includeworkingfiles                                                         if set, will include uncommitted
                                                                                    changes into the manifest from your
                                                                                    current working directory

  -c, --commit=commit                                                               add the changes specified by the
                                                                                    commit hash to the manifest

  -v, --api=api                                                                     api version for the built manifest
                                                                                    if different than what is already
                                                                                    declared

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:deploy [-p <string> -F] [-c <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

deploy files to a salesforce org

```
deploy files to a salesforce org

USAGE
  $ sfdx saatc:deploy [-p <string> -F] [-c <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -F, --fulldeploy                                                                  deploy all files indicated in the
                                                                                    manifest

  -c, --commit=commit                                                               add the changes specified by the
                                                                                    commit hash to the manifest

  -p, --packagename=packagename                                                     the name of the manifest file to
                                                                                    deploy. must be specified when
                                                                                    running in full deployment mode

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:deploy
  sfdx saatc:deploy -F -p "package.xml"
  sfdx saatc:deploy -c e9a6a03
```

## `sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

logs the user into the currently set org

```
logs the user into the currently set org

USAGE
  $ sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -D, --default                                                                     add a new path to the preferred
                                                                                    paths

  -t, --target=target                                                               redirect the user upon login to the
                                                                                    saved target definition

  -u, --alias=alias                                                                 log the user into the aliased org
                                                                                    declared should that org be
                                                                                    authorized

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:login
  sfdx saatc:login -u myDevOrg -t settings
```

## `sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

add a new path to the saved paths

```
add a new path to the saved paths

USAGE
  $ sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will add the specified
                                                                                    target entry from the saved targets.

  -u, --uri=uri                                                                     (required) will set the uri for the
                                                                                    specified target to redirect to upon
                                                                                    login.

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:add -t setup -u lightning/setup/SetupOneHome/home
```

## `sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

removes a path from the saved login paths

```
removes a path from the saved login paths

USAGE
  $ sfdx saatc:login:remove -t <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will remove the specified
                                                                                    target entry from the saved targets

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:remove -t deprecatedpath
```
<!-- commandsstop -->
* [`sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagile---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:commit -c <string> [--script] [-m <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilecommit--c-string---script--m-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilecomponents--m---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:configure -u <string> -s agile|validate [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagileconfigure--u-string--s-agilevalidate---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:validate [-W] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilevalidate--w---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:validate:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilevalidatereport---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:build:deploy [-N] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbuilddeploy--n--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:build:package [-v <string>] [-W] [-B -c <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbuildpackage--v-string--w--b--c-string---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:deploy [-p <string> -F] [-c <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcdeploy--p-string--f--c-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatclogin--u-string--t-string--d---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginadd--t-string--u-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginremove--t-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)

## `sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

retrieve the defined org alias for agile accelerator

```
retrieve the defined org alias for agile accelerator

USAGE
  $ sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile
```

## `sfdx saatc:agile:commit -c <string> [--script] [-m <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will record the commit information to agile accelerator

```
will record the commit information to agile accelerator

USAGE
  $ sfdx saatc:agile:commit -c <string> [--script] [-m <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -c, --commit=commit                                                               (required) record the specified
                                                                                    commit hash to agile accelerator

  -m, --message=message                                                             commit message to record

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:commit -c 44eaebf64
  sfdx saatc:agile:commit --commit=44eaebf647d74b8296f3a1f5593fe1c2d6c95ca6
```

## `sfdx saatc:agile:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will create release_component__c entries for every item in the working branch's manifest file

```
will create release_component__c entries for every item in the working branch's manifest file

USAGE
  $ sfdx saatc:agile:components [-M] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -M, --manualconfigurations                                                        start an interactive session to
                                                                                    write manual config steps

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:components
  sfdx saatc:agile:components -M
```

## `sfdx saatc:agile:configure -u <string> -s agile|validate [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

set the org alias for the deployment related orgs

```
set the org alias for the deployment related orgs

USAGE
  $ sfdx saatc:agile:configure -u <string> -s agile|validate [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -s, --setting=(agile|validate)                                                    (required) the setting to write to

  -u, --alias=alias                                                                 (required) the alias or username of
                                                                                    the org that for the target setting

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile:configure -s agile -u myAgileOrg
```

## `sfdx saatc:agile:validate [-W] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will run a validation against the configured validation org with the working branch manifest file

```
will run a validation against the configured validation org with the working branch manifest file

USAGE
  $ sfdx saatc:agile:validate [-W] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -W, --nowait                                                                      execute the validate asynchronously
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:agile:validate:report [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will record the commit information to agile accelerator

```
will record the commit information to agile accelerator

USAGE
  $ sfdx saatc:agile:validate:report [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:commit -c 44eaebf64
  sfdx saatc:agile:commit --commit=44eaebf647d74b8296f3a1f5593fe1c2d6c95ca6
```

## `sfdx saatc:build:deploy [-N] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:build:deploy [-N] [-u <string>] [--apiversion <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -N, --novalidate                                                                  will skip the validation phase and
                                                                                    go to a straigh deploy

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:build:package [-v <string>] [-W] [-B -c <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:build:package [-v <string>] [-W] [-B -c <string>] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -B, --commitgeneratedmanifest                                                     commit the generated manifest file
                                                                                    immediately upon creation

  -W, --includeworkingfiles                                                         if set, will include uncommitted
                                                                                    changes into the manifest from your
                                                                                    current working directory

  -c, --commit=commit                                                               add the changes specified by the
                                                                                    commit hash to the manifest

  -v, --api=api                                                                     api version for the built manifest
                                                                                    if different than what is already
                                                                                    declared

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:deploy [-p <string> -F] [-c <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

deploy files to a salesforce org

```
deploy files to a salesforce org

USAGE
  $ sfdx saatc:deploy [-p <string> -F] [-c <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -F, --fulldeploy                                                                  deploy all files indicated in the
                                                                                    manifest

  -c, --commit=commit                                                               add the changes specified by the
                                                                                    commit hash to the manifest

  -p, --packagename=packagename                                                     the name of the manifest file to
                                                                                    deploy. must be specified when
                                                                                    running in full deployment mode

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:deploy
  sfdx saatc:deploy -F -p "package.xml"
  sfdx saatc:deploy -c e9a6a03
```

## `sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

logs the user into the currently set org

```
logs the user into the currently set org

USAGE
  $ sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -D, --default                                                                     add a new path to the preferred
                                                                                    paths

  -t, --target=target                                                               redirect the user upon login to the
                                                                                    saved target definition

  -u, --alias=alias                                                                 log the user into the aliased org
                                                                                    declared should that org be
                                                                                    authorized

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:login
  sfdx saatc:login -u myDevOrg -t settings
```

## `sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

add a new path to the saved paths

```
add a new path to the saved paths

USAGE
  $ sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will add the specified
                                                                                    target entry from the saved targets.

  -u, --uri=uri                                                                     (required) will set the uri for the
                                                                                    specified target to redirect to upon
                                                                                    login.

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:add -t setup -u lightning/setup/SetupOneHome/home
```

## `sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

removes a path from the saved login paths

```
removes a path from the saved login paths

USAGE
  $ sfdx saatc:login:remove -t <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will remove the specified
                                                                                    target entry from the saved targets

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:remove -t deprecatedpath
```
<!-- commandsstop -->
* [`sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagile---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:commit -c <string> [--script] [-m <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilecommit--c-string---script--m-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilecomponents--m---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:configure -u <string> -s agile|validate [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagileconfigure--u-string--s-agilevalidate---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:validate [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilevalidate--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:build:deploy [-N] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbuilddeploy--n--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:build:package [-v <string>] [-W] [-B -c <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbuildpackage--v-string--w--b--c-string---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:deploy [-p <string> -F] [-c <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcdeploy--p-string--f--c-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatclogin--u-string--t-string--d---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginadd--t-string--u-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginremove--t-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)

## `sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

retrieve the defined org alias for agile accelerator

```
retrieve the defined org alias for agile accelerator

USAGE
  $ sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile
```

## `sfdx saatc:agile:commit -c <string> [--script] [-m <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will record the commit information to agile accelerator

```
will record the commit information to agile accelerator

USAGE
  $ sfdx saatc:agile:commit -c <string> [--script] [-m <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -c, --commit=commit                                                               (required) record the specified
                                                                                    commit hash to agile accelerator

  -m, --message=message                                                             commit message to record

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:commit -c 44eaebf64
  sfdx saatc:agile:commit --commit=44eaebf647d74b8296f3a1f5593fe1c2d6c95ca6
```

## `sfdx saatc:agile:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will create release_component__c entries for every item in the working branch's manifest file

```
will create release_component__c entries for every item in the working branch's manifest file

USAGE
  $ sfdx saatc:agile:components [-M] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -M, --manualconfigurations                                                        start an interactive session to
                                                                                    write manual config steps

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:components
  sfdx saatc:agile:components -M
```

## `sfdx saatc:agile:configure -u <string> -s agile|validate [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

set the org alias for the deployment related orgs

```
set the org alias for the deployment related orgs

USAGE
  $ sfdx saatc:agile:configure -u <string> -s agile|validate [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -s, --setting=(agile|validate)                                                    (required) the setting to write to

  -u, --alias=alias                                                                 (required) the alias or username of
                                                                                    the org that for the target setting

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile:configure -s agile -u myAgileOrg
```

## `sfdx saatc:agile:validate [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will run a validation against the configured validation org with the working branch manifest file

```
will run a validation against the configured validation org with the working branch manifest file

USAGE
  $ sfdx saatc:agile:validate [-u <string>] [--apiversion <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:build:deploy [-N] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:build:deploy [-N] [-u <string>] [--apiversion <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -N, --novalidate                                                                  will skip the validation phase and
                                                                                    go to a straigh deploy

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:build:package [-v <string>] [-W] [-B -c <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:build:package [-v <string>] [-W] [-B -c <string>] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -B, --commitgeneratedmanifest                                                     commit the generated manifest file
                                                                                    immediately upon creation

  -W, --includeworkingfiles                                                         if set, will include uncommitted
                                                                                    changes into the manifest from your
                                                                                    current working directory

  -c, --commit=commit                                                               add the changes specified by the
                                                                                    commit hash to the manifest

  -v, --api=api                                                                     api version for the built manifest
                                                                                    if different than what is already
                                                                                    declared

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:deploy [-p <string> -F] [-c <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

deploy files to a salesforce org

```
deploy files to a salesforce org

USAGE
  $ sfdx saatc:deploy [-p <string> -F] [-c <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -F, --fulldeploy                                                                  deploy all files indicated in the
                                                                                    manifest

  -c, --commit=commit                                                               add the changes specified by the
                                                                                    commit hash to the manifest

  -p, --packagename=packagename                                                     the name of the manifest file to
                                                                                    deploy. must be specified when
                                                                                    running in full deployment mode

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:deploy
  sfdx saatc:deploy -F -p "package.xml"
  sfdx saatc:deploy -c e9a6a03
```

## `sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

logs the user into the currently set org

```
logs the user into the currently set org

USAGE
  $ sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -D, --default                                                                     add a new path to the preferred
                                                                                    paths

  -t, --target=target                                                               redirect the user upon login to the
                                                                                    saved target definition

  -u, --alias=alias                                                                 log the user into the aliased org
                                                                                    declared should that org be
                                                                                    authorized

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:login
  sfdx saatc:login -u myDevOrg -t settings
```

## `sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

add a new path to the saved paths

```
add a new path to the saved paths

USAGE
  $ sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will add the specified
                                                                                    target entry from the saved targets.

  -u, --uri=uri                                                                     (required) will set the uri for the
                                                                                    specified target to redirect to upon
                                                                                    login.

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:add -t setup -u lightning/setup/SetupOneHome/home
```

## `sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

removes a path from the saved login paths

```
removes a path from the saved login paths

USAGE
  $ sfdx saatc:login:remove -t <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will remove the specified
                                                                                    target entry from the saved targets

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:remove -t deprecatedpath
```
<!-- commandsstop -->
* [`sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagile---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:commit -c <string> [--script] [-m <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilecommit--c-string---script--m-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilecomponents--m---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:configure -u <string> -s agile|validate [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagileconfigure--u-string--s-agilevalidate---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:validate [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilevalidate--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:build:deploy [-N] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbuilddeploy--n--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:build:package [-v <string>] [-W] [-B -c <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbuildpackage--v-string--w--b--c-string---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:deploy [-p <string> -F] [-c <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcdeploy--p-string--f--c-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatclogin--u-string--t-string--d---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginadd--t-string--u-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginremove--t-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)

## `sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

retrieve the defined org alias for agile accelerator

```
retrieve the defined org alias for agile accelerator

USAGE
  $ sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile
```

## `sfdx saatc:agile:commit -c <string> [--script] [-m <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will record the commit information to agile accelerator

```
will record the commit information to agile accelerator

USAGE
  $ sfdx saatc:agile:commit -c <string> [--script] [-m <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -c, --commit=commit                                                               (required) record the specified
                                                                                    commit hash to agile accelerator

  -m, --message=message                                                             commit message to record

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:commit -c 44eaebf64
  sfdx saatc:agile:commit --commit=44eaebf647d74b8296f3a1f5593fe1c2d6c95ca6
```

## `sfdx saatc:agile:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will create release_component__c entries for every item in the working branch's manifest file

```
will create release_component__c entries for every item in the working branch's manifest file

USAGE
  $ sfdx saatc:agile:components [-M] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -M, --manualconfigurations                                                        start an interactive session to
                                                                                    write manual config steps

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:components
  sfdx saatc:agile:components -M
```

## `sfdx saatc:agile:configure -u <string> -s agile|validate [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

set the org alias for the deployment related orgs

```
set the org alias for the deployment related orgs

USAGE
  $ sfdx saatc:agile:configure -u <string> -s agile|validate [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -s, --setting=(agile|validate)                                                    (required) the setting to write to

  -u, --alias=alias                                                                 (required) the alias or username of
                                                                                    the org that for the target setting

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile:configure -s agile -u myAgileOrg
```

## `sfdx saatc:agile:validate [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will run a validation against the configured validation org with the working branch manifest file

```
will run a validation against the configured validation org with the working branch manifest file

USAGE
  $ sfdx saatc:agile:validate [-u <string>] [--apiversion <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:build:deploy [-N] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:build:deploy [-N] [-u <string>] [--apiversion <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -N, --novalidate                                                                  will skip the validation phase and
                                                                                    go to a straigh deploy

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:build:package [-v <string>] [-W] [-B -c <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:build:package [-v <string>] [-W] [-B -c <string>] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -B, --commitgeneratedmanifest                                                     commit the generated manifest file
                                                                                    immediately upon creation

  -W, --includeworkingfiles                                                         if set, will include uncommitted
                                                                                    changes into the manifest from your
                                                                                    current working directory

  -c, --commit=commit                                                               add the changes specified by the
                                                                                    commit hash to the manifest

  -v, --api=api                                                                     api version for the built manifest
                                                                                    if different than what is already
                                                                                    declared

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:deploy [-p <string> -F] [-c <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

deploy files to a salesforce org

```
deploy files to a salesforce org

USAGE
  $ sfdx saatc:deploy [-p <string> -F] [-c <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -F, --fulldeploy                                                                  deploy all files indicated in the
                                                                                    manifest

  -c, --commit=commit                                                               add the changes specified by the
                                                                                    commit hash to the manifest

  -p, --packagename=packagename                                                     the name of the manifest file to
                                                                                    deploy. must be specified when
                                                                                    running in full deployment mode

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:deploy
  sfdx saatc:deploy -F -p "package.xml"
  sfdx saatc:deploy -c e9a6a03
```

## `sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

logs the user into the currently set org

```
logs the user into the currently set org

USAGE
  $ sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -D, --default                                                                     add a new path to the preferred
                                                                                    paths

  -t, --target=target                                                               redirect the user upon login to the
                                                                                    saved target definition

  -u, --alias=alias                                                                 log the user into the aliased org
                                                                                    declared should that org be
                                                                                    authorized

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:login
  sfdx saatc:login -u myDevOrg -t settings
```

## `sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

add a new path to the saved paths

```
add a new path to the saved paths

USAGE
  $ sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will add the specified
                                                                                    target entry from the saved targets.

  -u, --uri=uri                                                                     (required) will set the uri for the
                                                                                    specified target to redirect to upon
                                                                                    login.

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:add -t setup -u lightning/setup/SetupOneHome/home
```

## `sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

removes a path from the saved login paths

```
removes a path from the saved login paths

USAGE
  $ sfdx saatc:login:remove -t <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will remove the specified
                                                                                    target entry from the saved targets

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:remove -t deprecatedpath
```
<!-- commandsstop -->
* [`sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagile---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:commit -c <string> [--script] [-m <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilecommit--c-string---script--m-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilecomponents--m---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:configure -u <string> -s agile|validate [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagileconfigure--u-string--s-agilevalidate---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:validate [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilevalidate--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:build:deploy [-N] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbuilddeploy--n--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:build:package [-v <string>] [-W] [-B -c <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbuildpackage--v-string--w--b--c-string---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:deploy [-p <string> -F] [-c <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcdeploy--p-string--f--c-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatclogin--u-string--t-string--d---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginadd--t-string--u-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginremove--t-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)

## `sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

retrieve the defined org alias for agile accelerator

```
retrieve the defined org alias for agile accelerator

USAGE
  $ sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile
```

## `sfdx saatc:agile:commit -c <string> [--script] [-m <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will record the commit information to agile accelerator

```
will record the commit information to agile accelerator

USAGE
  $ sfdx saatc:agile:commit -c <string> [--script] [-m <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -c, --commit=commit                                                               (required) record the specified
                                                                                    commit hash to agile accelerator

  -m, --message=message                                                             commit message to record

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:commit -c 44eaebf64
  sfdx saatc:agile:commit --commit=44eaebf647d74b8296f3a1f5593fe1c2d6c95ca6
```

## `sfdx saatc:agile:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will create release_component__c entries for every item in the working branch's manifest file

```
will create release_component__c entries for every item in the working branch's manifest file

USAGE
  $ sfdx saatc:agile:components [-M] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -M, --manualconfigurations                                                        start an interactive session to
                                                                                    write manual config steps

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:components
  sfdx saatc:agile:components -M
```

## `sfdx saatc:agile:configure -u <string> -s agile|validate [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

set the org alias for the deployment related orgs

```
set the org alias for the deployment related orgs

USAGE
  $ sfdx saatc:agile:configure -u <string> -s agile|validate [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -s, --setting=(agile|validate)                                                    (required) the setting to write to

  -u, --alias=alias                                                                 (required) the alias or username of
                                                                                    the org that for the target setting

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile:configure -s agile -u myAgileOrg
```

## `sfdx saatc:agile:validate [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will run a validation against the configured validation org with the working branch manifest file

```
will run a validation against the configured validation org with the working branch manifest file

USAGE
  $ sfdx saatc:agile:validate [-u <string>] [--apiversion <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:build:deploy [-N] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:build:deploy [-N] [-u <string>] [--apiversion <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -N, --novalidate                                                                  will skip the validation phase and
                                                                                    go to a straigh deploy

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:build:package [-v <string>] [-W] [-B -c <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:build:package [-v <string>] [-W] [-B -c <string>] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -B, --commitgeneratedmanifest                                                     commit the generated manifest file
                                                                                    immediately upon creation

  -W, --includeworkingfiles                                                         if set, will include uncommitted
                                                                                    changes into the manifest from your
                                                                                    current working directory

  -c, --commit=commit                                                               add the changes specified by the
                                                                                    commit hash to the manifest

  -v, --api=api                                                                     api version for the built manifest
                                                                                    if different than what is already
                                                                                    declared

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:deploy [-p <string> -F] [-c <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

deploy files to a salesforce org

```
deploy files to a salesforce org

USAGE
  $ sfdx saatc:deploy [-p <string> -F] [-c <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -F, --fulldeploy                                                                  deploy all files indicated in the
                                                                                    manifest

  -c, --commit=commit                                                               add the changes specified by the
                                                                                    commit hash to the manifest

  -p, --packagename=packagename                                                     the name of the manifest file to
                                                                                    deploy. must be specified when
                                                                                    running in full deployment mode

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:deploy
  sfdx saatc:deploy -F -p "package.xml"
  sfdx saatc:deploy -c e9a6a03
```

## `sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

logs the user into the currently set org

```
logs the user into the currently set org

USAGE
  $ sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -D, --default                                                                     add a new path to the preferred
                                                                                    paths

  -t, --target=target                                                               redirect the user upon login to the
                                                                                    saved target definition

  -u, --alias=alias                                                                 log the user into the aliased org
                                                                                    declared should that org be
                                                                                    authorized

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:login
  sfdx saatc:login -u myDevOrg -t settings
```

## `sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

add a new path to the saved paths

```
add a new path to the saved paths

USAGE
  $ sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will add the specified
                                                                                    target entry from the saved targets.

  -u, --uri=uri                                                                     (required) will set the uri for the
                                                                                    specified target to redirect to upon
                                                                                    login.

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:add -t setup -u lightning/setup/SetupOneHome/home
```

## `sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

removes a path from the saved login paths

```
removes a path from the saved login paths

USAGE
  $ sfdx saatc:login:remove -t <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will remove the specified
                                                                                    target entry from the saved targets

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:remove -t deprecatedpath
```
<!-- commandsstop -->
* [`sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagile---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:commit -c <string> [--script] [-m <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilecommit--c-string---script--m-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilecomponents--m---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:configure -u <string> -s agile|validate [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagileconfigure--u-string--s-agilevalidate---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:validate [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilevalidate--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:build:deploy [-N] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbuilddeploy--n--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:build:package [-v <string>] [-W] [-B -c <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbuildpackage--v-string--w--b--c-string---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:deploy [-p <string> -F] [-c <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcdeploy--p-string--f--c-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatclogin--u-string--t-string--d---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginadd--t-string--u-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginremove--t-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)

## `sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

retrieve the defined org alias for agile accelerator

```
retrieve the defined org alias for agile accelerator

USAGE
  $ sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile
```

## `sfdx saatc:agile:commit -c <string> [--script] [-m <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will record the commit information to agile accelerator

```
will record the commit information to agile accelerator

USAGE
  $ sfdx saatc:agile:commit -c <string> [--script] [-m <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -c, --commit=commit                                                               (required) record the specified
                                                                                    commit hash to agile accelerator

  -m, --message=message                                                             commit message to record

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:commit -c 44eaebf64
  sfdx saatc:agile:commit --commit=44eaebf647d74b8296f3a1f5593fe1c2d6c95ca6
```

## `sfdx saatc:agile:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will create release_component__c entries for every item in the working branch's manifest file

```
will create release_component__c entries for every item in the working branch's manifest file

USAGE
  $ sfdx saatc:agile:components [-M] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -M, --manualconfigurations                                                        start an interactive session to
                                                                                    write manual config steps

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:components
  sfdx saatc:agile:components -M
```

## `sfdx saatc:agile:configure -u <string> -s agile|validate [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

set the org alias for the deployment related orgs

```
set the org alias for the deployment related orgs

USAGE
  $ sfdx saatc:agile:configure -u <string> -s agile|validate [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -s, --setting=(agile|validate)                                                    (required) the setting to write to

  -u, --alias=alias                                                                 (required) the alias or username of
                                                                                    the org that for the target setting

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile:configure -s agile -u myAgileOrg
```

## `sfdx saatc:agile:validate [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will run a validation against the configured validation org with the working branch manifest file

```
will run a validation against the configured validation org with the working branch manifest file

USAGE
  $ sfdx saatc:agile:validate [-u <string>] [--apiversion <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:build:deploy [-N] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:build:deploy [-N] [-u <string>] [--apiversion <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -N, --novalidate                                                                  will skip the validation phase and
                                                                                    go to a straigh deploy

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:build:package [-v <string>] [-W] [-B -c <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:build:package [-v <string>] [-W] [-B -c <string>] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -B, --commitgeneratedmanifest                                                     commit the generated manifest file
                                                                                    immediately upon creation

  -W, --includeworkingfiles                                                         if set, will include uncommitted
                                                                                    changes into the manifest from your
                                                                                    current working directory

  -c, --commit=commit                                                               add the changes specified by the
                                                                                    commit hash to the manifest

  -v, --api=api                                                                     api version for the built manifest
                                                                                    if different than what is already
                                                                                    declared

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:deploy [-p <string> -F] [-c <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

deploy files to a salesforce org

```
deploy files to a salesforce org

USAGE
  $ sfdx saatc:deploy [-p <string> -F] [-c <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -F, --fulldeploy                                                                  deploy all files indicated in the
                                                                                    manifest

  -c, --commit=commit                                                               add the changes specified by the
                                                                                    commit hash to the manifest

  -p, --packagename=packagename                                                     the name of the manifest file to
                                                                                    deploy. must be specified when
                                                                                    running in full deployment mode

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:deploy
  sfdx saatc:deploy -F -p "package.xml"
  sfdx saatc:deploy -c e9a6a03
```

## `sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

logs the user into the currently set org

```
logs the user into the currently set org

USAGE
  $ sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -D, --default                                                                     add a new path to the preferred
                                                                                    paths

  -t, --target=target                                                               redirect the user upon login to the
                                                                                    saved target definition

  -u, --alias=alias                                                                 log the user into the aliased org
                                                                                    declared should that org be
                                                                                    authorized

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:login
  sfdx saatc:login -u myDevOrg -t settings
```

## `sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

add a new path to the saved paths

```
add a new path to the saved paths

USAGE
  $ sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will add the specified
                                                                                    target entry from the saved targets.

  -u, --uri=uri                                                                     (required) will set the uri for the
                                                                                    specified target to redirect to upon
                                                                                    login.

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:add -t setup -u lightning/setup/SetupOneHome/home
```

## `sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

removes a path from the saved login paths

```
removes a path from the saved login paths

USAGE
  $ sfdx saatc:login:remove -t <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will remove the specified
                                                                                    target entry from the saved targets

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:remove -t deprecatedpath
```
<!-- commandsstop -->
* [`sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagile---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:commit -c <string> [--script] [-m <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilecommit--c-string---script--m-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilecomponents--m---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:configure -u <string> -s agile|validate [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagileconfigure--u-string--s-agilevalidate---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:validate [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilevalidate--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:build:deploy [-N] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbuilddeploy--n--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:build:package [-v <string>] [-W] [-B -c <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbuildpackage--v-string--w--b--c-string---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:deploy [-p <string> -F] [-c <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcdeploy--p-string--f--c-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatclogin--u-string--t-string--d---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginadd--t-string--u-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginremove--t-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)

## `sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

retrieve the defined org alias for agile accelerator

```
retrieve the defined org alias for agile accelerator

USAGE
  $ sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile
```

## `sfdx saatc:agile:commit -c <string> [--script] [-m <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will record the commit information to agile accelerator

```
will record the commit information to agile accelerator

USAGE
  $ sfdx saatc:agile:commit -c <string> [--script] [-m <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -c, --commit=commit                                                               (required) record the specified
                                                                                    commit hash to agile accelerator

  -m, --message=message                                                             commit message to record

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:commit -c 44eaebf64
  sfdx saatc:agile:commit --commit=44eaebf647d74b8296f3a1f5593fe1c2d6c95ca6
```

## `sfdx saatc:agile:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will create release_component__c entries for every item in the working branch's manifest file

```
will create release_component__c entries for every item in the working branch's manifest file

USAGE
  $ sfdx saatc:agile:components [-M] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -M, --manualconfigurations                                                        start an interactive session to
                                                                                    write manual config steps

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:components
  sfdx saatc:agile:components -M
```

## `sfdx saatc:agile:configure -u <string> -s agile|validate [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

set the org alias for the deployment related orgs

```
set the org alias for the deployment related orgs

USAGE
  $ sfdx saatc:agile:configure -u <string> -s agile|validate [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -s, --setting=(agile|validate)                                                    (required) the setting to write to

  -u, --alias=alias                                                                 (required) the alias or username of
                                                                                    the org that for the target setting

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile:configure -s agile -u myAgileOrg
```

## `sfdx saatc:agile:validate [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will run a validation against the configured validation org with the working branch manifest file

```
will run a validation against the configured validation org with the working branch manifest file

USAGE
  $ sfdx saatc:agile:validate [-u <string>] [--apiversion <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:build:deploy [-N] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:build:deploy [-N] [-u <string>] [--apiversion <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -N, --novalidate                                                                  will skip the validation phase and
                                                                                    go to a straigh deploy

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:build:package [-v <string>] [-W] [-B -c <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:build:package [-v <string>] [-W] [-B -c <string>] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -B, --commitgeneratedmanifest                                                     commit the generated manifest file
                                                                                    immediately upon creation

  -W, --includeworkingfiles                                                         if set, will include uncommitted
                                                                                    changes into the manifest from your
                                                                                    current working directory

  -c, --commit=commit                                                               add the changes specified by the
                                                                                    commit hash to the manifest

  -v, --api=api                                                                     api version for the built manifest
                                                                                    if different than what is already
                                                                                    declared

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:deploy [-p <string> -F] [-c <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

deploy files to a salesforce org

```
deploy files to a salesforce org

USAGE
  $ sfdx saatc:deploy [-p <string> -F] [-c <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -F, --fulldeploy                                                                  deploy all files indicated in the
                                                                                    manifest

  -c, --commit=commit                                                               add the changes specified by the
                                                                                    commit hash to the manifest

  -p, --packagename=packagename                                                     the name of the manifest file to
                                                                                    deploy. must be specified when
                                                                                    running in full deployment mode

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:deploy
  sfdx saatc:deploy -F -p "package.xml"
  sfdx saatc:deploy -c e9a6a03
```

## `sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

logs the user into the currently set org

```
logs the user into the currently set org

USAGE
  $ sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -D, --default                                                                     add a new path to the preferred
                                                                                    paths

  -t, --target=target                                                               redirect the user upon login to the
                                                                                    saved target definition

  -u, --alias=alias                                                                 log the user into the aliased org
                                                                                    declared should that org be
                                                                                    authorized

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:login
  sfdx saatc:login -u myDevOrg -t settings
```

## `sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

add a new path to the saved paths

```
add a new path to the saved paths

USAGE
  $ sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will add the specified
                                                                                    target entry from the saved targets.

  -u, --uri=uri                                                                     (required) will set the uri for the
                                                                                    specified target to redirect to upon
                                                                                    login.

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:add -t setup -u lightning/setup/SetupOneHome/home
```

## `sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

removes a path from the saved login paths

```
removes a path from the saved login paths

USAGE
  $ sfdx saatc:login:remove -t <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will remove the specified
                                                                                    target entry from the saved targets

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:remove -t deprecatedpath
```
<!-- commandsstop -->
* [`sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagile---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:commit -c <string> [--script] [-m <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilecommit--c-string---script--m-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilecomponents--m---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:configure -u <string> -s agile|validate [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagileconfigure--u-string--s-agilevalidate---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:agile:validate [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcagilevalidate--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:build:deploy [-N] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbuilddeploy--n--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:build:package [-v <string>] [-W] [-B -c <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcbuildpackage--v-string--w--b--c-string---script---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:deploy [-p <string> -F] [-c <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcdeploy--p-string--f--c-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatclogin--u-string--t-string--d---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginadd--t-string--u-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-saatcloginremove--t-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)

## `sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

retrieve the defined org alias for agile accelerator

```
retrieve the defined org alias for agile accelerator

USAGE
  $ sfdx saatc:agile [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile
```

## `sfdx saatc:agile:commit -c <string> [--script] [-m <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will record the commit information to agile accelerator

```
will record the commit information to agile accelerator

USAGE
  $ sfdx saatc:agile:commit -c <string> [--script] [-m <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -c, --commit=commit                                                               (required) record the specified
                                                                                    commit hash to agile accelerator

  -m, --message=message                                                             commit message to record

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:commit -c 44eaebf64
  sfdx saatc:agile:commit --commit=44eaebf647d74b8296f3a1f5593fe1c2d6c95ca6
```

## `sfdx saatc:agile:components [-M] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will create release_component__c entries for every item in the working branch's manifest file

```
will create release_component__c entries for every item in the working branch's manifest file

USAGE
  $ sfdx saatc:agile:components [-M] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -M, --manualconfigurations                                                        start an interactive session to
                                                                                    write manual config steps

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:agile:components
  sfdx saatc:agile:components -M
```

## `sfdx saatc:agile:configure -u <string> -s agile|validate [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

set the org alias for the deployment related orgs

```
set the org alias for the deployment related orgs

USAGE
  $ sfdx saatc:agile:configure -u <string> -s agile|validate [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -s, --setting=(agile|validate)                                                    (required) the setting to write to

  -u, --alias=alias                                                                 (required) the alias or username of
                                                                                    the org that for the target setting

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:agile:configure -s agile -u myAgileOrg
```

## `sfdx saatc:agile:validate [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

will run a validation against the configured validation org with the working branch manifest file

```
will run a validation against the configured validation org with the working branch manifest file

USAGE
  $ sfdx saatc:agile:validate [-u <string>] [--apiversion <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:build:deploy [-N] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:build:deploy [-N] [-u <string>] [--apiversion <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -N, --novalidate                                                                  will skip the validation phase and
                                                                                    go to a straigh deploy

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:build:package [-v <string>] [-W] [-B -c <string>] [--script] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

create and/or update a manifest file with the contents of your current working files

```
create and/or update a manifest file with the contents of your current working files

USAGE
  $ sfdx saatc:build:package [-v <string>] [-W] [-B -c <string>] [--script] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -B, --commitgeneratedmanifest                                                     commit the generated manifest file
                                                                                    immediately upon creation

  -W, --includeworkingfiles                                                         if set, will include uncommitted
                                                                                    changes into the package from your
                                                                                    current working directory

  -c, --commit=commit                                                               add the changes specified by the
                                                                                    commit hash to the manifest

  -v, --api=api                                                                     api version for the built manifest
                                                                                    if different than what is already
                                                                                    declared

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

  --script                                                                          do not set if not running command
                                                                                    from a script

EXAMPLES
  sfdx saatc:build:package -p manifest/someNewPackage.xml -a 50.0
  sfdx saatc:build:package -p manifest/someOtherNewPackage.xml
```

## `sfdx saatc:deploy [-p <string> -F] [-c <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

deploy files to a salesforce org

```
deploy files to a salesforce org

USAGE
  $ sfdx saatc:deploy [-p <string> -F] [-c <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -F, --fulldeploy                                                                  deploy all files indicated in the
                                                                                    manifest

  -c, --commit=commit                                                               add the changes specified by the
                                                                                    commit hash to the manifest

  -p, --packagename=packagename                                                     the name of the manifest file to
                                                                                    deploy. must be specified when
                                                                                    running in full deployment mode

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:deploy
  sfdx saatc:deploy -F -p "package.xml"
  sfdx saatc:deploy -c e9a6a03
```

## `sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

logs the user into the currently set org

```
logs the user into the currently set org

USAGE
  $ sfdx saatc:login [-u <string>] [-t <string>] [-D] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -D, --default                                                                     add a new path to the preferred
                                                                                    paths

  -t, --target=target                                                               redirect the user upon login to the
                                                                                    saved target definition

  -u, --alias=alias                                                                 log the user into the aliased org
                                                                                    declared should that org be
                                                                                    authorized

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  sfdx saatc:login
  sfdx saatc:login -u myDevOrg -t settings
```

## `sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

add a new path to the saved paths

```
add a new path to the saved paths

USAGE
  $ sfdx saatc:login:add -t <string> -u <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will add the specified
                                                                                    target entry from the saved targets.

  -u, --uri=uri                                                                     (required) will set the uri for the
                                                                                    specified target to redirect to upon
                                                                                    login.

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:add -t setup -u lightning/setup/SetupOneHome/home
```

## `sfdx saatc:login:remove -t <string> [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

removes a path from the saved login paths

```
removes a path from the saved login paths

USAGE
  $ sfdx saatc:login:remove -t <string> [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -t, --target=target                                                               (required) will remove the specified
                                                                                    target entry from the saved targets

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  sfdx saatc:login:remove -t deprecatedpath
```
<!-- commandsstop -->

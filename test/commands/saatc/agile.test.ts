import { execCmd, TestSession } from '@salesforce/cli-plugins-testkit';

describe('saatc:agile', () => {
    let testSession:  TestSession;
    before(async () => {
        testSession = await TestSession.create({
            project: {
                name: 'MyTestProject'
            }
        });
    });

    it('should print connected agile orgs', async () => {
        const rv = await execCmd('agile', { async: true, ensureExitCode: 0});
        console.log(rv);
    });

    after(async () => {
        await testSession?.clean();
    });
});